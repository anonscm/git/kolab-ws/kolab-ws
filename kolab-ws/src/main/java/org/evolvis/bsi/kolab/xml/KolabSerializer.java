package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.service.Address;
import org.evolvis.bsi.kolab.service.AddressType;
import org.evolvis.bsi.kolab.service.Attendee;
import org.evolvis.bsi.kolab.service.AttendeeStatus;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.ColorLabel;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Gender;
import org.evolvis.bsi.kolab.service.Incidence;
import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Phone;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.evolvis.bsi.kolab.service.PreferredAddress;
import org.evolvis.bsi.kolab.service.Recurrence;
import org.evolvis.bsi.kolab.service.Role;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.evolvis.bsi.kolab.service.ShowTimeAs;
import org.evolvis.bsi.kolab.service.Status;
import org.evolvis.bsi.kolab.service.Task;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import static org.evolvis.bsi.kolab.xml.Helper.toBooleanString;
import static org.evolvis.bsi.kolab.xml.Helper.toDateString;
import static org.evolvis.bsi.kolab.xml.Helper.toDateTimeString;
import static org.evolvis.bsi.kolab.xml.Helper.toNumberString;

/**
 * This class holds static operations to serialize (and deserialize)
 * {@link CollaborationItem} instances to kolab xml format.
 *
 * @author Hendrik Helwich
 */
public class KolabSerializer implements XMLStreamConstants, KolabConstants {
	protected static final Logger logger = Logger.getLogger(KolabSerializer.class.getName());

	public static Event
	readEvent(InputStream xmlStream)
	throws KolabParseException
	{
		try {
			return read(new EventItemHandler(), xmlStream);
		} catch (XMLStreamException e) {
			throw new KolabParseException("Failed to parse kolab event XML stream.", e);
		}
	}

	public static Contact
	readContact(InputStream xmlStream)
	throws KolabParseException
	{
		try {
			return read(new ContactItemHandler(), xmlStream);
		} catch (XMLStreamException e) {
			throw new KolabParseException("Failed to parse kolab contact XML stream.", e);
		}
	}

	public static Note
	readNote(InputStream xmlStream)
	throws KolabParseException
	{
		try {
			return read(new NoteItemHandler(), xmlStream);
		} catch (XMLStreamException e) {
			throw new KolabParseException("Failed to parse kolab note XML stream.", e);
		}
	}

	public static Task
	readTask(InputStream xmlStream)
	throws KolabParseException
	{
		try {
			return read(new TaskItemHandler(), xmlStream);
		} catch (XMLStreamException e) {
			throw new KolabParseException("Failed to parse kolab task XML stream.", e);
		}
	}

	public static void
	write(Event event, OutputStream xmlStream) throws XMLStreamException
	{
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(xmlStream);

		writer.writeStartDocument();
		writer.writeStartElement(ELEMENT_EVENT);
		writer.writeAttribute(ATTRIBUTE_VERSION, VALUE_VERSION_1_0);

		writeIncidenceElements(event, writer);

		String showta = null;
		ShowTimeAs showTimeAs = event.getShowTimeAs();
		if (showTimeAs != null)
			switch (showTimeAs) {
			case FREE:
				showta = VALUE_SHOW_TIME_AS_FREE;
				break;
			case TENTATIVE:
				showta = VALUE_SHOW_TIME_AS_TENTATIVE;
				break;
			case BUSY:
				showta = VALUE_SHOW_TIME_AS_BUSY;
				break;
			case OUTOFOFFICE:
				showta = VALUE_SHOW_TIME_AS_OUTOFOFFICE;
				break;
			default:
				throw new RuntimeException("wsdl changed. unknown showTimeAs value " + showTimeAs);
			}
		writeElement(writer, ELEMENT_SHOW_TIME_AS, showta);

		String clrlbl = null;
		ColorLabel colorLabel = event.getColorLabel();
		if (colorLabel != null)
			switch (colorLabel) {
			case NONE:
				clrlbl = VALUE_COLOR_LABEL_NONE;
				break;
			case IMPORTANT:
				clrlbl = VALUE_COLOR_LABEL_IMPORTANT;
				break;
			case BUSINESS:
				clrlbl = VALUE_COLOR_LABEL_BUSINESS;
				break;
			case PERSONAL:
				clrlbl = VALUE_COLOR_LABEL_PERSONAL;
				break;
			case VACATION:
				clrlbl = VALUE_COLOR_LABEL_VACATION;
				break;
			case MUST_ATTEND:
				clrlbl = VALUE_COLOR_LABEL_MUST_ATTEND;
				break;
			case TRAVEL_REQUIRED:
				clrlbl = VALUE_COLOR_LABEL_TRAVEL_REQUIRED;
				break;
			case NEEDS_PREPARATION:
				clrlbl = VALUE_COLOR_LABEL_NEEDS_PREPARATION;
				break;
			case BIRTHDAY:
				clrlbl = VALUE_COLOR_LABEL_BIRTHDAY;
				break;
			case ANNIVERSARY:
				clrlbl = VALUE_COLOR_LABEL_ANNIVERSARY;
				break;
			case PHONE_CALL:
				clrlbl = VALUE_COLOR_LABEL_PHONE_CALL;
				break;
			default:
				throw new RuntimeException("wsdl changed. unknown colorLabel value " + colorLabel);
			}
		writeElement(writer, ELEMENT_COLOR_LABEL, clrlbl);

		String endDate = toDateString(event.getEndDate(), event.isAllDay());
		writeElement(writer, ELEMENT_END_DATE, endDate);

		writer.writeEndElement();
		writer.writeEndDocument();
		writer.close();
	}

	public static void
	write(Contact contact, OutputStream xmlStream) throws XMLStreamException
	{
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(xmlStream);

		writer.writeStartDocument();
		writer.writeStartElement(ELEMENT_CONTACT);
		writer.writeAttribute(ATTRIBUTE_VERSION, VALUE_VERSION_1_0);

		writeCollaborationItemElements(contact, writer);

		writer.writeStartElement(ELEMENT_NAME);
		writeElement(writer, ELEMENT_NAME_GIVEN_NAME, contact.getGivenName());
		writeElement(writer, ELEMENT_NAME_MIDDLE_NAMES, contact.getMiddleNames());
		writeElement(writer, ELEMENT_NAME_LAST_NAME, contact.getLastName());
		writeElement(writer, ELEMENT_NAME_FULL_NAME, contact.getFullName());
		writeElement(writer, ELEMENT_NAME_INITIALS, contact.getInitials());
		writeElement(writer, ELEMENT_NAME_PREFIX, contact.getPrefix());
		writeElement(writer, ELEMENT_NAME_SUFFIX, contact.getSuffix());
		writer.writeEndElement();

		writeElement(writer, ELEMENT_FREE_BUSY_URL, contact.getFreeBusyUrl());
		writeElement(writer, ELEMENT_ORGANIZATION, contact.getOrganization());
		writeElement(writer, ELEMENT_WEB_PAGE, contact.getWebPage());
		writeElement(writer, ELEMENT_IM_ADDRESS, contact.getImAddress());
		writeElement(writer, ELEMENT_DEPARTMENT, contact.getDepartment());
		writeElement(writer, ELEMENT_OFFICE_LOCATION, contact.getOfficeLocation());
		writeElement(writer, ELEMENT_PROFESSION, contact.getProfession());
		writeElement(writer, ELEMENT_JOB_TITLE, contact.getJobTitle());
		writeElement(writer, ELEMENT_MANAGER_NAME, contact.getManagerName());
		writeElement(writer, ELEMENT_ASSISTANT, contact.getAssistant());
		writeElement(writer, ELEMENT_NICK_NAME, contact.getNickName());
		writeElement(writer, ELEMENT_SPOUSE_NAME, contact.getSpouseName());
		String dateStr = toDateString(contact.getBirthday());
		writeElement(writer, ELEMENT_BIRTHDAY, dateStr);
		dateStr = toDateString(contact.getAnniversary());
		writeElement(writer, ELEMENT_ANNIVERSARY, dateStr);
		writeElement(writer, ELEMENT_PICTURE, contact.getPictureAttachmentId());
		writeElement(writer, ELEMENT_CHILDREN, contact.getChildren());
		Gender gender = contact.getGender();
		String genderStr = null;
		if (Gender.MALE == gender)
			genderStr = VALUE_GENDER_MALE;
		else if (Gender.FEMALE == gender)
			genderStr = VALUE_GENDER_FEMALE;
		writeElement(writer, ELEMENT_GENDER, genderStr);
		writeElement(writer, ELEMENT_LANGUAGE, contact.getLanguage());

		for (Phone phone : contact.getPhone()) {
			writer.writeStartElement(ELEMENT_PHONE);
			PhoneType phtp = phone.getType();
			String phtpstr = null;
			if (phtp != null)
				switch (phtp) {
				case BUSINESS_1:
					phtpstr = VALUE_PHONE_TYPE_BUSINESS_1;
					break;
				case BUSINESS_2:
					phtpstr = VALUE_PHONE_TYPE_BUSINESS_2;
					break;
				case BUSINESSFAX:
					phtpstr = VALUE_PHONE_TYPE_BUSINESSFAX;
					break;
				case CALLBACK:
					phtpstr = VALUE_PHONE_TYPE_CALLBACK;
					break;
				case CAR:
					phtpstr = VALUE_PHONE_TYPE_CAR;
					break;
				case COMPANY:
					phtpstr = VALUE_PHONE_TYPE_COMPANY;
					break;
				case HOME_1:
					phtpstr = VALUE_PHONE_TYPE_HOME_1;
					break;
				case HOME_2:
					phtpstr = VALUE_PHONE_TYPE_HOME_2;
					break;
				case HOMEFAX:
					phtpstr = VALUE_PHONE_TYPE_HOMEFAX;
					break;
				case ISDN:
					phtpstr = VALUE_PHONE_TYPE_ISDN;
					break;
				case MOBILE:
					phtpstr = VALUE_PHONE_TYPE_MOBILE;
					break;
				case PAGER:
					phtpstr = VALUE_PHONE_TYPE_PAGER;
					break;
				case PRIMARY:
					phtpstr = VALUE_PHONE_TYPE_PRIMARY;
					break;
				case RADIO:
					phtpstr = VALUE_PHONE_TYPE_RADIO;
					break;
				case TELEX:
					phtpstr = VALUE_PHONE_TYPE_TELEX;
					break;
				case TTYTDD:
					phtpstr = VALUE_PHONE_TYPE_TTYTDD;
					break;
				case ASSISTANT:
					phtpstr = VALUE_PHONE_TYPE_ASSISTANT;
					break;
				case OTHER:
					phtpstr = VALUE_PHONE_TYPE_OTHER;
					break;
				default:
					throw new RuntimeException("wsdl changed. unknown phone type " + phtp);
				}
			writeElement(writer, ELEMENT_PHONE_TYPE, phtpstr);
			writeElement(writer, ELEMENT_PHONE_NUMBER, phone.getNumber());
			writer.writeEndElement();
		}

		for (Email email : contact.getEmail()) {
			writer.writeStartElement(ELEMENT_EMAIL);
			writeElement(writer, ELEMENT_EMAIL_DISPLAY_NAME, email.getDisplayName());
			writeElement(writer, ELEMENT_EMAIL_SMTP_ADDRESS, email.getSmtpAddress());
			writer.writeEndElement();
		}

		for (Address address : contact.getAddress()) {
			writer.writeStartElement(ELEMENT_ADDRESS);
			AddressType adtp = address.getType();
			String adtpstr = null;
			if (adtp != null)
				switch (adtp) {
				case HOME:
					adtpstr = VALUE_ADDRESS_TYPE_HOME;
					break;
				case BUSINESS:
					adtpstr = VALUE_ADDRESS_TYPE_BUSINESS;
					break;
				case OTHER:
					adtpstr = VALUE_ADDRESS_TYPE_OTHER;
					break;
				default:
					throw new RuntimeException("wsdl changed. unknown adress type " + adtp);
				}
			writeElement(writer, ELEMENT_ADDRESS_TYPE, adtpstr);
			writeElement(writer, ELEMENT_ADDRESS_STREET, address.getStreet());
			writeElement(writer, ELEMENT_ADDRESS_LOCALITY, address.getLocality());
			writeElement(writer, ELEMENT_ADDRESS_REGION, address.getRegion());
			writeElement(writer, ELEMENT_ADDRESS_POSTAL_CODE, address.getPostalCode());
			writeElement(writer, ELEMENT_ADDRESS_COUNTRY, address.getCountry());
			writer.writeEndElement();
		}
		PreferredAddress padtp = contact.getPreferredAddress();
		String padtpstr = null;
		if (padtp != null)
			switch (padtp) {
			case HOME:
				padtpstr = VALUE_ADDRESS_TYPE_HOME;
				break;
			case BUSINESS:
				padtpstr = VALUE_ADDRESS_TYPE_BUSINESS;
				break;
			case OTHER:
				padtpstr = VALUE_ADDRESS_TYPE_OTHER;
				break;
			case NONE:
				padtpstr = VALUE_PREFERRED_ADDRESS_NONE;
				break;
			default:
				throw new RuntimeException("wsdl changed. unknown preferred adress type " + padtp);
			}
		writeElement(writer, ELEMENT_PREFERRED_ADDRESS, padtpstr);
		writeElement(writer, ELEMENT_LATITUDE, toNumberString(contact.getLatitude()));
		writeElement(writer, ELEMENT_LONGITUDE, toNumberString(contact.getLongitude()));

		writer.writeEndElement();
		writer.writeEndDocument();
		writer.close();
	}

	public static void
	write(Note note, OutputStream xmlStream) throws XMLStreamException
	{
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(xmlStream);

		writer.writeStartDocument();
		writer.writeStartElement(ELEMENT_NOTE);
		writer.writeAttribute(ATTRIBUTE_VERSION, VALUE_VERSION_1_0);

		writeCollaborationItemElements(note, writer);

		writeElement(writer, ELEMENT_NOTE_SUMMARY, note.getSummary());
		writeElement(writer, ELEMENT_BACKGROUND_COLOR, note.getBackgroundColor());
		writeElement(writer, ELEMENT_FOREGROUND_COLOR, note.getForegroundColor());

		writer.writeEndElement();
		writer.writeEndDocument();
		writer.close();
	}

	public static void
	write(Task task, OutputStream xmlStream) throws XMLStreamException
	{
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(xmlStream);

		writer.writeStartDocument();
		writer.writeStartElement(ELEMENT_TASK);
		writer.writeAttribute(ATTRIBUTE_VERSION, VALUE_VERSION_1_0);

		writeIncidenceElements(task, writer);

		writeElement(writer, ELEMENT_PRIORITY, toNumberString(task.getPriority()));
		writeElement(writer, ELEMENT_COMPLETED, toNumberString(task.getCompleted()));
		Status status = task.getStatus();
		String statstr = null;
		if (status != null)
			switch (status) {
			case NOT_STARTED:
				statstr = VALUE_STATUS_NOT_STARTED;
				break;
			case IN_PROGRESS:
				statstr = VALUE_STATUS_IN_PROGRESS;
				break;
			case COMPLETED:
				statstr = VALUE_STATUS_COMPLETED;
				break;
			case WAITING_ON_SOMEONE_ELSE:
				statstr = VALUE_STATUS_WAITING_ON_SOMEONE_ELSE;
				break;
			case DEFERRED:
				statstr = VALUE_STATUS_DEFERRED;
				break;
			default:
				throw new RuntimeException("wsdl changed. unknown status " + status);
			}
		writeElement(writer, ELEMENT_STATUS, statstr);
		String dateStr;
		if (task.isDueDateAllDay() != null && task.isDueDateAllDay())
			dateStr = toDateString(task.getDueDate());
		else
			dateStr = toDateTimeString(task.getDueDate());
		writeElement(writer, ELEMENT_DUE_DATE, dateStr);
		writeElement(writer, ELEMENT_PARENT, task.getParent());

		writer.writeEndElement();
		writer.writeEndDocument();
		writer.close();
	}

	private static String toString(Integer value)
	{
		if (value == null)
			return null;
		else
			return Integer.toString(value);
	}

	private static void writeWeekDays(XMLStreamWriter writer, int days) throws XMLStreamException
	{
		for (int i = 0; i < 7; i++)
			if ((days & (1 << i)) != 0)
				writeElement(writer, ELEMENT_RECURRENCE_DAY, VALUE_RECURRENCE_DAY[i]);
	}

	private static void
	writeIncidenceElements(Incidence incidence, XMLStreamWriter writer)
	throws XMLStreamException
	{
		writeCollaborationItemElements(incidence, writer);

		writeElement(writer, ELEMENT_SUMMARY, incidence.getSummary());
		writeElement(writer, ELEMENT_LOCATION, incidence.getLocation());

		if (incidence.getOrganizerDisplayName() != null || incidence.getOrganizerSmtpAddress() != null) {
			writer.writeStartElement(ELEMENT_ORGANIZER);
			writeElement(writer, ELEMENT_ORGANIZER_DISPLAY_NAME, incidence.getOrganizerDisplayName());
			writeElement(writer, ELEMENT_ORGANIZER_SMTP_ADDRESS, incidence.getOrganizerSmtpAddress());
			writer.writeEndElement();
		}

		String startDate = toDateString(incidence.getStartDate(), incidence.isAllDay());
		writeElement(writer, ELEMENT_START_DATE, startDate);
		writeElement(writer, ELEMENT_ALARM, toString(incidence.getAlarm()));

		Recurrence recurrence = incidence.getRecurrence();
		if (recurrence != null) {
			writer.writeStartElement(ELEMENT_RECURRENCE);
			if (recurrence.getYearDay() != null) {
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_CYCLE, VALUE_RECURRENCE_CYCLE_YEARLY);
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_TYPE, VALUE_RECURRENCE_TYPE_YEARDAY);
				writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
				    Integer.toString(recurrence.getInterval()));
				writeElement(writer, ELEMENT_RECURRENCE_DAYNUMBER, recurrence.getYearDay().toString());
			} else if (recurrence.getMonth() != null) {
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_CYCLE, VALUE_RECURRENCE_CYCLE_YEARLY);
				if (recurrence.getMonthDay() != null) {
					writer.writeAttribute(ATTRIBUTE_RECURRENCE_TYPE,
					    VALUE_RECURRENCE_TYPE_MONTHDAY);
					writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
					    Integer.toString(recurrence.getInterval()));
					writeElement(writer, ELEMENT_RECURRENCE_DAYNUMBER,
					    recurrence.getMonthDay().toString());
					writeElement(writer, ELEMENT_RECURRENCE_MONTH,
					    VALUE_RECURRENCE_MONTH[recurrence.getMonth() - 1]);
				} else {
					writer.writeAttribute(ATTRIBUTE_RECURRENCE_TYPE, VALUE_RECURRENCE_TYPE_WEEKDAY);
					writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
					    Integer.toString(recurrence.getInterval()));
					writeElement(writer, ELEMENT_RECURRENCE_DAYNUMBER,
					    recurrence.getWeek().toString());
					writeWeekDays(writer, recurrence.getWeekDays());
					writeElement(writer, ELEMENT_RECURRENCE_MONTH,
					    VALUE_RECURRENCE_MONTH[recurrence.getMonth() - 1]);
				}
			} else if (recurrence.getMonthDay() != null) {
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_CYCLE, VALUE_RECURRENCE_CYCLE_MONTHLY);
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_TYPE, VALUE_RECURRENCE_TYPE_DAYNUMBER);
				writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
				    Integer.toString(recurrence.getInterval()));
				writeElement(writer, ELEMENT_RECURRENCE_DAYNUMBER, recurrence.getMonthDay().toString());
			} else if (recurrence.getWeek() != null) {
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_CYCLE, VALUE_RECURRENCE_CYCLE_MONTHLY);
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_TYPE, VALUE_RECURRENCE_TYPE_WEEKDAY);
				writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
				    Integer.toString(recurrence.getInterval()));
				writeElement(writer, ELEMENT_RECURRENCE_DAYNUMBER, recurrence.getWeek().toString());
				writeWeekDays(writer, recurrence.getWeekDays());
			} else if (recurrence.getWeekDays() != null) {
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_CYCLE, VALUE_RECURRENCE_CYCLE_WEEKLY);
				writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
				    Integer.toString(recurrence.getInterval()));
				writeWeekDays(writer, recurrence.getWeekDays());
			} else {
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_CYCLE, VALUE_RECURRENCE_CYCLE_DAILY);
				writeElement(writer, ELEMENT_RECURRENCE_INTERVAL,
				    Integer.toString(recurrence.getInterval()));
			}

			if (recurrence.getRangeNumber() != null) { // rangeDate == null
				int nbr = recurrence.getRangeNumber();
				writer.writeStartElement(ELEMENT_RECURRENCE_RANGE);
				if (nbr == 0)
					writer.writeAttribute(ATTRIBUTE_RECURRENCE_RANGE_TYPE,
					    VALUE_RECURRENCE_RANGE_TYPE_NONE);
				else {
					writer.writeAttribute(ATTRIBUTE_RECURRENCE_RANGE_TYPE,
					    VALUE_RECURRENCE_RANGE_TYPE_NUMBER);
					writer.writeCharacters(Integer.toString(nbr));
				}
				writer.writeEndElement();
			} else { // rangeDate != null
				writer.writeStartElement(ELEMENT_RECURRENCE_RANGE);
				writer.writeAttribute(ATTRIBUTE_RECURRENCE_RANGE_TYPE,
				    VALUE_RECURRENCE_RANGE_TYPE_DATE);
				writer.writeCharacters(toDateString(recurrence.getRangeDate()));
				writer.writeEndElement();
			}

			for (Date exclusion : recurrence.getExclusion()) {
				String dateStr = toDateString(exclusion);
				writeElement(writer, ELEMENT_RECURRENCE_EXCLUSION, dateStr);
			}
			writer.writeEndElement();
		}

		for (Attendee attendee : incidence.getAttendee()) {
			writer.writeStartElement(ELEMENT_ATTENDEE);
			writeElement(writer, ELEMENT_ATTENDEE_DISPLAY_NAME, attendee.getDisplayName());
			writeElement(writer, ELEMENT_ATTENDEE_SMTP_ADDRESS, attendee.getSmtpAddress());
			String statusStr = null;
			AttendeeStatus status = attendee.getStatus();
			if (status != null)
				switch (status) {
				case NONE:
					statusStr = VALUE_ATTENDEE_STATUS_NONE;
					break;
				case TENTATIVE:
					statusStr = VALUE_ATTENDEE_STATUS_TENTATIVE;
					break;
				case ACCEPTED:
					statusStr = VALUE_ATTENDEE_STATUS_ACCEPTED;
					break;
				case DECLINED:
					statusStr = VALUE_ATTENDEE_STATUS_DECLINED;
					break;
				case NEEDS_ACTION:
					statusStr = VALUE_ATTENDEE_STATUS_NEEDS_ACTION;
					break;
				case DELEGATED:
					statusStr = VALUE_ATTENDEE_STATUS_DELEGATED;
					break;
				default:
					throw new RuntimeException("wsdl file changed. unknown status " + status);
				}
			writeElement(writer, ELEMENT_ATTENDEE_STATUS, statusStr);
			String isReqResp = toBooleanString(attendee.isRequestResponse());
			writeElement(writer, ELEMENT_ATTENDEE_REQUEST_RESPONSE, isReqResp);
			String roleStr = null;
			Role role = attendee.getRole();
			if (role != null)
				switch (role) {
				case REQUIRED:
					roleStr = VALUE_ATTENDEE_ROLE_REQUIRED;
					break;
				case OPTIONAL:
					roleStr = VALUE_ATTENDEE_ROLE_OPTIONAL;
					break;
				case RESOURCE:
					roleStr = VALUE_ATTENDEE_ROLE_RESOURCE;
					break;
				default:
					throw new RuntimeException("wsdl file changed. unknown role " + role);
				}
			writeElement(writer, ELEMENT_ATTENDEE_ROLE, roleStr);
			writer.writeEndElement();
		}
	}

	private static void
	writeElement(XMLStreamWriter writer, String elementName, String content)
	throws XMLStreamException
	{
		if (content != null) {
			writer.writeStartElement(elementName);
			writer.writeCharacters(content);
			writer.writeEndElement();
		}
	}

	private static void
	writeCollaborationItemElements(CollaborationItem item, XMLStreamWriter writer)
	throws XMLStreamException
	{
		writeElement(writer, ELEMENT_UID, item.getUid());
		writeElement(writer, ELEMENT_BODY, item.getBody());
		writeElement(writer, ELEMENT_CATEGORIES, item.getCategories());
		writeElement(writer, ELEMENT_CREATION_DATE, toDateTimeString(item.getCreationDate()));
		writeElement(writer, ELEMENT_LAST_MODIFICATION_DATE, toDateTimeString(item.getLastModificationDate()));
		String sensivity = null;
		Sensitivity sens = item.getSensitivity();
		if (sens != null) {
			switch (sens) {
			case PUBLIC:
				sensivity = VALUE_SENSITIVITY_PUBLIC;
				break;
			case PRIVATE:
				sensivity = VALUE_SENSITIVITY_PRIVATE;
				break;
			case CONFIDENTIAL:
				sensivity = VALUE_SENSITIVITY_CONFIDENTIAL;
				break;
			default:
				throw new RuntimeException("wsdl definition changed");
			}
			writeElement(writer, ELEMENT_SENSITIVITY, sensivity);
		}
		for (String attachment : item.getInlineAttachment())
			writeElement(writer, ELEMENT_INLINE_ATTACHMENT, attachment);
		for (String attachment : item.getLinkAttachment())
			writeElement(writer, ELEMENT_LINK_ATTACHMENT, attachment);
		writeElement(writer, ELEMENT_PRODUCT_ID, item.getProductId());
	}

	protected static <T extends CollaborationItem> T
	read(ItemHandler<T> handler, InputStream xmlStream)
	throws XMLStreamException, KolabParseException
	{
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(xmlStream);

		for (int level = 0; parser.hasNext(); parser.next()) {
			int eventType = parser.getEventType();
			if (START_ELEMENT == eventType) {
				level++;
				String name = parser.getLocalName();
				if (level == 1) {
					handler.checkRootName(name);
					checkVersion(parser);
					continue;
				}
				if (level == 2) {
					if (handler.readElement(name, parser)) {
						level--;
						// check parser return state
						assert parser.getEventType() == END_ELEMENT;
						assert parser.getLocalName() == name;
					} else {
						logger.info("unhandled kolab data element: " + name);
						continue;
					}
				}
			} else if (END_ELEMENT == eventType)
				level--;
		}
		handler.validate();
		return handler.getItem();
	}

	/**
	 * Checks if the version which is attributed to the root element holds the
	 * correct value.
	 * The specified parser must be located on the opening root element.
	 */
	protected static void
	checkVersion(XMLStreamReader parser) throws KolabParseException
	{
		String version = parser.getAttributeValue(null, ATTRIBUTE_VERSION);
		if (!VALUE_VERSION_1_0.equals(version))
			throw new KolabParseException("not supported version '" +
			    version + "'");
	}

	public static void
	write(CollaborationItem item, OutputStream out) throws XMLStreamException
	{
		if (item instanceof Event)
			write((Event)item, out);
		else if (item instanceof Contact)
			write((Contact)item, out);
		else if (item instanceof Task)
			write((Task)item, out);
		else if (item instanceof Note)
			write((Note)item, out);
		else
			throw new UnsupportedOperationException();
	}
}
