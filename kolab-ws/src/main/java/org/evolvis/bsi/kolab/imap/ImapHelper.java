package org.evolvis.bsi.kolab.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.iap.Argument;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.AppendUID;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.Helper;
import org.evolvis.bsi.kolab.KolabConfigException;
import org.evolvis.bsi.kolab.service.Attachment;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Task;
import org.evolvis.bsi.kolab.util.FolderType;
import org.evolvis.bsi.kolab.xml.KolabParseException;
import org.evolvis.bsi.kolab.xml.KolabSerializerDataSource;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.evolvis.bsi.kolab.Helper.isInTestMode;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPBodyPart_getContentType;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPBodyPart_getHeader;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPBodyPart_getInputStream;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFFolder_getMessageByUID;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFFolder_getMode;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_appendUIDMessages;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_close;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_doOptionalCommand;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_expunge;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_getFullName;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_getStore;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_isOpen;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_list;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPFolder_open;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPInternetAddress_new;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPInternetHeaders_addHeaderLine;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPInternetHeaders_new;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_getAllHeaders;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_getContent;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_getSubject;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_setContent;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_setFlag;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_setFrom;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_setHeader;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMessage_setSubject;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeBodyPart_new;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeBodyPart_setDescription;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeBodyPart_setFileName;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeMessage_new;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeMultipart_addBodyPart;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeMultipart_getBodyPart;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeMultipart_getCount;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPMimeMultipart_removeBodyPart;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPProtocol_command;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPResponse_getKey;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPResponse_getRest;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPStore_getFolder;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPStore_getProtocol;
import static org.evolvis.bsi.kolab.imap.IMAPProfilingUtil.IMAPStore_getUserNamespaces;

/**
 * Set of static operations which add functionality which is missing in the java
 * mail library or which alleviate imap specific tasks.
 *
 * @author Hendrik Helwich
 */
public class ImapHelper {
	private static final String KOLAB_TYPE_PREFIX = "application/x-vnd.kolab.";

	private static final String PRODUCT_ID = "Kolab WebService (tarent)";

	private static final Logger logger = Logger.getLogger(ImapHelper.class.getName());

	private static final String TYPE_CONTACT = "contact";
	private static final String TYPE_EVENT = "event";
	private static final String TYPE_JOURNAL = "journal";
	private static final String TYPE_NOTE = "note";
	private static final String TYPE_TASK = "task";
	private static final String TYPE_MAIL = "mail";

	private static final String SUBTYPE_DEFAULT = "default";
	private static final String SUBTYPE_INBOX = "inbox";
	private static final String SUBTYPE_DRAFTS = "drafts";
	private static final String SUBTYPE_SENTITEMS = "sentitems";
	private static final String SUBTYPE_JUNKMAIL = "junkemail";

	private static final String RESPONSE_ANNOTATION = "ANNOTATION";
	private static final String COMMAND_GETANNOTATION = "GETANNOTATION";
	private static final String ARG_ANNOTATION_1 = "\"/vendor/kolab/folder-type\"";
	private static final String ARG_ANNOTATION_2 = "\"value\"";

	private static final String ARG_ANNOTATION_SYNC_1 = "\"/vendor/tarent/syncpref\"";
	private static final String ARG_ANNOTATION_SYNC_2 = "\"value.priv\"";

	// example value: "INBOX/Kalender" "/vendor/kolab/folder-type" ("value.shared" "event.default")
	private static final Pattern PATTERN_RESPONSE_ANNOTATION = Pattern.compile(
	    "\\s*\"([^\"]*)\"\\s+\"/vendor/kolab/folder-type\"\\s+\\(\"([^\"]*)\"\\s+\"([^\"]*)\"\\)\\s*");
	// example value: "user/hh/Kontakte" "/vendor/tarent/syncpref" ("value.priv" "syncme")
	private static final Pattern PATTERN_RESPONSE_ANNOTATION_SYNC = Pattern.compile(
	    "\\s*\"([^\"]*)\"\\s+\"/vendor/tarent/syncpref\"\\s+\\(\"([^\"]*)\"\\s+\"([^\"]*)\"\\)\\s*");

	public static boolean
	isSyncFolder(IMAPFolder folder)
	throws MessagingException, KolabConfigException
	{

		IMAPStore store = IMAPFolder_getStore(folder);
		IMAPProtocol protocol = IMAPStore_getProtocol(store, folder);

		if (isInTestMode()) // in junit test mode => do directly
			//TODO use bytecode hack lib to avoid this check?
			return (Boolean)IMAPFolder_doOptionalCommand(folder, "syncFolder", null);

		Argument args = new Argument();
		StringBuilder sb = new StringBuilder();
		sb.append('\"').append(IMAPFolder_getFullName(folder)).append('\"');
		args.writeAtom(sb.toString());
		args.writeAtom(ARG_ANNOTATION_SYNC_1);
		args.writeAtom(ARG_ANNOTATION_SYNC_2);

		Response[] response = IMAPProtocol_command(protocol, COMMAND_GETANNOTATION, args);
		for (Response resp : response) {
			IMAPResponse iresp = (IMAPResponse)resp;
			if (RESPONSE_ANNOTATION.equals(IMAPResponse_getKey(iresp))) {
				String rest = IMAPResponse_getRest(iresp);
				Matcher matcher = PATTERN_RESPONSE_ANNOTATION_SYNC.matcher(rest);
				if (matcher.matches()
				    && IMAPFolder_getFullName(folder).equals(matcher.group(1))
				    && "value.priv".equals(matcher.group(2))
				    && "syncme".equals(matcher.group(3))) {
					return true;
				} else
					throw new KolabConfigException("unexpected sync annotation response for folder "
					    + IMAPFolder_getFullName(folder) + ": " + rest);
			}
		}
		return false;
	}

	/**
	 * Get the kolab type of an imap folder. This is done by checking imap
	 * annotations which are set by other kolab clients.
	 */
	public static FolderType
	getFolderType(IMAPFolder folder)
	throws MessagingException, KolabConfigException
	{
		IMAPStore store = IMAPFolder_getStore(folder);
		IMAPProtocol protocol = IMAPStore_getProtocol(store, folder);

		if (isInTestMode()) // in junit test mode => do directly
			return (FolderType)IMAPFolder_doOptionalCommand(folder, "folderType", null);

		Argument args = new Argument();
		StringBuilder sb = new StringBuilder();
		sb.append('\"').append(IMAPFolder_getFullName(folder)).append('\"');
		args.writeAtom(sb.toString());
		args.writeAtom(ARG_ANNOTATION_1);
		args.writeAtom(ARG_ANNOTATION_2);

		Response[] response = IMAPProtocol_command(protocol, COMMAND_GETANNOTATION, args);
		for (Response resp : response) {
			IMAPResponse iresp = (IMAPResponse)resp;
			if (RESPONSE_ANNOTATION.equals(IMAPResponse_getKey(iresp))) {
				String rest = IMAPResponse_getRest(iresp);
				Matcher matcher = PATTERN_RESPONSE_ANNOTATION.matcher(rest);
				if (matcher.matches()
				    && IMAPFolder_getFullName(folder).equals(matcher.group(1))
				    && "value.shared".equals(matcher.group(2))) {
					String type = matcher.group(3);
					String subtype = null;
					int stidx = type.indexOf('.');
					if (stidx != -1) {
						subtype = type.substring(stidx + 1);
						type = type.substring(0, stidx);
					}
					if (TYPE_CONTACT.equals(type)) {
						if (subtype == null)
							return FolderType.CONTACT;
						else if (SUBTYPE_DEFAULT.equals(subtype))
							return FolderType.CONTACT_DEFAULT;
					} else if (TYPE_EVENT.equals(type)) {
						if (subtype == null)
							return FolderType.EVENT;
						else if (SUBTYPE_DEFAULT.equals(subtype))
							return FolderType.EVENT_DEFAULT;
					} else if (TYPE_JOURNAL.equals(type)) {
						if (subtype == null)
							return FolderType.JOURNAL;
						else if (SUBTYPE_DEFAULT.equals(subtype))
							return FolderType.JOURNAL_DEFAULT;
					} else if (TYPE_NOTE.equals(type)) {
						if (subtype == null)
							return FolderType.NOTE;
						else if (SUBTYPE_DEFAULT.equals(subtype))
							return FolderType.NOTE_DEFAULT;
					} else if (TYPE_TASK.equals(type)) {
						if (subtype == null)
							return FolderType.TASK;
						else if (SUBTYPE_DEFAULT.equals(subtype))
							return FolderType.TASK_DEFAULT;
					} else if (TYPE_MAIL.equals(type)) {
						/*
						 * XXX what to do about regular mail folders?
						 */
						if (subtype == null)
							return FolderType.NONE;
						else if (SUBTYPE_DRAFTS.equals(subtype))
							return FolderType.MAIL_DRAFTS;
						else if (SUBTYPE_INBOX.equals(subtype))
							return FolderType.MAIL_INBOX;
						else if (SUBTYPE_JUNKMAIL.equals(subtype))
							return FolderType.MAIL_JUNKMAIL;
						else if (SUBTYPE_SENTITEMS.equals(subtype))
							return FolderType.MAIL_SENTITEMS;
					}
					logger.warn("illegal type " + type
					    + " or subtype " + subtype + " for folder " +
					    IMAPFolder_getFullName(folder));
				} else
					throw new KolabConfigException("unexpected annotation response for folder "
					    + IMAPFolder_getFullName(folder) + ": " + rest);
			}
		}
		return FolderType.NONE;
	}

	/**
	 * Add the given folder and its childs to the given folder list if the
	 * folder has the given folder type. If a default type is found, it is set
	 * to the first element of the list.
	 */
	private static void
	traverseFolder(List<IMAPFolder> folders, IMAPFolder folder,
	    FolderType ftype, FolderType deflt, boolean checkSync, boolean isUserHome)
	throws MessagingException, KolabConfigException
	{
		if (!checkSync || isSyncFolder(
		    folder)) { // maybe check if the folder is marked as syncable folder in the kolab manager
			FolderType type = getFolderType(folder);
			if (type == ftype) {
				if (folders.size() == 0) // default folder goes to index 0 and should be found later
					folders.add(null);
				folders.add(folder);
			} else if (type == deflt) {
				if (folders.size() == 0)
					folders.add(folder);
				else if (folders.get(0) == null)
					folders.set(0, folder);
				else {
					// there shall be no two default folders in the user’s home
					if (isUserHome) {
						throw new KolabConfigException(
						    "there are two default folders for the same type:"
							+ IMAPFolder_getFullName(folder) + " and " +
							IMAPFolder_getFullName(folders.get(0)));
					}
					// but if we’re not in the user’s home we can safely add the folder.
					// we need to do this because we can mark other persons personal open folders
					// as syncable
					// check Bug #28 for description
					else {
						folders.add(folder);
					}
				}
			}
		}
		for (Folder child : IMAPFolder_list(folder))
			traverseFolder(folders, (IMAPFolder)child, ftype, deflt, checkSync, isUserHome);
	}

	/**
	 * Get all folders of a user of a specified folder type.
	 */
	private static List<IMAPFolder>
	getFolders(IMAPStore store, String user, FolderType ftype, FolderType deflt,
	    boolean checkSync) throws KolabConfigException, MessagingException
	{
		//TODO get all folders from the db (add initial folder sync and folder sync thread)

		// get user folders
		Folder[] folder = IMAPStore_getUserNamespaces(store, user);
		List<IMAPFolder> folders = new LinkedList<IMAPFolder>();
		for (Folder f : folder)
			traverseFolder(folders, (IMAPFolder)f, ftype, deflt, checkSync, true);
		if (folders.size() == 0 || folders.get(0) == null)
			throw new KolabConfigException("no default folder can be found for user " + user);

		// get shared folder
		IMAPFolder rootFolder = (IMAPFolder)IMAPStore_getFolder(store, "");
		folder = IMAPFolder_list(rootFolder);
		for (Folder f : folder)
			if (!"INBOX".equals(IMAPFolder_getFullName(f)))
				traverseFolder(folders, (IMAPFolder)f, ftype, deflt, checkSync, false);

		return folders;
	}

	public static List<IMAPFolder>
	getFoldersContact(IMAPStore store, String user, boolean checkSync)
	throws KolabConfigException, MessagingException
	{
		return getFolders(store, user, FolderType.CONTACT, FolderType.CONTACT_DEFAULT, checkSync);
	}

	/**
	 * Get all event folders which can be read by the specified user.
	 * The default folder (if list is not empty) is the first element of the
	 * list.
	 * The INBOX folder of the user is excluded to have unique folder names.
	 */
	public static List<IMAPFolder>
	getFoldersEvent(IMAPStore store, String user, boolean checkSync)
	throws KolabConfigException, MessagingException
	{
		return getFolders(store, user, FolderType.EVENT, FolderType.EVENT_DEFAULT, checkSync);
	}

	public static List<IMAPFolder>
	getFoldersJournal(IMAPStore store, String user, boolean checkSync)
	throws KolabConfigException, MessagingException
	{
		return getFolders(store, user, FolderType.JOURNAL, FolderType.JOURNAL_DEFAULT, checkSync);
	}

	public static List<IMAPFolder>
	getFoldersNote(IMAPStore store, String user, boolean checkSync)
	throws KolabConfigException, MessagingException
	{
		return getFolders(store, user, FolderType.NOTE, FolderType.NOTE_DEFAULT, checkSync);
	}

	public static List<IMAPFolder>
	getFoldersTask(IMAPStore store, String user, boolean checkSync)
	throws KolabConfigException, MessagingException
	{
		return getFolders(store, user, FolderType.TASK, FolderType.TASK_DEFAULT, checkSync);
	}

	/**
	 * Get the stream of the kolab xml data for the specified mail.
	 * If the mail is not available, <code>null</code> is returned.
	 */
	public static InputStream
	getKolabXmlStream(IMAPFolder folder, long uid)
	throws MessagingException, IOException, KolabParseException
	{
		if (!IMAPFolder_isOpen(folder))
			IMAPFolder_open(folder, Folder.READ_ONLY);
		Message message = IMAPFFolder_getMessageByUID(folder, uid);
		if (message == null)
			return null;
		Object content = IMAPMessage_getContent(message);
		if (!(content instanceof MimeMultipart))
			throw new KolabParseException("no mime multipart message");
		MimeMultipart mmp = (MimeMultipart)content;
		if (IMAPMimeMultipart_getCount(mmp) < 2)
			throw new KolabParseException(
			    "only " + IMAPMimeMultipart_getCount(mmp) + " body parts are available");

		int kolabAttachmentIndex = 1;
		for (int i = 1; i < IMAPMimeMultipart_getCount(mmp);
		    ++i) // assume here that first part is the message text part
		{
			final BodyPart bp = IMAPMimeMultipart_getBodyPart(mmp, i);
			if (IMAPBodyPart_getContentType(bp).toLowerCase().contains("x-vnd.kolab")) {
				kolabAttachmentIndex = i;
			}
		}
		BodyPart bp = IMAPMimeMultipart_getBodyPart(mmp, kolabAttachmentIndex);
		return IMAPBodyPart_getInputStream(bp);
	}

	/**
	 * Get the stream of the kolab xml data for the specified mail.
	 * If the mail or attachment is not available, <code>null</code> is returned.
	 */
	public static Attachment
	getAttachmentXmlStream(IMAPFolder folder, long uid, String attachmentId)
	throws MessagingException, IOException, KolabParseException
	{
		if (!IMAPFolder_isOpen(folder))
			IMAPFolder_open(folder, Folder.READ_ONLY);
		Message message = IMAPFFolder_getMessageByUID(folder, uid);
		if (message == null)
			return null;
		Object content = IMAPMessage_getContent(message);
		if (!(content instanceof MimeMultipart))
			throw new KolabParseException("no mime multipart message");
		MimeMultipart mmp = (MimeMultipart)content;
		if (IMAPMimeMultipart_getCount(mmp) < 2)
			throw new KolabParseException("only " +
			    IMAPMimeMultipart_getCount(mmp) +
			    " body parts are available");

		int kolabAttachmentIndex = -1;
		for (int i = 0; i < IMAPMimeMultipart_getCount(mmp); ++i) {
			final BodyPart bp = IMAPMimeMultipart_getBodyPart(mmp, i);
			if (IMAPBodyPart_getContentType(bp).toLowerCase().contains(attachmentId)) {
				kolabAttachmentIndex = i;
			}
		}
		if (kolabAttachmentIndex == -1)
			return null;
		BodyPart bp = IMAPMimeMultipart_getBodyPart(mmp, kolabAttachmentIndex);
		final InputStream in = IMAPBodyPart_getInputStream(bp);
		byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
		if (byteArray == null)
			return null;
		Attachment attachment = new Attachment();
		attachment.setContent(byteArray);
		// get content type
		String[] cta = IMAPBodyPart_getHeader(bp, "Content-Type");
		// avoid upper case in getContentType()
		if (cta != null && cta.length > 0) {
			String ct = cta[0];
			// cut off name attribute if set
			int idx = ct.indexOf(';');
			if (idx >= 0)
				ct = ct.substring(0, idx);
			attachment.setContentType(ct);
		}
		return attachment;
	}

	public static final long
	addAttachment(Session session, IMAPFolder folder, final long uid,
	    Attachment attachment, final String attachmentId)
	throws MessagingException, IOException, KolabParseException
	{
		if (attachment == null || attachment.getContent() == null || attachment.getContentType() == null)
			throw new IllegalArgumentException("attachment object is incomplete");
		//		InputStream is = getKolabXmlStream(folder, uid);
		//		if (is == null)
		//			throw new NullPointerException();
		//		String xmlcontent = is.toString();
		Message newMessage = IMAPMimeMessage_new(session);
		Message message = IMAPFFolder_getMessageByUID(folder, uid);
		//		message.set
		//		message.saveChanges();
		Object content = IMAPMessage_getContent(message);
		if (content instanceof MimeMultipart) {
			MimeMultipart multipart = (MimeMultipart)content;
			InternetHeaders attachmentBodyHeaders = IMAPInternetHeaders_new();
			// TODO Allow generic
			String contentType = attachment.getContentType();
			IMAPInternetHeaders_addHeaderLine(attachmentBodyHeaders,
			    "Content-Type: " + contentType + "; name=\"" + attachmentId + "\"");
			IMAPInternetHeaders_addHeaderLine(attachmentBodyHeaders,
			    "Content-Disposition: attachment; filename=\"" + attachmentId + "\"");
			IMAPInternetHeaders_addHeaderLine(attachmentBodyHeaders, "Content-Transfer-Encoding: base64");

			byte[] b64 = Base64.encodeBase64(attachment.getContent(), true);
			MimeBodyPart attachmentBody = new MimeBodyPart(attachmentBodyHeaders, b64);
			attachmentBody.setDescription(attachmentId);
			attachmentBody.setFileName(attachmentId);
			multipart.addBodyPart(attachmentBody);
			IMAPMessage_setContent(newMessage, multipart);

			for (Enumeration<Header> headerLines = IMAPMessage_getAllHeaders(message);
			    headerLines.hasMoreElements(); ) {
				final Header headerLine = (Header)headerLines.nextElement();
				IMAPMessage_setHeader(newMessage, headerLine.getName(), headerLine.getValue());
			}

			IMAPMessage_setSubject(newMessage, IMAPMessage_getSubject(message));

			// delete old mail and if we can’t don’t write new mail:
			if (!deleteKolabMail(folder, uid, false))
				throw new RuntimeException(
				    "cant delete old mail, so we won't write a new mail with the attachment");

			AppendUID[] uids = IMAPFolder_appendUIDMessages(folder, new Message[] { newMessage });

			if (uids.length != 1)
				throw new RuntimeException("unexpected uid array size");

			return uids[0].uid;
		}
		throw new
		    RuntimeException("unexpected content in mail message where " +
		    "we want to store the attachment" + attachment.toString());
	}

	public static final long
	removeAttachment(Session session, IMAPFolder folder, final long uid,
	    final String attachmentId)
	throws MessagingException, IOException, KolabParseException
	{
		Message newMessage = IMAPMimeMessage_new(session);
		Message message = IMAPFFolder_getMessageByUID(folder, uid);
		Object content = IMAPMessage_getContent(message);
		if (content instanceof MimeMultipart) {
			MimeMultipart multipart = (MimeMultipart)content;
			// remove attachment for given attachmentId, fi "photo.attachment"
			// failing is non fatal, if we dont find the proper attachment id
			// we simply attach a new bodypart
			for (int i = 1; i < IMAPMimeMultipart_getCount(multipart); ++i) {
				BodyPart bodyPart = IMAPMimeMultipart_getBodyPart(multipart, i);
				if (IMAPBodyPart_getContentType(bodyPart).contains(attachmentId)) {
					IMAPMimeMultipart_removeBodyPart(multipart, i);
				}
			}

			IMAPMessage_setContent(newMessage, multipart);

			for (Enumeration<Header> headerLines = IMAPMessage_getAllHeaders(message);
			    headerLines.hasMoreElements(); ) {
				final Header headerLine = (Header)headerLines.nextElement();
				IMAPMessage_setHeader(newMessage, headerLine.getName(), headerLine.getValue());
			}

			IMAPMessage_setSubject(newMessage, message.getSubject());

			// delete old mail and if we can’t don’t write new mail:
			if (!deleteKolabMail(folder, uid, false))
				throw new RuntimeException(
				    "cant delete old mail, so we won't write a new mail with the attachment");

			AppendUID[] uids = IMAPFolder_appendUIDMessages(folder, new Message[] { newMessage });

			if (uids.length != 1)
				throw new RuntimeException("unexpected uid array size");

			return uids[0].uid;
		}
		throw new
		    RuntimeException("unexpected content in mail message where " +
		    "we want to remove the attachment with id" + attachmentId);
	}

	public static final long
	updateAttachment(Session session, IMAPFolder folder, final long uid,
	    Attachment attachment, final String attachmentId)
	throws MessagingException, IOException, KolabParseException
	{
		if (attachment == null || attachment.getContent() == null || attachment.getContentType() == null)
			throw new IllegalArgumentException("attachment object is incomplete");
		Message newMessage = IMAPMimeMessage_new(session);
		Message message = IMAPFFolder_getMessageByUID(folder, uid);
		Object content = IMAPMessage_getContent(message);
		if (content instanceof MimeMultipart) {
			MimeMultipart multipart = (MimeMultipart)content;
			// remove attachment for given attachmentId, fi "photo.attachment"
			// failing is non fatal, if we dont find the proper attachment id
			// we simply attach a new bodypart
			for (int i = 1; i < IMAPMimeMultipart_getCount(multipart); ++i) {
				BodyPart bodyPart = IMAPMimeMultipart_getBodyPart(multipart, i);
				if (IMAPBodyPart_getContentType(bodyPart).contains(attachmentId)) {
					IMAPMimeMultipart_removeBodyPart(multipart, i);
				}
			}
			InternetHeaders attachmentBodyHeaders = IMAPInternetHeaders_new();
			String contentType = attachment.getContentType();
			IMAPInternetHeaders_addHeaderLine(attachmentBodyHeaders,
			    "Content-Type: " + contentType + "; name=\"" + attachmentId + "\"");
			IMAPInternetHeaders_addHeaderLine(attachmentBodyHeaders,
			    "Content-Disposition: attachment; filename=\"" + attachmentId + "\"");
			IMAPInternetHeaders_addHeaderLine(attachmentBodyHeaders, "Content-Transfer-Encoding: base64");

			byte[] b64 = Base64.encodeBase64(attachment.getContent(), true);
			MimeBodyPart attachmentBody = IMAPMimeBodyPart_new(attachmentBodyHeaders, b64);
			IMAPMimeBodyPart_setDescription(attachmentBody, attachmentId);
			IMAPMimeBodyPart_setFileName(attachmentBody, attachmentId);
			IMAPMimeMultipart_addBodyPart(multipart, attachmentBody);
			IMAPMessage_setContent(newMessage, multipart);

			for (Enumeration<Header> headerLines = IMAPMessage_getAllHeaders(message);
			    headerLines.hasMoreElements(); ) {
				final Header headerLine = (Header)headerLines.nextElement();
				IMAPMessage_setHeader(newMessage, headerLine.getName(), headerLine.getValue());
			}

			IMAPMessage_setSubject(newMessage, message.getSubject());

			// delete old mail and if we can’t don’t write new mail:
			if (!deleteKolabMail(folder, uid, false))
				throw new RuntimeException(
				    "cant delete old mail, so we won't write a new mail with the attachment");

			AppendUID[] uids = IMAPFolder_appendUIDMessages(folder, new Message[] { newMessage });

			if (uids.length != 1)
				throw new RuntimeException("unexpected uid array size");

			return uids[0].uid;
		}
		throw new
		    RuntimeException("unexpected content in mail message where " +
		    "we want to store the attachment" + attachment.toString());
	}

	public static final long
	writeKolabMailWithAttachment(Session session, IMAPFolder folder,
	    final CollaborationItem item, String senderAddress, Attachment attachment)
	throws AddressException, MessagingException
	{
		if (attachment == null)
			return writeKolabMail(session, folder, item, senderAddress);

		//		final long id = writeKolabMail(session, folder, item, senderAddress);
		if (item.getUid() == null)
			throw new NullPointerException("uid must not be null");
		Message msg = IMAPMimeMessage_new(session);
		IMAPMessage_setFrom(msg, IMAPInternetAddress_new(senderAddress));
		InternetAddress addressTo = IMAPInternetAddress_new(senderAddress);
		msg.setRecipient(Message.RecipientType.TO, addressTo);
		msg.setSubject(item.getUid());
		msg.setHeader("User-Agent", PRODUCT_ID + " " + Helper.getVersion());
		String type;
		if (item instanceof Event)
			type = TYPE_EVENT;
		else if (item instanceof Contact)
			type = TYPE_CONTACT;
		else if (item instanceof Note)
			type = TYPE_NOTE;
		else if (item instanceof Task)
			type = TYPE_TASK;
		else
			throw new RuntimeException("unhandled type for class " + item.getClass().getName());
		type = KOLAB_TYPE_PREFIX + type;
		msg.setHeader("X-Kolab-Type", type);
		msg.setSentDate(new Date());

		MimeMultipart multipart = new MimeMultipart();

		MimeBodyPart textPart = new MimeBodyPart();
		textPart.setText(MAIL_CONTENT, "UTF-8");

		multipart.addBodyPart(textPart);

		BodyPart kolabXmlPart = new MimeBodyPart();
		;
		kolabXmlPart.setFileName("kolab.xml");           // gibt dem Anhang einen Namen

		item.setLastModificationDate(new Date());
		if (item.getCreationDate() == null)
			item.setCreationDate(item.getLastModificationDate());

		final String idemprodid = item.getProductId();
		if (idemprodid == null || !idemprodid.startsWith("SimKolab "))
			item.setProductId(PRODUCT_ID + " " + Helper.getVersion());

		KolabSerializerDataSource ds = new KolabSerializerDataSource(item, type);

		kolabXmlPart.setDataHandler(new DataHandler(ds));
		multipart.addBodyPart(kolabXmlPart);

		final InputStream attachmentIs = IOUtils.toInputStream(attachment.getContent().toString());
		MimeBodyPart attachmentPart = new MimeBodyPart(attachmentIs);
		//FIXME allow generic
		attachmentPart.setFileName("photo.attachment");
		multipart.addBodyPart(attachmentPart);

		msg.setContent(multipart);

		AppendUID[] uids = IMAPFolder_appendUIDMessages(folder, new Message[] { msg });

		if (ds.getIoException() != null)
			throw new RuntimeException(ds.getIoException());
		if (ds.getXmlStreamException() != null)
			throw new RuntimeException(ds.getXmlStreamException());

		if (uids.length != 1)
			throw new RuntimeException("unexpected uid array size");
		return uids[0].uid;
	}

	/**
	 * Create a new email on the imap server which holds the specified
	 * {@link CollaborationItem}.
	 *
	 * @return The imap uid of the created email.
	 */
	public static final long
	writeKolabMail(Session session, IMAPFolder folder,
	    final CollaborationItem item, String senderAddress)
	throws AddressException, MessagingException
	{
		if (item.getUid() == null)
			throw new NullPointerException("uid must not be null");
		Message msg = IMAPMimeMessage_new(session);
		IMAPMessage_setFrom(msg, IMAPInternetAddress_new(senderAddress));
		InternetAddress addressTo = IMAPInternetAddress_new(senderAddress);
		msg.setRecipient(Message.RecipientType.TO, addressTo);
		msg.setSubject(item.getUid());
		msg.setHeader("User-Agent", PRODUCT_ID + " " + Helper.getVersion());
		String type;
		if (item instanceof Event)
			type = TYPE_EVENT;
		else if (item instanceof Contact)
			type = TYPE_CONTACT;
		else if (item instanceof Note)
			type = TYPE_NOTE;
		else if (item instanceof Task)
			type = TYPE_TASK;
		else
			throw new RuntimeException("unhandled type for class " + item.getClass().getName());
		type = KOLAB_TYPE_PREFIX + type;
		msg.setHeader("X-Kolab-Type", type);
		msg.setSentDate(new Date());

		MimeMultipart multipart = new MimeMultipart();

		MimeBodyPart textPart = new MimeBodyPart();
		textPart.setText(MAIL_CONTENT, "UTF-8");

		multipart.addBodyPart(textPart);

		BodyPart kolabXmlPart = new MimeBodyPart();
		;
		kolabXmlPart.setFileName("kolab.xml");           // gibt dem Anhang einen Namen

		item.setLastModificationDate(new Date());
		if (item.getCreationDate() == null)
			item.setCreationDate(item.getLastModificationDate());

		final String idemprodid = item.getProductId();
		if (idemprodid == null || !idemprodid.startsWith("SimKolab "))
			item.setProductId(PRODUCT_ID + " " + Helper.getVersion());

		KolabSerializerDataSource ds = new KolabSerializerDataSource(item, type);

		kolabXmlPart.setDataHandler(new DataHandler(ds));
		multipart.addBodyPart(kolabXmlPart);

		msg.setContent(multipart);

		AppendUID[] uids = IMAPFolder_appendUIDMessages(folder, new Message[] { msg });

		if (ds.getIoException() != null)
			throw new RuntimeException(ds.getIoException());
		if (ds.getXmlStreamException() != null)
			throw new RuntimeException(ds.getXmlStreamException());

		if (uids.length != 1)
			throw new RuntimeException("unexpected uid array size");
		return uids[0].uid;
	}

	private static String MAIL_CONTENT;

	static {
		try {
			MAIL_CONTENT = readTextResource("/org/evolvis/bsi/kolab/mailcontent");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String
	readTextResource(String name) throws IOException
	{
		InputStream resource = ImapHelper.class.getResourceAsStream(name);
		if (resource == null)
			throw new RuntimeException("resource " + name + " was no found");
		StringBuilder sb = new StringBuilder(1024);
		BufferedReader reader = new BufferedReader(new InputStreamReader(resource, "UTF-8"));
		char[] buf = new char[1024];
		for (int numRead = 0; (numRead = reader.read(buf)) != -1; )
			sb.append(String.valueOf(buf, 0, numRead));
		reader.close();
		return sb.toString();
	}

	public static boolean
	deleteKolabMail(IMAPFolder folder, Long mailId, boolean expunge) throws MessagingException
	{
		if (!IMAPFolder_isOpen(folder))
			IMAPFolder_open(folder, Folder.READ_WRITE);
		else if (IMAPFFolder_getMode(folder) != Folder.READ_WRITE) {
			IMAPFolder_close(folder, false);
			IMAPFolder_open(folder, Folder.READ_WRITE);
		}
		Message msg = IMAPFFolder_getMessageByUID(folder, mailId);
		if (msg == null)
			return false;
		IMAPMessage_setFlag(msg, Flag.DELETED, true);
		if (expunge)
			IMAPFolder_expunge(folder);
		return true;
	}
}
