package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.CollaborationItem;

import javax.activation.DataSource;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * Data structure which is needed to serialize {@link CollaborationItem}s to
 * an email xml attachment.
 * A thread is opened to pipe the xml stream.
 *
 * @author Hendrik Helwich
 */
public class KolabSerializerDataSource implements DataSource {
	private CollaborationItem item;
	private XMLStreamException xmlStreamException;
	private IOException ioException;
	private String contentType;

	public KolabSerializerDataSource(CollaborationItem item, String contentType)
	{
		this.item = item;
		this.contentType = contentType;
	}

	@Override
	public String
	getContentType()
	{
		return contentType;
	}

	@Override
	public InputStream
	getInputStream() throws IOException
	{
		final PipedInputStream in = new PipedInputStream();
		final PipedOutputStream out = new PipedOutputStream(in);
		new Thread(new Runnable() {
			public void run()
			{
				try {
					KolabSerializer.write(item, out);
				} catch (XMLStreamException e) {
					xmlStreamException = e;
				} finally {
					try {
						out.close();
					} catch (IOException e) {
						ioException = e;
					}
				}
			}
		}).start();
		return in;
	}

	@Override
	public String
	getName()
	{
		return null;
	}

	@Override
	public OutputStream
	getOutputStream() throws IOException
	{
		throw new UnsupportedOperationException();
	}

	public XMLStreamException
	getXmlStreamException()
	{
		return xmlStreamException;
	}

	public IOException
	getIoException()
	{
		return ioException;
	}
}
