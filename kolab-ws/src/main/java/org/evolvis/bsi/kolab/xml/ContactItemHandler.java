package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.Address;
import org.evolvis.bsi.kolab.service.AddressType;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.Gender;
import org.evolvis.bsi.kolab.service.Phone;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.evolvis.bsi.kolab.service.PreferredAddress;
import org.evolvis.bsi.kolab.service.Validator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.text.ParseException;
import java.util.Locale;

import static org.evolvis.bsi.kolab.xml.Helper.parseDate;
import static org.evolvis.bsi.kolab.xml.Helper.parseDouble;

/**
 * Used to fill the kolab xml data which is contact specific to an {@link Contact}
 * instance.
 *
 * @author Hendrik Helwich
 */
public class ContactItemHandler extends ItemHandler<Contact> {
	private Contact contact = new Contact();

	@Override
	public void
	checkRootName(String name) throws KolabParseException
	{
		if (!ELEMENT_CONTACT.equals(name.toLowerCase(Locale.ENGLISH)))
			throw new KolabParseException("unexpected contact root element name: " + name);
	}

	@Override
	public boolean
	readElement(String name, XMLStreamReader parser)
	throws XMLStreamException, KolabParseException
	{
		if (readElementCollaborationItem(contact, name, parser))
			return true;
		else if (ELEMENT_NAME.equals(name.toLowerCase(Locale.ENGLISH))) {
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_NAME_GIVEN_NAME.equals(name.toLowerCase(Locale.ENGLISH)))
							contact.setGivenName(parser.getElementText());
						else if (ELEMENT_NAME_MIDDLE_NAMES.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							contact.setMiddleNames(parser.getElementText());
						else if (ELEMENT_NAME_LAST_NAME.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							contact.setLastName(parser.getElementText());
						else if (ELEMENT_NAME_FULL_NAME.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							contact.setFullName(parser.getElementText());
						else if (ELEMENT_NAME_INITIALS.equals(name.toLowerCase(Locale.ENGLISH)))
							contact.setInitials(parser.getElementText());
						else if (ELEMENT_NAME_PREFIX.equals(name.toLowerCase(Locale.ENGLISH)))
							contact.setPrefix(parser.getElementText());
						else if (ELEMENT_NAME_SUFFIX.equals(name.toLowerCase(Locale.ENGLISH)))
							contact.setSuffix(parser.getElementText());
						else {
							logger.info("unhandled kolab data element: " +
							    ELEMENT_NAME + "/" + name); //TODO extract operation
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
		} else if (ELEMENT_FREE_BUSY_URL.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setFreeBusyUrl(parser.getElementText());
		else if (ELEMENT_ORGANIZATION.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setOrganization(parser.getElementText());
		else if (ELEMENT_WEB_PAGE.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setWebPage(parser.getElementText());
		else if (ELEMENT_IM_ADDRESS.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setImAddress(parser.getElementText());
		else if (ELEMENT_DEPARTMENT.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setDepartment(parser.getElementText());
		else if (ELEMENT_OFFICE_LOCATION.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setOfficeLocation(parser.getElementText());
		else if (ELEMENT_PROFESSION.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setProfession(parser.getElementText());
		else if (ELEMENT_JOB_TITLE.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setJobTitle(parser.getElementText());
		else if (ELEMENT_MANAGER_NAME.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setManagerName(parser.getElementText());
		else if (ELEMENT_ASSISTANT.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setAssistant(parser.getElementText());
		else if (ELEMENT_NICK_NAME.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setNickName(parser.getElementText());
		else if (ELEMENT_SPOUSE_NAME.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setSpouseName(parser.getElementText());
		else if (ELEMENT_BIRTHDAY.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setBirthday(parseDate(parser.getElementText()));
		else if (ELEMENT_ANNIVERSARY.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setAnniversary(parseDate(parser.getElementText()));
		else if (ELEMENT_PICTURE.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setPictureAttachmentId(parser.getElementText());
		else if (ELEMENT_CHILDREN.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setChildren(parser.getElementText());
		else if (ELEMENT_GENDER.equals(name.toLowerCase(Locale.ENGLISH))) {
			String gender = parser.getElementText();
			if (VALUE_GENDER_MALE.equals(gender.toLowerCase(Locale.ENGLISH)))
				contact.setGender(Gender.MALE);
			else if (VALUE_GENDER_FEMALE.equals(gender.toLowerCase(Locale.ENGLISH)))
				contact.setGender(Gender.FEMALE);
			else
				throw new KolabParseException("invalid gender value: '" + gender + "'");
		} else if (ELEMENT_LANGUAGE.equals(name.toLowerCase(Locale.ENGLISH)))
			contact.setLanguage(parser.getElementText());
		else if (ELEMENT_PHONE.equals(name.toLowerCase(Locale.ENGLISH))) {
			Phone phone = new Phone();
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_PHONE_NUMBER.equals(name.toLowerCase(Locale.ENGLISH)))
							phone.setNumber(parser.getElementText());
						else if (ELEMENT_PHONE_TYPE.equals(name.toLowerCase(Locale.ENGLISH))) {
							String pt = parser.getElementText();
							PhoneType ptype = null;
							if (VALUE_PHONE_TYPE_BUSINESS_1.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.BUSINESS_1;
							else if (VALUE_PHONE_TYPE_BUSINESS_2.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.BUSINESS_2;
							else if (VALUE_PHONE_TYPE_BUSINESSFAX.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.BUSINESSFAX;
							else if (VALUE_PHONE_TYPE_CALLBACK.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.CALLBACK;
							else if (VALUE_PHONE_TYPE_CAR.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.CAR;
							else if (VALUE_PHONE_TYPE_COMPANY.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.COMPANY;
							else if (VALUE_PHONE_TYPE_HOME_1.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.HOME_1;
							else if (VALUE_PHONE_TYPE_HOME_2.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.HOME_2;
							else if (VALUE_PHONE_TYPE_HOMEFAX.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.HOMEFAX;
							else if (VALUE_PHONE_TYPE_ISDN.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.ISDN;
							else if (VALUE_PHONE_TYPE_MOBILE.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.MOBILE;
							else if (VALUE_PHONE_TYPE_PAGER.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.PAGER;
							else if (VALUE_PHONE_TYPE_PRIMARY.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.PRIMARY;
							else if (VALUE_PHONE_TYPE_RADIO.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.RADIO;
							else if (VALUE_PHONE_TYPE_TELEX.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.TELEX;
							else if (VALUE_PHONE_TYPE_TTYTDD.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.TTYTDD;
							else if (VALUE_PHONE_TYPE_ASSISTANT.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.ASSISTANT;
							else if (VALUE_PHONE_TYPE_OTHER.equals(
							    pt.toLowerCase(Locale.ENGLISH)))
								ptype = PhoneType.OTHER;
							else
								throw new KolabParseException(
								    "invalid phone type value '" + pt + "'");
							phone.setType(ptype);
						} else {
							logger.info("unhandled kolab data element: " +
							    ELEMENT_PHONE + "/" + name); //TODO extract operation
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
			contact.getPhone().add(phone);
		} else if (ELEMENT_EMAIL.equals(name.toLowerCase(Locale.ENGLISH))) {
			Email email = new Email();
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_EMAIL_DISPLAY_NAME.equals(name.toLowerCase(Locale.ENGLISH)))
							email.setDisplayName(parser.getElementText());
						else if (ELEMENT_EMAIL_SMTP_ADDRESS.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							email.setSmtpAddress(parser.getElementText());
						else {
							logger.info("unhandled kolab data element: " +
							    ELEMENT_EMAIL + "/" + name); //TODO extract operation
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
			contact.getEmail().add(email);
		} else if (ELEMENT_ADDRESS.equals(name.toLowerCase(Locale.ENGLISH))) {
			Address address = new Address();
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_ADDRESS_TYPE.equals(name.toLowerCase(Locale.ENGLISH))) {
							String adtstr = parser.getElementText();
							AddressType adt = null;
							if (VALUE_ADDRESS_TYPE_HOME.equals(
							    adtstr.toLowerCase(Locale.ENGLISH)))
								adt = AddressType.HOME;
							else if (VALUE_ADDRESS_TYPE_BUSINESS.equals(
							    adtstr.toLowerCase(Locale.ENGLISH)))
								adt = AddressType.BUSINESS;
							else if (VALUE_ADDRESS_TYPE_OTHER.equals(
							    adtstr.toLowerCase(Locale.ENGLISH)))
								adt = AddressType.OTHER;
							else
								throw new KolabParseException(
								    "invalid address type value '" + adtstr + "'");
							address.setType(adt);
						} else if (ELEMENT_ADDRESS_STREET.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							address.setStreet(parser.getElementText());
						else if (ELEMENT_ADDRESS_LOCALITY.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							address.setLocality(parser.getElementText());
						else if (ELEMENT_ADDRESS_REGION.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							address.setRegion(parser.getElementText());
						else if (ELEMENT_ADDRESS_POSTAL_CODE.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							address.setPostalCode(parser.getElementText());
						else if (ELEMENT_ADDRESS_COUNTRY.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							address.setCountry(parser.getElementText());
						else {
							logger.info("unhandled kolab data element: " +
							    ELEMENT_ADDRESS + "/" + name); //TODO extract operation
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
			contact.getAddress().add(address);
		} else if (ELEMENT_PREFERRED_ADDRESS.equals(name.toLowerCase(Locale.ENGLISH))) {
			String padtstr = parser.getElementText();
			PreferredAddress padt = null;
			if (VALUE_ADDRESS_TYPE_HOME.equals(padtstr.toLowerCase(Locale.ENGLISH)))
				padt = PreferredAddress.HOME;
			else if (VALUE_ADDRESS_TYPE_BUSINESS.equals(padtstr.toLowerCase(Locale.ENGLISH)))
				padt = PreferredAddress.BUSINESS;
			else if (VALUE_ADDRESS_TYPE_OTHER.equals(padtstr.toLowerCase(Locale.ENGLISH)))
				padt = PreferredAddress.OTHER;
			else if (VALUE_PREFERRED_ADDRESS_NONE.equals(padtstr.toLowerCase(Locale.ENGLISH)))
				padt = PreferredAddress.NONE;
			else
				throw new KolabParseException("invalid preferred address type value '" + padtstr + "'");
			contact.setPreferredAddress(padt);
		} else if (ELEMENT_LATITUDE.equals(name.toLowerCase(Locale.ENGLISH)))
			try {
				contact.setLatitude(parseDouble(parser.getElementText()));
			} catch (ParseException e) {
				throw new KolabParseException(e);
			}
		else if (ELEMENT_LONGITUDE.equals(name.toLowerCase(Locale.ENGLISH)))
			try {
				contact.setLongitude(parseDouble(parser.getElementText()));
			} catch (ParseException e) {
				throw new KolabParseException(e);
			}
		else
			return false;
		return true;
	}

	@Override
	public void
	validate() throws KolabParseException
	{
		try {
			Validator.validate(contact);
		} catch (IllegalArgumentException e) {
			throw new KolabParseException(e.getMessage());
		}
	}

	@Override
	public Contact
	getItem()
	{
		return contact;
	}
}
