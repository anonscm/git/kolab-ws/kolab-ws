package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Internally used {@link Query} proxy.
 *
 * @author Hendrik Helwich
 * @see ProfilerEntityManager
 */
class ProfilerQuery implements Query {
	/**
	 * Namespace of the SQL queries which is used as a class name on top of the
	 * method stack.
	 * SQL queries are used as method name.
	 */
	private static final String SQL_SPACE = "org.evolvis.bsi.kolab.SQL";

	private final String id;
	private final Query delegate;

	public ProfilerQuery(String id, Query delegate)
	{
		this.id = id;
		this.delegate = delegate;
	}

	// --------------------------------------------------- profile dbms requests

	@Override
	public List<?>
	getResultList()
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return delegate.getResultList();
		return (List<?>)Profiler.profileInvoke(SQL_SPACE,
		    id, delegate, "getResultList");
	}

	@Override
	public Object
	getSingleResult()
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return delegate.getSingleResult();
		return Profiler.profileInvoke(SQL_SPACE, id,
		    delegate, "getSingleResult");
	}

	@Override
	public int
	executeUpdate()
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return delegate.executeUpdate();
		return (Integer)Profiler.profileInvoke(SQL_SPACE,
		    id, delegate, "executeUpdate");
	}

	// ----------------------------------------------------- delegate operations

	@Override
	public Query
	setMaxResults(int maxResult)
	{
		return delegate.setMaxResults(maxResult);
	}

	@Override
	public Query
	setFirstResult(int startPosition)
	{
		return delegate.setFirstResult(startPosition);
	}

	@Override
	public Query
	setHint(String hintName, Object value)
	{
		return delegate.setHint(hintName, value);
	}

	@Override
	public Query
	setParameter(String name, Object value)
	{
		return delegate.setParameter(name, value);
	}

	@Override
	public Query
	setParameter(String name, Date value, TemporalType temporalType)
	{
		return delegate.setParameter(name, value, temporalType);
	}

	@Override
	public Query
	setParameter(String name, Calendar value, TemporalType temporalType)
	{
		return delegate.setParameter(name, value, temporalType);
	}

	@Override
	public Query
	setParameter(int position, Object value)
	{
		return delegate.setParameter(position, value);
	}

	@Override
	public Query
	setParameter(int position, Date value, TemporalType temporalType)
	{
		return delegate.setParameter(position, value, temporalType);
	}

	@Override
	public Query
	setParameter(int position, Calendar value, TemporalType temporalType)
	{
		return delegate.setParameter(position, value, temporalType);
	}

	@Override
	public Query
	setFlushMode(FlushModeType flushMode)
	{
		return delegate.setFlushMode(flushMode);
	}

	@Override
	public int
	getMaxResults()
	{
		return delegate.getMaxResults();
	}

	@Override
	public int
	getFirstResult()
	{
		return delegate.getFirstResult();
	}

	@Override
	public Map<String, Object>
	getHints()
	{
		return delegate.getHints();
	}

	@Override
	public <T> Query
	setParameter(Parameter<T> param, T value)
	{
		return delegate.setParameter(param, value);
	}

	@Override
	public Query
	setParameter(Parameter<Calendar> param, Calendar value, TemporalType temporalType)
	{
		return delegate.setParameter(param, value, temporalType);
	}

	@Override
	public Query
	setParameter(Parameter<Date> param, Date value, TemporalType temporalType)
	{
		return delegate.setParameter(param, value, temporalType);
	}

	@Override
	public Set<Parameter<?>>
	getParameters()
	{
		return delegate.getParameters();
	}

	@Override
	public Parameter<?>
	getParameter(String name)
	{
		return delegate.getParameter(name);
	}

	@Override
	public <T> Parameter<T>
	getParameter(String name, Class<T> type)
	{
		return delegate.getParameter(name, type);
	}

	@Override
	public Parameter<?>
	getParameter(int position)
	{
		return delegate.getParameter(position);
	}

	@Override
	public <T> Parameter<T>
	getParameter(int position, Class<T> type)
	{
		return delegate.getParameter(position, type);
	}

	@Override
	public boolean
	isBound(Parameter<?> param)
	{
		return delegate.isBound(param);
	}

	@Override
	public <T> T
	getParameterValue(Parameter<T> param)
	{
		return delegate.getParameterValue(param);
	}

	@Override
	public Object
	getParameterValue(String name)
	{
		return delegate.getParameterValue(name);
	}

	@Override
	public Object
	getParameterValue(int position)
	{
		return delegate.getParameterValue(position);
	}

	@Override
	public FlushModeType
	getFlushMode()
	{
		return delegate.getFlushMode();
	}

	@Override
	public Query
	setLockMode(LockModeType lockMode)
	{
		return delegate.setLockMode(lockMode);
	}

	@Override
	public LockModeType
	getLockMode()
	{
		return delegate.getLockMode();
	}

	@Override
	public <T> T
	unwrap(Class<T> cls)
	{
		return delegate.unwrap(cls);
	}
}
