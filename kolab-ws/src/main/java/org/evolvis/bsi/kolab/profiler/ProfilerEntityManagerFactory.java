package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import java.util.Map;

/**
 * Internally used {@link EntityManagerFactory} proxy.
 *
 * @author Hendrik Helwich
 * @see Profiler#profileQueryExecution(javax.persistence.EntityManagerFactory)
 */
class ProfilerEntityManagerFactory implements EntityManagerFactory {
	private final EntityManagerFactory delegate;

	public ProfilerEntityManagerFactory(EntityManagerFactory delegate)
	{
		this.delegate = delegate;
	}

	// ------------------------------------------- wrap returned entity managers

	@Override
	public EntityManager
	createEntityManager()
	{
		return new ProfilerEntityManager(delegate.createEntityManager());
	}

	@Override
	public EntityManager
	createEntityManager(@SuppressWarnings("rawtypes") Map map)
	{
		return new ProfilerEntityManager(delegate.createEntityManager(map));
	}

	// ----------------------------------------------------- delegate operations

	@Override
	public void
	close()
	{
		delegate.close();
	}

	@Override
	public boolean
	isOpen()
	{
		return delegate.isOpen();
	}

	@Override
	public EntityManager
	createEntityManager(SynchronizationType synchronizationType)
	{
		return delegate.createEntityManager(synchronizationType);
	}

	@Override
	public EntityManager
	createEntityManager(SynchronizationType synchronizationType, Map map)
	{
		return delegate.createEntityManager(synchronizationType, map);
	}

	@Override
	public CriteriaBuilder
	getCriteriaBuilder()
	{
		return delegate.getCriteriaBuilder();
	}

	@Override
	public Metamodel
	getMetamodel()
	{
		return delegate.getMetamodel();
	}

	@Override
	public Map<String, Object>
	getProperties()
	{
		return delegate.getProperties();
	}

	@Override
	public Cache
	getCache()
	{
		return delegate.getCache();
	}

	@Override
	public PersistenceUnitUtil
	getPersistenceUnitUtil()
	{
		return delegate.getPersistenceUnitUtil();
	}

	@Override
	public void
	addNamedQuery(String name, Query query)
	{
		delegate.addNamedQuery(name, query);
	}

	@Override
	public <T> T
	unwrap(Class<T> cls)
	{
		return delegate.unwrap(cls);
	}

	@Override
	public <T> void
	addNamedEntityGraph(String graphName, EntityGraph<T> entityGraph)
	{
		delegate.addNamedEntityGraph(graphName, entityGraph);
	}
}
