package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.ColorLabel;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.ShowTimeAs;
import org.evolvis.bsi.kolab.service.Validator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.Date;
import java.util.Locale;

/**
 * Used to fill the kolab xml data which is event specific to an {@link Event}
 * instance.
 *
 * @author Hendrik Helwich
 */
public class EventItemHandler extends ItemHandler<Event> {
	private Event event = new Event();

	@Override
	public void
	checkRootName(String name) throws KolabParseException
	{
		if (!ELEMENT_EVENT.equals(name.toLowerCase(Locale.ENGLISH)))
			throw new KolabParseException("unexpected event root element name: " + name);
	}

	@Override
	public boolean
	readElement(String name, XMLStreamReader parser)
	throws XMLStreamException, KolabParseException
	{
		if (readElementIncidence(event, name, parser))
			return true;
		else if (ELEMENT_SHOW_TIME_AS.equals(name.toLowerCase(Locale.ENGLISH))) {
			String sta = parser.getElementText();
			if (VALUE_SHOW_TIME_AS_FREE.equals(sta.toLowerCase(Locale.ENGLISH)))
				event.setShowTimeAs(ShowTimeAs.FREE);
			else if (VALUE_SHOW_TIME_AS_TENTATIVE.equals(sta.toLowerCase(Locale.ENGLISH)))
				event.setShowTimeAs(ShowTimeAs.TENTATIVE);
			else if (VALUE_SHOW_TIME_AS_BUSY.equals(sta.toLowerCase(Locale.ENGLISH)))
				event.setShowTimeAs(ShowTimeAs.BUSY);
			else if (VALUE_SHOW_TIME_AS_OUTOFOFFICE.equals(sta.toLowerCase(Locale.ENGLISH)))
				event.setShowTimeAs(ShowTimeAs.OUTOFOFFICE);
			else
				throw new KolabParseException("invalid show-time-as value '" + sta + "'");
		} else if (ELEMENT_COLOR_LABEL.equals(name.toLowerCase(Locale.ENGLISH))) {
			String clbl = parser.getElementText();
			ColorLabel lbl;
			if (VALUE_COLOR_LABEL_NONE.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.NONE;
			else if (VALUE_COLOR_LABEL_IMPORTANT.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.IMPORTANT;
			else if (VALUE_COLOR_LABEL_BUSINESS.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.BUSINESS;
			else if (VALUE_COLOR_LABEL_PERSONAL.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.PERSONAL;
			else if (VALUE_COLOR_LABEL_VACATION.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.VACATION;
			else if (VALUE_COLOR_LABEL_MUST_ATTEND.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.MUST_ATTEND;
			else if (VALUE_COLOR_LABEL_TRAVEL_REQUIRED.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.TRAVEL_REQUIRED;
			else if (VALUE_COLOR_LABEL_NEEDS_PREPARATION.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.NEEDS_PREPARATION;
			else if (VALUE_COLOR_LABEL_BIRTHDAY.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.BIRTHDAY;
			else if (VALUE_COLOR_LABEL_ANNIVERSARY.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.ANNIVERSARY;
			else if (VALUE_COLOR_LABEL_PHONE_CALL.equals(clbl.toLowerCase(Locale.ENGLISH)))
				lbl = ColorLabel.PHONE_CALL;
			else
				throw new KolabParseException("invalid color-label value '" + clbl + "'");
			event.setColorLabel(lbl);
		} else if (ELEMENT_END_DATE.equals(name.toLowerCase(Locale.ENGLISH))) {
			Date date = event.isAllDay() ? readDate(parser) : readDateTime(parser);
			event.setEndDate(date);
		} else
			return false;
		return true;
	}

	@Override
	public void
	validate() throws KolabParseException
	{
		try {
			Validator.validate(event);
		} catch (IllegalArgumentException e) {
			throw new KolabParseException(e.getMessage());
		}
	}

	@Override
	public Event
	getItem()
	{
		return event;
	}
}
