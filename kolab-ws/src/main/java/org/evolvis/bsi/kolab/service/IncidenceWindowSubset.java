package org.evolvis.bsi.kolab.service;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;

import java.util.Date;

/**
 * @author Hendrik Helwich
 */
public class IncidenceWindowSubset {
	private static final String CONF_CALENDAR_WINDOW_PAST_DAYS = "calendar.windowPastDays";
	private static final String CONF_CALENDAR_WINDOW_FUTURE_DAYS = "calendar.windowFutureDays";

	public static final int DEFAULT_SYNC_WINDOW_PAST_DAYS = 60;
	public static final int DEFAULT_SYNC_WINDOW_FUTURE_DAYS = 60;

	private static final Logger logger = Logger.getLogger(IncidenceWindowSubset.class.getName());

	public static final int
	getSyncWindowPastDays()
	{
		return getSyncWindowParam(CONF_CALENDAR_WINDOW_PAST_DAYS, DEFAULT_SYNC_WINDOW_PAST_DAYS);
	}

	public static final int
	getSyncWindowFutureDays()
	{
		return getSyncWindowParam(CONF_CALENDAR_WINDOW_FUTURE_DAYS, DEFAULT_SYNC_WINDOW_FUTURE_DAYS);
	}

	public static final Date
	getSyncWindowStart()
	{
		return new Date(new Date().getTime() - getSyncWindowPastDays() * 1000L * 60 * 60 * 24);
	}

	public static final Date[]
	getSyncWindow(Date windowCenter)
	{
		Date[] window = new Date[2];
		window[0] = new Date(windowCenter.getTime() - getSyncWindowPastDays() * 1000L * 60 * 60 * 24);
		window[1] = new Date(windowCenter.getTime() + getSyncWindowFutureDays() * 1000L * 60 * 60 * 24);
		return window;
	}

	private static final int
	getSyncWindowParam(String confParam, int defaultValue)
	{
		String value = ServiceConf.get(confParam);
		if (value != null) {
			try {
				int i = Integer.parseInt(value);
				if (i >= 0)
					return i;
			} catch (NumberFormatException e) {
				// do nothing
			}
			logger.warn("parameter " + confParam + " holds an illegal value: " + value);
		}
		return defaultValue;
	}
}
