package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.service.ServiceConf;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;

/**
 * This thread should only be used by the {@link Profiler} class.
 * If the profiler is active, one instance of this thread could be started to
 * periodically log the memory usage of the JVM.
 *
 * @author Hendrik Helwich
 */
class MemoryMonitor extends Thread {
	private static final Logger logger = Logger.getLogger(MemoryMonitor.class.getName());

	private MemoryMXBean mbean;

	public MemoryMonitor()
	{
		mbean = ManagementFactory.getMemoryMXBean();
	}

	@Override
	public void
	run()
	{
		while (!isInterrupted()) {
			// get current time
			long time = System.currentTimeMillis();
			// get current memory usage
			MemoryUsage usage = mbean.getHeapMemoryUsage();
			// log memory usage
			Profiler.INSTANCE.logMemoryUsage(time, usage.getUsed(), usage.getCommitted(), usage.getMax());
			// sleep a configured interval
			try {
				String millis = ServiceConf.get("profiler.memory.interval");
				long millis_;
				try {
					millis_ = Long.parseLong(millis);
				} catch (NumberFormatException e) {
					logger.warn("profiler memory usage log interval is misconfigured");
					millis_ = 1000;
				}
				sleep(millis_);
			} catch (InterruptedException e) {
				break;
			}
		}
	}
}
