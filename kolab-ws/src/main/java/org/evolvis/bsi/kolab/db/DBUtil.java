package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.IdAndChangeTime;
import org.evolvis.bsi.kolab.service.Task;
import org.evolvis.bsi.kolab.util.DateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Hendrik Helwich
 */
public class DBUtil {
	private static final String QUERY_VISIBLE_ITEMS =
	    "SELECT DISTINCT i.kolabId FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.deleted=false AND i.valid=true";

	private static final String QUERY_VISIBLE_ITEMS_WITH_CHANGE_TIME =
	    "SELECT DISTINCT i.kolabId, i.changeDate_ FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.deleted=false AND i.valid=true";

	private static final String QUERY_NEW_ITEMS =
	    "SELECT DISTINCT i.kolabId FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.valid=true AND i.deleted=false AND (i.createDeviceId <> :deviceId OR i.createDeviceId IS NULL) AND i.creationDate_ > :since";

	private static final String QUERY_DELETED_ITEMS =
	    "SELECT DISTINCT i.kolabId FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.valid=true AND i.deleted=true AND i.changeDate_ > :since";

	private static final String QUERY_UPDATED_ITEMS =
	    "SELECT DISTINCT i.kolabId FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.valid=true AND i.deleted=false AND ( i.creationDate_ <= :since OR (i.createDeviceId = :deviceId AND i.createDeviceId IS NOT NULL)) AND i.changeDate_ > :since";

	private static final String QUERY_WHERE_CHANGE_USER =
	    " AND ( i.changeUserId IS NULL OR i.changeDeviceId IS NULL OR i.changeUserId <> :changeUserId OR i.changeDeviceId <> :changeDeviceId )";

	private static final String QUERY_NOT_DELETED_ITEMS =
	    "SELECT DISTINCT i FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.deleted=false ORDER BY i.mailId";

	private static final String QUERY_OUTDATED_EVENTS =
	    "SELECT DISTINCT i FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.deleted=true AND i.incidenceLastDate_ < :outWindow ORDER BY i.mailId";

	private static final String QUERY_SELECT_FOLDER_BY_NAME =
	    "SELECT DISTINCT f FROM ImapFolder f WHERE f.name=:name";

	private static final String QUERY_SELECT_ITEM_BY_KOLAB_ID =
	    "SELECT DISTINCT i FROM ImapFolder f JOIN f.items i WHERE f.name=:folderName AND i.kolabId=:kolabId";

	private static final String QUERY_SELECT_ITEM_BY_MAIL_ID =
	    "SELECT DISTINCT i FROM ImapFolder f JOIN f.items i WHERE f.name=:folderName AND i.mailId=:mailId";

	private static final String QUERY_DELETE_ITEM_BY_MAIL_ID =
	    "DELETE FROM KolabItem item WHERE item.mailId=:mailId AND EXISTS (SELECT f FROM item.folder f WHERE f.name=:folderName)";

	private static final String QUERY_SELECT_VISIBLE_ITEM_BY_KOLAB_ID =
	    "SELECT DISTINCT i FROM ImapFolder f JOIN f.items i WHERE f.name=:folderName AND i.kolabId=:kolabId AND i.deleted=false AND i.valid=true";

	private static final String QUERY_SELECT_VISIBLE_ITEM_BY_MAIL_ID =
	    "SELECT DISTINCT i FROM ImapFolder f JOIN f.items i WHERE f.name=:folderName AND i.mailId=:mailId AND i.deleted=false AND i.valid=true";

	private static final String QUERY_DELETE_FOLDER_ITEMS =
	    "DELETE FROM KolabItem item WHERE EXISTS (SELECT f FROM item.folder f WHERE f.name=:name)";

	private static final String QUERY_USER =
	    "SELECT DISTINCT u FROM User u WHERE u.name=:name";

	private static final String QUERY_SELECT_FOLDER_VISIBILITY =
	    "SELECT DISTINCT v FROM FolderVisibility v JOIN v.folder f JOIN v.user u WHERE f.name=:folderName AND u.name=:userName AND v.changeDate_ <= :time ORDER BY v.changeDate_ DESC";

	private static final String QUERY_SELECT_FOLDER_VISIBILITY_EQUAL =
	    "SELECT DISTINCT v FROM FolderVisibility v JOIN v.folder f JOIN v.user u WHERE f.name=:folderName AND u.name=:userName AND v.changeDate_ = :time";

	/**
	 * Get the IDs of all items which are in the imap folder at a previous time.
	 * IDs of items which are created after this time are also included. This is
	 * due to the fact, that funambol calls getNew*IDs and getDeleted*IDs after
	 * the known time.
	 */
	private static final String QUERY_VISIBLE_ITEMS_BEFORE =
	    "SELECT DISTINCT i.kolabId FROM ImapFolder f LEFT OUTER JOIN f.items i WHERE f.name=:name AND i.valid=true AND ( i.deleted=false OR ( i.deleted=true AND i.changeDate_ > :since ) )";

	@SuppressWarnings("unchecked")
	public static List<KolabItem>
	getNotDeltedItems(EntityManager entityManager, String folderName)
	{
		Query query = entityManager.createQuery(QUERY_NOT_DELETED_ITEMS);
		query.setParameter("name", folderName);
		List<?> result = query.getResultList();
		return (List<KolabItem>)result;
	}

	@SuppressWarnings("unchecked")
	public static List<KolabItem>
	getOutdatedEvents(EntityManager entityManager, String folderName, Long outWindow)
	{
		Query query = entityManager.createQuery(QUERY_OUTDATED_EVENTS);
		query.setParameter("name", folderName);
		query.setParameter("outWindow", outWindow);
		List<?> result = query.getResultList();
		return (List<KolabItem>)result;
	}

	@SuppressWarnings("unchecked")
	public static List<String>
	getVisibleItems(EntityManager entityManager, String folderName)
	{
		Query query = entityManager.createQuery(QUERY_VISIBLE_ITEMS);
		query.setParameter("name", folderName);
		List<?> result = query.getResultList();
		return (List<String>)result;
	}

	public static List<IdAndChangeTime>
	getVisibleItemsWithChangeTime(EntityManager entityManager, String folderName)
	{
		Query query = entityManager.createQuery(QUERY_VISIBLE_ITEMS_WITH_CHANGE_TIME);
		query.setParameter("name", folderName);
		List<?> result = query.getResultList();
		List<IdAndChangeTime> result_ = new ArrayList<IdAndChangeTime>(result.size());
		for (int i = 0; i < result.size(); i++) {
			IdAndChangeTime idcht = new IdAndChangeTime();
			Object[] tupel = (Object[])result.get(i);
			idcht.setID((String)tupel[0]);
			idcht.setChangeDate(new Date(((Long)tupel[1]).longValue()));
			result_.add(idcht);
		}
		return result_;
	}

	public static ImapFolder
	getFolder(EntityManager entityManager, String folderName)
	{
		ImapFolder folder = null;
		try {
			Query query = entityManager.createQuery(QUERY_SELECT_FOLDER_BY_NAME);
			query.setParameter("name", folderName);
			Object result = query.getSingleResult();
			folder = (ImapFolder)result;
		} catch (NoResultException e) {
			// nothing to do
		}
		return folder;
	}

	public static KolabItem
	getItemForKolabId(EntityManager entityManager, String folderName, String kolabId)
	{
		KolabItem item = null;
		Query query = entityManager.createQuery(QUERY_SELECT_ITEM_BY_KOLAB_ID);
		query.setParameter("folderName", folderName);
		query.setParameter("kolabId", kolabId);
		try {
			Object result = query.getSingleResult();
			item = (KolabItem)result;
		} catch (NoResultException e) {
			// nothing to do
		}
		return item;
	}

	public static KolabItem
	getItemForMailId(EntityManager entityManager, String folderName, Long mailId)
	{
		KolabItem item = null;
		Query query = entityManager.createQuery(QUERY_SELECT_ITEM_BY_MAIL_ID);
		query.setParameter("folderName", folderName);
		query.setParameter("mailId", mailId);
		try {
			Object result = query.getSingleResult();
			item = (KolabItem)result;
		} catch (NoResultException e) {
			// nothing to do
		}
		return item;
	}

	public static void
	deleteItemForMailId(EntityManager entityManager, String folderName, Long mailId)
	{
		Query query = entityManager.createQuery(QUERY_DELETE_ITEM_BY_MAIL_ID);
		query.setParameter("folderName", folderName);
		query.setParameter("mailId", mailId);
		query.executeUpdate();
	}

	public static KolabItem
	getVisibleItemForMailId(EntityManager entityManager, String folderName, Long mailId)
	{
		KolabItem item = null;
		Query query = entityManager.createQuery(QUERY_SELECT_VISIBLE_ITEM_BY_MAIL_ID);
		query.setParameter("folderName", folderName);
		query.setParameter("mailId", mailId);
		try {
			Object result = query.getSingleResult();
			item = (KolabItem)result;
		} catch (NoResultException e) {
			// nothing to do
		}
		return item;
	}

	public static KolabItem
	getVisibleItemForKolabId(EntityManager entityManager, String folderName, String kolabId)
	{
		KolabItem item = null;
		Query query = entityManager.createQuery(QUERY_SELECT_VISIBLE_ITEM_BY_KOLAB_ID);
		query.setParameter("folderName", folderName);
		query.setParameter("kolabId", kolabId);
		try {
			Object result = query.getSingleResult();
			item = (KolabItem)result;
		} catch (NoResultException e) {
			// nothing to do
		}
		return item;
	}

	public static void
	deleteFolderItems(EntityManager entityManager, String folderName)
	{
		final Query query = entityManager.createQuery(QUERY_DELETE_FOLDER_ITEMS);
		query.setParameter("name", folderName);
		EntityTransaction transaction = entityManager.getTransaction();

		executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				query.executeUpdate();
			}
		});
	}

	public static void
	createNewItem(final EntityManager entityManager, final String folderName,
	    CollaborationItem citem, final Long mailId,
	    String changeDeviceId, String changeUserId)
	{
		if (folderName == null || mailId == null || changeDeviceId == null || changeUserId == null)
			throw new NullPointerException();
		ImapFolder folder = getFolder(entityManager, folderName);
		if (folder == null)
			throw new NullPointerException("folder must be created before");
		final KolabItem item = new KolabItem();
		Date now = new Date();
		item.setChangeDate(now);
		item.setChangeDeviceId(changeDeviceId);
		item.setChangeUserId(changeUserId);
		item.setCreateDeviceId(changeDeviceId);
		item.setCreateUserId(changeUserId);
		item.setCreationDate(now);
		item.setDeleted(false);
		item.setFolder(folder);
		item.setKolabId(citem.getUid());
		item.setMailId(mailId);
		item.setValid(true);
		if (citem instanceof Task)
			DBSynchronizer.setTaskDates(item, (Task)citem);
		else if (citem instanceof Event)
			DBSynchronizer.setEventDates(item, (Event)citem);
		EntityTransaction transaction = entityManager.getTransaction();

		executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				DBUtil.deleteItemForMailId(entityManager, folderName, mailId);
				entityManager.persist(item);
			}
		});
	}

	public static void
	deleteItem(final EntityManager entityManager, String folderName, Long mailId,
	    String changeDeviceId, String changeUserId)
	{
		if (folderName == null || mailId == null || changeDeviceId == null || changeUserId == null)
			throw new NullPointerException();
		final KolabItem item = getVisibleItemForMailId(entityManager, folderName, mailId);
		Date now = new Date();
		item.setChangeDate(now);
		item.setChangeDeviceId(changeDeviceId);
		item.setChangeUserId(changeUserId);
		item.setDeleted(true);
		EntityTransaction transaction = entityManager.getTransaction();

		executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				entityManager.persist(item);
			}
		});
	}

	public static void
	updateItem(final EntityManager entityManager, String folderName, Long mailId,
	    long newMailId, CollaborationItem citem, String changeDeviceId, String changeUserId)
	{
		if (folderName == null || mailId == null || citem == null || changeDeviceId == null ||
		    changeUserId == null)
			throw new NullPointerException();
		final KolabItem item = getVisibleItemForMailId(entityManager, folderName, mailId);
		Date now = new Date();
		item.setChangeDate(now);
		item.setChangeDeviceId(changeDeviceId);
		item.setChangeUserId(changeUserId);
		item.setMailId(newMailId);
		if (citem instanceof Task)
			DBSynchronizer.setTaskDates(item, (Task)citem);
		else if (citem instanceof Event)
			DBSynchronizer.setEventDates(item, (Event)citem);
		EntityTransaction transaction = entityManager.getTransaction();

		executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				entityManager.persist(item);
			}
		});
	}

	public static void
	updateItemByKolabItem(final EntityManager entityManager,
	    String folderName, Long mailId, long newMailId, final KolabItem item,
	    String changeDeviceId, String changeUserId)
	{
		if (folderName == null || mailId == null || changeDeviceId == null || changeUserId == null)
			throw new NullPointerException();
		//		KolabItem item = getVisibleItemForMailId(entityManager, folderName, mailId);
		Date now = new Date();
		item.setChangeDate(now);
		item.setChangeDeviceId(changeDeviceId);
		item.setChangeUserId(changeUserId);
		item.setMailId(newMailId);
		//		if (citem instanceof Task)
		//			DBSynchronizer.setTaskDates(item, (Task) citem);
		//		else if (citem instanceof Event)
		//			DBSynchronizer.setEventDates(item, (Event) citem);
		EntityTransaction transaction = entityManager.getTransaction();

		executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				entityManager.persist(item);
			}
		});
	}

	public static void
	substituteItemByKolabItem(final EntityManager entityManager,
	    String folderName, Long mailId, long newMailId, final KolabItem item,
	    String changeDeviceId, String changeUserId)
	{
		if (folderName == null || mailId == null || changeDeviceId == null || changeUserId == null)
			throw new NullPointerException();
		//		KolabItem item = getVisibleItemForMailId(entityManager, folderName, mailId);
		Date now = new Date();
		item.setChangeDate(item.getChangeDate());
		item.setChangeDeviceId(changeDeviceId);
		item.setChangeUserId(changeUserId);
		item.setMailId(newMailId);
		//		if (citem instanceof Task)
		//			DBSynchronizer.setTaskDates(item, (Task) citem);
		//		else if (citem instanceof Event)
		//			DBSynchronizer.setEventDates(item, (Event) citem);
		EntityTransaction transaction = entityManager.getTransaction();

		executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				entityManager.persist(item);
			}
		});
	}

	public static List<String>
	getNewIDsForFolder(EntityManager entityManager, String folderName,
	    Date since, String deviceId, String userId)
	{
		return getIDsForFolder(QUERY_NEW_ITEMS, entityManager, folderName,
		    since, deviceId, userId);
	}

	public static List<String>
	getDeletedIDsForFolder(EntityManager entityManager, String folderName,
	    Date since, String deviceId, String userId)
	{
		return getIDsForFolder(QUERY_DELETED_ITEMS, entityManager, folderName,
		    since, deviceId, userId);
	}

	public static List<String>
	getUpdatedIDsForFolder(EntityManager entityManager, String folderName,
	    Date since, String deviceId, String userId)
	{
		return getIDsForFolder(QUERY_UPDATED_ITEMS, entityManager, folderName,
		    since, deviceId, userId);
	}

	public static List<String>
	getBeforeVisibleItemIDs(EntityManager entityManager, String folderName,
	    Date since)
	{
		return getIDsForFolder(QUERY_VISIBLE_ITEMS_BEFORE, entityManager, folderName,
		    since, null, null);
	}

	@SuppressWarnings("unchecked")
	private static List<String>
	getIDsForFolder(String queryPrefix, EntityManager entityManager,
	    String folderName, Date since, String deviceId, String userId)
	{
		boolean checkChangeUser = deviceId != null && userId != null;
		Query query = entityManager.createQuery(
		    checkChangeUser ? queryPrefix + QUERY_WHERE_CHANGE_USER : queryPrefix);
		query.setParameter("name", folderName);
		query.setParameter("since", DateUtil.getLong(since));
		if (checkChangeUser) {
			query.setParameter("changeDeviceId", deviceId);
			query.setParameter("changeUserId", userId);
		}
		if (queryPrefix.contains("createDeviceId")) {
			query.setParameter("deviceId", deviceId);
		}
		List<?> result = query.getResultList();
		return (List<String>)result;
	}

	/**
	 * Add a new user with the specified name in the databse if it is not
	 * already created before.
	 * Returns the user entry with the pk field which is not null.
	 */
	public static synchronized User
	addUser(final EntityManager entityManager, String userName)
	{
		User user = null;
		Query query = entityManager.createQuery(QUERY_USER);
		query.setParameter("name", userName);
		try {
			Object result = query.getSingleResult();
			user = (User)result;
		} catch (NoResultException e) {
			user = new User();
			user.setName(userName);
			EntityTransaction transaction = entityManager.getTransaction();

			final User userToPersist = user;
			executeInTransaction(transaction, new Runnable() {
				@Override
				public void run()
				{
					entityManager.persist(userToPersist);
				}
			});

			// read user pk
			Object result = query.getSingleResult();
			user = (User)result;
		}
		return user;
	}

	/**
	 * Get the visibility of a specified folder for a user to a specified time.
	 * If no visibility is known for the given parameters a null value is
	 * returned.
	 *
	 * @param previous if no entry with the given time parameter is available, should
	 *                 the previous entry be taken?
	 */
	public static Boolean
	isSyncFolder(EntityManager entityManager, String folderName,
	    String userName, Date time, boolean previous)
	{
		Query query = entityManager.createQuery(
		    previous ? QUERY_SELECT_FOLDER_VISIBILITY : QUERY_SELECT_FOLDER_VISIBILITY_EQUAL);
		query.setParameter("folderName", folderName);
		query.setParameter("userName", userName);
		if (previous && time == null)
			query.setParameter("time", Long.MAX_VALUE);
		else
			query.setParameter("time", DateUtil.getLong(time));
		FolderVisibility fv;
		if (previous) {
			List<FolderVisibility> result = query.getResultList();
			if (result.isEmpty())
				return null;
			fv = result.get(0);
		} else {
			try {
				fv = (FolderVisibility)query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		return fv.getVisible();
	}

	public static void
	executeInTransaction(EntityTransaction transaction, Runnable task)
	{
		try {
			transaction.begin();
			task.run();
			transaction.commit();
		} finally {
			if (transaction.isActive())
				transaction.rollback();
		}
	}
}
