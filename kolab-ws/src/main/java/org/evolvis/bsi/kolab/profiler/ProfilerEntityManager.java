package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import java.util.List;
import java.util.Map;

/**
 * Internally used {@link EntityManager} proxy.
 *
 * @author Hendrik Helwich
 * @see ProfilerEntityManagerFactory
 */
class ProfilerEntityManager implements EntityManager {
	private final EntityManager delegate;

	public ProfilerEntityManager(EntityManager delegate)
	{
		this.delegate = delegate;
	}

	// --------------------------------------------------- wrap returned queries

	@Override
	public Query
	createQuery(String qlString)
	{
		return new ProfilerQuery(qlString, delegate.createQuery(qlString));
	}

	@Override
	public Query
	createNamedQuery(String name)
	{
		return new ProfilerQuery(name, delegate.createNamedQuery(name));
	}

	@Override
	public Query
	createNativeQuery(String sqlString)
	{
		return new ProfilerQuery(sqlString, delegate.createNativeQuery(sqlString));
	}

	@Override
	public Query
	createNativeQuery(String sqlString, @SuppressWarnings("rawtypes") Class resultClass)
	{
		return new ProfilerQuery(sqlString, delegate.createNativeQuery(sqlString, resultClass));
	}

	@Override
	public Query
	createNativeQuery(String sqlString, String resultSetMapping)
	{
		return new ProfilerQuery(sqlString, delegate.createNativeQuery(sqlString, resultSetMapping));
	}

	// ----------------------------------------------------- delegate operations

	@Override
	public void
	persist(Object entity)
	{
		delegate.persist(entity);
	}

	@Override
	public <T> T
	merge(T entity)
	{
		return delegate.merge(entity);
	}

	@Override
	public void
	remove(Object entity)
	{
		delegate.remove(entity);
	}

	@Override
	public <T> T
	find(Class<T> entityClass, Object primaryKey)
	{
		return delegate.find(entityClass, primaryKey);
	}

	@Override
	public <T> T
	getReference(Class<T> entityClass, Object primaryKey)
	{
		return delegate.getReference(entityClass, primaryKey);
	}

	@Override
	public void
	flush()
	{
		delegate.flush();
	}

	@Override
	public void
	setFlushMode(FlushModeType flushMode)
	{
		delegate.setFlushMode(flushMode);
	}

	@Override
	public FlushModeType
	getFlushMode()
	{
		return delegate.getFlushMode();
	}

	@Override
	public void
	lock(Object entity, LockModeType lockMode)
	{
		delegate.lock(entity, lockMode);
	}

	@Override
	public void
	refresh(Object entity)
	{
		delegate.refresh(entity);
	}

	@Override
	public void
	clear()
	{
		delegate.clear();
	}

	@Override
	public boolean
	contains(Object entity)
	{
		return delegate.contains(entity);
	}

	@Override
	public void
	joinTransaction()
	{
		delegate.joinTransaction();
	}

	@Override
	public Object
	getDelegate()
	{
		return delegate.getDelegate();
	}

	@Override
	public void
	close()
	{
		delegate.close();
	}

	@Override
	public boolean
	isOpen()
	{
		return delegate.isOpen();
	}

	@Override
	public EntityTransaction
	getTransaction()
	{
		return delegate.getTransaction();
	}

	@Override
	public <T> T
	find(Class<T> entityClass, Object primaryKey, Map<String, Object> properties)
	{
		return delegate.find(entityClass, primaryKey, properties);
	}

	@Override
	public <T> T
	find(Class<T> entityClass, Object primaryKey, LockModeType lockMode)
	{
		return delegate.find(entityClass, primaryKey, lockMode);
	}

	@Override
	public <T> T
	find(Class<T> entityClass, Object primaryKey, LockModeType lockMode,
	    Map<String, Object> properties)
	{
		return delegate.find(entityClass, primaryKey, lockMode, properties);
	}

	@Override
	public void
	lock(Object entity, LockModeType lockMode, Map<String, Object> properties)
	{
		delegate.lock(entity, lockMode, properties);
	}

	@Override
	public void
	refresh(Object entity, Map<String, Object> properties)
	{
		delegate.refresh(entity, properties);
	}

	@Override
	public void
	refresh(Object entity, LockModeType lockMode)
	{
		delegate.refresh(entity, lockMode);
	}

	@Override
	public void
	refresh(Object entity, LockModeType lockMode, Map<String, Object> properties)
	{
		delegate.refresh(entity, lockMode, properties);
	}

	@Override
	public void
	detach(Object entity)
	{
		delegate.detach(entity);
	}

	@Override
	public LockModeType
	getLockMode(Object entity)
	{
		return delegate.getLockMode(entity);
	}

	@Override
	public void
	setProperty(String propertyName, Object value)
	{
		delegate.setProperty(propertyName, value);
	}

	@Override
	public Map<String, Object>
	getProperties()
	{
		return delegate.getProperties();
	}

	@Override
	public <T> TypedQuery<T>
	createQuery(CriteriaQuery<T> criteriaQuery)
	{
		return delegate.createQuery(criteriaQuery);
	}

	@Override
	public Query
	createQuery(CriteriaUpdate updateQuery)
	{
		return delegate.createQuery(updateQuery);
	}

	@Override
	public Query
	createQuery(CriteriaDelete deleteQuery)
	{
		return delegate.createQuery(deleteQuery);
	}

	@Override
	public <T> TypedQuery<T>
	createQuery(String qlString, Class<T> resultClass)
	{
		return delegate.createQuery(qlString, resultClass);
	}

	@Override
	public <T> TypedQuery<T>
	createNamedQuery(String name, Class<T> resultClass)
	{
		return delegate.createNamedQuery(name, resultClass);
	}

	@Override
	public StoredProcedureQuery
	createNamedStoredProcedureQuery(String name)
	{
		return delegate.createNamedStoredProcedureQuery(name);
	}

	@Override
	public StoredProcedureQuery
	createStoredProcedureQuery(String procedureName)
	{
		return delegate.createStoredProcedureQuery(procedureName);
	}

	@Override
	public StoredProcedureQuery
	createStoredProcedureQuery(String procedureName, Class... resultClasses)
	{
		return delegate.createStoredProcedureQuery(procedureName, resultClasses);
	}

	@Override
	public StoredProcedureQuery
	createStoredProcedureQuery(String procedureName, String... resultSetMappings)
	{
		return delegate.createStoredProcedureQuery(procedureName, resultSetMappings);
	}

	@Override
	public boolean
	isJoinedToTransaction()
	{
		return delegate.isJoinedToTransaction();
	}

	@Override
	public <T> T
	unwrap(Class<T> cls)
	{
		return delegate.unwrap(cls);
	}

	@Override
	public EntityManagerFactory
	getEntityManagerFactory()
	{
		return delegate.getEntityManagerFactory();
	}

	@Override
	public CriteriaBuilder
	getCriteriaBuilder()
	{
		return delegate.getCriteriaBuilder();
	}

	@Override
	public Metamodel
	getMetamodel()
	{
		return delegate.getMetamodel();
	}

	@Override
	public <T> EntityGraph<T>
	createEntityGraph(Class<T> rootType)
	{
		return delegate.createEntityGraph(rootType);
	}

	@Override
	public EntityGraph<?>
	createEntityGraph(String graphName)
	{
		return delegate.createEntityGraph(graphName);
	}

	@Override
	public EntityGraph<?>
	getEntityGraph(String graphName)
	{
		return delegate.getEntityGraph(graphName);
	}

	@Override
	public <T> List<EntityGraph<? super T>>
	getEntityGraphs(Class<T> entityClass)
	{
		return delegate.getEntityGraphs(entityClass);
	}
}
