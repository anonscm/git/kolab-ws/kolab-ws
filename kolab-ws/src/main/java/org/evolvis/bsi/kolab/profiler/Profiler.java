package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.service.ServiceConf;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A singleton profiler which can be used to write informations into a log file.
 * It is activated/deactivated by a configuration file property which is
 * evaluated every 10 seconds.
 *
 * @author Hendrik Helwich
 */
public class Profiler {
	private static final Logger logger = Logger.getLogger(Profiler.class.getName());

	/**
	 * Classes which have at least on of the given prefixes are included to the
	 * operation stack which is written to a log file.
	 */
	private static final String[] STACK_INCLUDE_PREFIXES = new String[] {
	    "org.evolvis.",
	    "com.sun.mail.",
	    "javax.mail."
	};

	/**
	 * Classes which are already included in the operation stack, and which have
	 * at least on of the given prefixes are excluded from the operation stack
	 * which is written to a log file.
	 */
	private static final String[] STACK_EXCLUDE_PREFIXES = new String[] {
	    "org.evolvis.bsi.kolab.profiler.",
	    "org.evolvis.bsi.kolab.imap.IMAPProfilingUtil"
	};

	/**
	 * A value of <code>null</code> does indicate that the profiler is not setup
	 */
	private ConfigUpdateObserver cuObserver;

	/**
	 * A CPU load monitor thread which should be running if the profiler is
	 * active.
	 */
	private CPUMonitor cpuMonitor;

	/**
	 * A memory usage monitor thread which should be running if the profiler is
	 * active.
	 */
	private MemoryMonitor memMonitor;

	private boolean active = false;

	/**
	 * The logfile to which all informations are written.
	 */
	private LogFileWriter logFileWriter;

	/**
	 * The singleton instance of the profiler.
	 */
	public static final Profiler INSTANCE = new Profiler();

	private Profiler()
	{
	}

	/**
	 * A thread which checks the configuration file every 10 seconds to
	 * activated or deactivate the profiler if necessary.
	 * One instance of this thread should be running in the background if the
	 * profiler is {@link Profiler#setUp()}.
	 * If this thread is stopped (e.g. because the profiler is
	 * {@link Profiler#tearDown()}, the profiler is deactivated.
	 */
	private static class ConfigUpdateObserver extends Thread {
		@Override
		public void
		run()
		{
			do {
				load();
				try {
					sleep(10 * 1000); // sleep 10 seconds
				} catch (InterruptedException e) {
					break;
				}
			} while (!isInterrupted());
			Profiler.setActive(false);
		}

		private void
		load()
		{
			boolean active = Boolean.parseBoolean(ServiceConf.get("profiler.active"));
			Profiler.setActive(active);
		}
	}

	/**
	 * This operation should be called once to initialize the kolab ws profiler.
	 * A background process will be started which periodically checks if the
	 * configured activation status of the profiler has changed. If the profiler
	 * is configured as active, It could start profiling background threads like
	 * CPU and memory logger threads. If this operation is called multiple
	 * times and {@link #tearDown()} is not called, only the first call will
	 * cause an action.
	 */
	public synchronized static void
	setUp()
	{
		// this will trigger the initialization of the singleton instance
		if (!INSTANCE.isSetUp()) {
			INSTANCE.cuObserver = new ConfigUpdateObserver();
			INSTANCE.cuObserver.start();
		}
	}

	/**
	 * Returns <code>true</code> if the profiler is {@link #setUp()}.
	 */
	private boolean
	isSetUp()
	{
		return cuObserver != null;
	}

	/**
	 * Stop the profiling configuration checking thread and also deactivate the
	 * profiler. This will possibly stop other threads if the profiler was
	 * active before.
	 */
	public synchronized static void
	tearDown()
	{
		if (INSTANCE.isSetUp()) {
			// stop config observing thread
			INSTANCE.cuObserver.interrupt();
			// wait till config observing thread is stopped
			while (INSTANCE.cuObserver.isAlive())
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			// indicate, that the profiler is not setup
			INSTANCE.cuObserver = null;
		}
	}

	/**
	 * Activate/Deactivate the profiler.
	 * If the profiler is activated, a log file is created and background
	 * logging processes are started (e.g. CPU load monitor).
	 * If the profiler is deactivated, background processes are stopped (e.g.
	 * CPU load monitor) and the current log file is finished, and zipped.
	 *
	 * @param active <code>true</code> if the profiler should be activated
	 */
	private static void
	setActive(boolean active)
	{
		if (active == INSTANCE.active)
			return;
		INSTANCE.active = active;
		if (active) { // profiler is activated
			logger.info("profiler has been activated");
			try { // initialize profiler
				INSTANCE.logFileWriter = new LogFileWriter();
				INSTANCE.cpuMonitor = new CPUMonitor();
				INSTANCE.cpuMonitor.start();
				INSTANCE.memMonitor = new MemoryMonitor();
				INSTANCE.memMonitor.start();
			} finally {
			}
		} else { // profiler is deactivated
			try { // deinitialize profiler
				logger.info("profiler has been deactivated");
				INSTANCE.cpuMonitor.interrupt();
				INSTANCE.memMonitor.interrupt();
				while (INSTANCE.cpuMonitor.isAlive() || INSTANCE.memMonitor.isAlive())
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				INSTANCE.cpuMonitor = null;
				INSTANCE.memMonitor = null;
				try {
					INSTANCE.logFileWriter.close();
				} catch (XMLStreamException e) {
					logger.warn("error while closing profiler log file", e);
					e.printStackTrace();
				} catch (IOException e) {
					logger.warn("error while closing profiler log file", e);
				}
			} finally {
			}
		}
	}

	public boolean
	isActive()
	{
		return active;
	}

	/**
	 * If the profiler is active, the given CPU load and the measuring time are
	 * written to the log file.
	 *
	 * @param time measuring time
	 * @param load measured CPU load
	 */
	void
	logCpu(long time, float load)
	{
		if (!isActive())
			return;
		logFileWriter.logCpuLoad(time, load);
	}

	/**
	 * If the profiler is active, the given memory usage and the measuring time
	 * are written to the log file.
	 *
	 * @param time      measuring time
	 * @param used      bytes of memory used by the JVM
	 * @param committed bytes of memory allocated by the JVM
	 * @param max       maximum bytes of memory which can be allocated by the JVM
	 */
	public void
	logMemoryUsage(long time, long used, long committed, long max)
	{
		if (!isActive())
			return;
		logFileWriter.logMemoryUsage(time, used, committed, max);
	}

	/**
	 * If the profiler is active, the given start time and duration together
	 * with the current method stack are written to the log file.
	 *
	 * @param startTime time just before the method call
	 * @param duration  duration of the method call
	 * @param stackTop  An optional element which is put on top of the current method
	 *                  stack. Can be <code>null</code>.
	 */
	public void
	logOperation(long startTime, long duration, StackTraceElement... stackTop)
	{
		if (!isActive())
			return;
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		if (stackTop.length > 0) { // put stackTop on top of the stack
			StackTraceElement[] stack2 = new StackTraceElement[stack.length + stackTop.length];
			System.arraycopy(stackTop, 0, stack2, 0, stackTop.length);
			System.arraycopy(stack, 0, stack2, stackTop.length, stack.length);
			stack = stack2;
		}
		for (int i = 0; i < stack.length; i++)
			if (stack[i] != null && !isLoggedClass(stack[i].getClassName()))
				stack[i] = null;
		logFileWriter.logOperation(startTime, duration, stack);
	}

	/**
	 * Returns <code>true</code> if method stack informations of the class with
	 * the given name should be logged.
	 *
	 * @return <code>true</code> if method stack informations of the class with
	 *     the given name should be logged
	 */
	private static boolean
	isLoggedClass(String className)
	{
		if (className != null)
			for (String inpref : STACK_INCLUDE_PREFIXES)
				if (className.startsWith(inpref)) { // class is included
					for (String expref : STACK_EXCLUDE_PREFIXES)
						if (className.startsWith(expref)) // included class is excluded
							return false;
					return true;
				}
		return false;
	}

	/**
	 * Returns a proxy instance for the given factory with the same behavior.
	 * If the kolab ws profiler is active, database queries done via
	 * {@link Query} objects which are derived from the returned proxy factory,
	 * are written to the profiler log.
	 *
	 * @param factory The factory which should be wrapped
	 *
	 * @return A proxy for the given factory
	 *
	 * @see #isActive()
	 */
	public static EntityManagerFactory
	profileQueryExecution(EntityManagerFactory factory)
	{
		return new ProfilerEntityManagerFactory(factory);
	}

	/**
	 * Wrap a given interface instance and log all accesses on the
	 * the interface methods.
	 * If the given object has not a long living time, this operation can be
	 * dismissed if {@link #isActive()} returns <code>false</code> to increase
	 * performance.
	 *
	 * @param instance The interface instance which should be wrapped
	 *
	 * @return The wrapped interface
	 */
	@SuppressWarnings("unchecked")
	public static <T> T
	profileInstance(T instance)
	{
		return (T)ProfileInstanceProxy.newInstance(instance);
	}

	/**
	 * Invoke a method on a given object, record the start time and the time
	 * length and log the informations afterwards.
	 * If the profiler is inactive, the method is called anyway but no logging
	 * is done.
	 *
	 * @param obj    The object on which the specified method will be called
	 * @param method The method which should be called on the given object
	 * @param args   The arguments which will be passed to the method
	 *
	 * @return The return value of the method
	 */
	public static Object
	invoke(Object obj, Method method, Object... args) throws Throwable
	{
		return invoke(null, null, obj, method, args);
	}

	/**
	 * Invoke a method on a given object, record the start time and the time
	 * length and log the informations afterwards.
	 * If the profiler is inactive, the method is called anyway but no logging
	 * is done.
	 *
	 * If both parameter <code>topClass</code> and <code>topMethod</code> are
	 * given, they are put on top of the operation stack which is written to the
	 * log file.
	 *
	 * @param topClass  Put on top of the stack together with <code>topMethod</code>.
	 *                  Can be <code>null</code>.
	 * @param topMethod Put on top of the stack together with <code>topClass</code>.
	 *                  Can be <code>null</code>.
	 * @param obj       The object on which the specified method will be called
	 * @param method    The method which should be called on the given object
	 * @param args      The arguments which will be passed to the method
	 *
	 * @return The return value of the method
	 */
	public static Object
	invoke(String topClass, String topMethod, Object obj, Method method, Object... args)
	throws Throwable
	{
		long startTime = System.currentTimeMillis();
		try {
			return method.invoke(obj, args);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw e.getTargetException();
		} finally {
			StackTraceElement[] s = new StackTraceElement[2];
			if (topClass != null && topMethod != null)
				s[0] = new StackTraceElement(topClass, topMethod, null, -1);
			s[1] = new StackTraceElement(obj.getClass().getName(), method.getName(), null, -1);
			long duration = System.currentTimeMillis() - startTime;
			Profiler.INSTANCE.logOperation(startTime, duration, s);
		}
	}

	/**
	 * Invoke a method on a given object, record the start time and the time
	 * length and log the informations afterwards.
	 * If the profiler is inactive, the method is called anyway but no logging
	 * is done.
	 *
	 * @param obj        The object on which the specified method will be called
	 * @param methodName The name of the method which should be called on the given object
	 * @param args       The arguments which will be passed to the method
	 *
	 * @return The return value of the method
	 */
	public static Object
	profileInvokeCatch(Object obj, String methodName, Object... args)
	throws Throwable
	{
		return profileInvokeCatch(null, null, obj, methodName, args);
	}

	/**
	 * Like {@link #profileInvokeCatch(Object, String, Object...)}
	 * but not {@link RuntimeException}s are catched and wrapped in a new
	 * {@link RuntimeException}. This operation can be used to invoke methods
	 * which does only throw {@link RuntimeException}s.
	 *
	 * @see Profiler#profileInvokeCatch(Object, String, Object...)
	 */
	public static Object
	profileInvoke(Object obj, String methodName, Object... args)
	{
		return profileInvoke(null, null, obj, methodName, args);
	}

	/**
	 * Invoke a method on a given object, record the start time and the time
	 * length and log the informations afterwards.
	 * If the profiler is inactive, the method is called anyway but no logging
	 * is done.
	 *
	 * If both parameter <code>topClass</code> and <code>topMethod</code> are
	 * given, they are put on top of the operation stack which is written to the
	 * log file.
	 *
	 * @param topClass   Put on top of the stack together with <code>topMethod</code>.
	 *                   Can be <code>null</code>.
	 * @param topMethod  Put on top of the stack together with <code>topClass</code>.
	 *                   Can be <code>null</code>.
	 * @param obj        The object on which the specified method will be called
	 * @param methodName The name of the method which should be called on the given object
	 * @param args       The arguments which will be passed to the method
	 *
	 * @return The return value of the method
	 */
	public static Object
	profileInvokeCatch(String topClass, String topMethod, Object obj, String methodName, Object... args)
	throws Throwable
	{
		Class<?>[] argst = new Class[args.length];
		for (int i = 0; i < args.length; i++)
			argst[i] = args[i].getClass();
		Method method;
		try {
			method = searchForMethod(obj.getClass(), methodName, argst);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		return invoke(topClass, topMethod, obj, method, args);
	}

	/**
	 * Like {@link #profileInvokeCatch(String, String, Object, String, Object...)}
	 * but not {@link RuntimeException}s are catched and wrapped in a new
	 * {@link RuntimeException}. This operation can be used to invoke methods
	 * which does only throw {@link RuntimeException}s.
	 *
	 * @see Profiler#profileInvokeCatch(String, String, Object, String, Object...)
	 */
	public static Object
	profileInvoke(String topClass, String topMethod, Object obj, String methodName, Object... args)
	{
		try {
			return profileInvokeCatch(topClass, topMethod, obj, methodName, args);
		} catch (RuntimeException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException("unexpected exception", e);
		}
	}

	// see: http://sourceforge.net/projects/meta-jb/

	/**
	 * Searches through all methods looking for one with the specified name that
	 * will take the specified parameters even if the parameter types are more
	 * generic in the actual method implementation. This is similar to the
	 * findConstructor() method and has the similar limitations that it doesn’t
	 * do a real widening scope search and simply processes the methods in
	 * order.
	 */
	private static Method
	searchForMethod(Class<?> type, String name, Class<?>[] parms)
	throws NoSuchMethodException
	{
		Method[] methods = type.getMethods();
		for (int i = 0; i < methods.length; i++) {
			// Has to be named the same of course.
			if (!methods[i].getName().equals(name))
				continue;

			Class<?>[] types = methods[i].getParameterTypes();

			// Does it have the same number of arguments that we’re looking for.
			if (types.length != parms.length)
				continue;

			// Check for type compatibility
			if (areTypesCompatible(types, parms))
				return methods[i];
		}
		throw new NoSuchMethodException("cannot find method " + name
		    + "() in class " + type.getName());
	}

	/**
	 * Returns true if all classes in the sources list are assignment compatible
	 * with the targets list. In other words, if all
	 * targets[n].isAssignableFrom( sources[n] ) then this method returns true.
	 * Any null values in sources are considered wild-cards and will skip the
	 * isAssignableFrom check as if it passed.
	 */
	private static boolean
	areTypesCompatible(Class<?>[] targets, Class<?>[] sources)
	{
		if (targets.length != sources.length)
			return (false);

		for (int i = 0; i < targets.length; i++) {
			if (sources[i] == null)
				continue;
			if (!translateFromPrimitive(targets[i])
			    .isAssignableFrom(sources[i]))
				return (false);
		}
		return (true);
	}

	/**
	 * If this specified class represents a primitive type (int, float, etc.)
	 * then it is translated into its wrapper type (Integer, Float, etc.). If
	 * the passed class is not a primitive then it is just returned.
	 */
	private static Class<?>
	translateFromPrimitive(Class<?> primitive)
	{
		if (!primitive.isPrimitive())
			return (primitive);

		if (Boolean.TYPE.equals(primitive))
			return (Boolean.class);
		if (Character.TYPE.equals(primitive))
			return (Character.class);
		if (Byte.TYPE.equals(primitive))
			return (Byte.class);
		if (Short.TYPE.equals(primitive))
			return (Short.class);
		if (Integer.TYPE.equals(primitive))
			return (Integer.class);
		if (Long.TYPE.equals(primitive))
			return (Long.class);
		if (Float.TYPE.equals(primitive))
			return (Float.class);
		if (Double.TYPE.equals(primitive))
			return (Double.class);

		throw new RuntimeException("Error translating type:" + primitive);
	}
}
