package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Validator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.Locale;

/**
 * Used to fill the kolab xml data which is note specific to an {@link Note}
 * instance.
 *
 * @author Hendrik Helwich
 */
public class NoteItemHandler extends ItemHandler<Note> {
	private Note note = new Note();

	@Override
	public void
	checkRootName(String name) throws KolabParseException
	{
		if (!ELEMENT_NOTE.equals(name.toLowerCase(Locale.ENGLISH)))
			throw new KolabParseException("unexpected note root element name: " + name);
	}

	@Override
	public boolean
	readElement(String name, XMLStreamReader parser)
	throws XMLStreamException, KolabParseException
	{
		if (readElementCollaborationItem(note, name, parser))
			return true;
		else if (ELEMENT_NOTE_SUMMARY.equals(name.toLowerCase(Locale.ENGLISH)))
			note.setSummary(parser.getElementText());
		else if (ELEMENT_BACKGROUND_COLOR.equals(name.toLowerCase(Locale.ENGLISH)))
			note.setBackgroundColor(parser.getElementText());
		else if (ELEMENT_FOREGROUND_COLOR.equals(name.toLowerCase(Locale.ENGLISH)))
			note.setForegroundColor(parser.getElementText());
		else
			return false;
		return true;
	}

	@Override
	public void
	validate() throws KolabParseException
	{
		try {
			Validator.validate(note);
		} catch (IllegalArgumentException e) {
			throw new KolabParseException(e.getMessage());
		}
	}

	@Override
	public Note
	getItem()
	{
		return note;
	}
}
