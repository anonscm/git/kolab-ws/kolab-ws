package org.evolvis.bsi.kolab.service;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.Helper;
import org.evolvis.bsi.kolab.db.ImapFolder;
import org.evolvis.bsi.kolab.profiler.Profiler;
import org.jboss.logging.Logger;
import org.jboss.ws.api.annotation.WebContext;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 * @author Hendrik Helwich
 */
@Stateless
@WebService(wsdlLocation = "/META-INF/wsdl/KolabServiceStatus.wsdl",
    serviceName = "KolabServiceStatus", portName = "KolabServiceStatusPort")
@WebContext(contextRoot = "/kolab-ws")
public class KolabServiceStatusPortTypeImpl implements KolabServiceStatusPortType {
	private static final Logger logger = Logger.getLogger(KolabServiceStatusPortTypeImpl.class.getName());

	@PersistenceUnit(unitName = "KolabWs")
	private EntityManagerFactory emf;

	// implementation

	@Override
	public synchronized WsStatus
	getStatus() throws KolabServiceFault_Exception
	{
		// TODO check imap connection status
		EntityManager em = Profiler.profileQueryExecution(emf).createEntityManager();

		Query query = em.createNativeQuery("SELECT COUNT(*) FROM " + ImapFolder.TABLE_NAME);
		try {
			query.getResultList();
		} catch (Throwable e) {
			logger.error("cannot execute test sql statement", e);
			return WsStatus.NO_ACCESS_DBMS;
		}
		return WsStatus.OK;
	}

	@Override
	public String
	getVersion() throws KolabServiceFault_Exception
	{
		return Helper.getVersion();
	}
}
