package org.evolvis.bsi.kolab.service;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A singleton which holds the local configuration needed by the web service.
 * The properties are stored in the file {@link #PROPERTIES_FILE} and can be
 * accessed like this:<br>
 * <code>String value = ServiceConf.get("key");</code>
 *
 * @author Hendrik Helwich
 */
public class ServiceConf {
	public static final String PROPERTIES_FILE = "/org/evolvis/bsi/kolab/kolab-ws.properties";

	private File propertiesFile;
	private long lastModifiedDate;

	private static final ServiceConf INSTANCE = new ServiceConf();

	private Properties properties;

	private static final Logger logger = Logger.getLogger(ServiceConf.class.getName());

	private ServiceConf()
	{
	}

	public static void
	setPropertiesFile(File file) throws FileNotFoundException
	{
		INSTANCE.propertiesFile = file;
		INSTANCE.lastModifiedDate = 0;
		INSTANCE.load();
	}

	public static String
	get(String key)
	{
		try {
			INSTANCE.load();
		} catch (FileNotFoundException e) {
			try {
				setPropertiesFile(null);
			} catch (FileNotFoundException e1) {
				// not possible
			}
			logger.error("config file can not be found", e);
		}
		String value = INSTANCE.properties.getProperty(key);
		if (value == null)
			logger.warn("service property is null: " + key);
		return value;
	}

	private void
	load() throws FileNotFoundException
	{
		if (propertiesFile != null) {
			if (lastModifiedDate != propertiesFile.lastModified()) {
				lastModifiedDate = propertiesFile.lastModified();
				logger.info("loading properties file: " + propertiesFile.getAbsolutePath());
				load(new FileInputStream(propertiesFile));
			}
		} else {
			InputStream is = ServiceConf.class.getResourceAsStream(PROPERTIES_FILE);
			load(is);
		}
	}

	private void
	load(InputStream is)
	{
		properties = new Properties();
		try {
			properties.load(is);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw new RuntimeException("unable to load properties file", e);
		}
	}
}
