package org.evolvis.bsi.kolab.service;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.cxf.common.security.UsernameToken;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.evolvis.bsi.kolab.Helper;
import org.evolvis.bsi.kolab.KolabAccessor;
import org.evolvis.bsi.kolab.KolabAccessorInterface;
import org.evolvis.bsi.kolab.profiler.Profiler;
import org.jboss.logging.Logger;
import org.jboss.ws.api.annotation.WebContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.mail.StoreClosedException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

// note: security-domains 'jboss-web-policy' and 'jboss-ejb-policy'
// must be defined in JBoss/WildFly configuration (standalone.xml)

/**
 * @author Hendrik Helwich, Patrick Apel, tarent GmbH
 */
@Stateless
@WebService(wsdlLocation = "/META-INF/wsdl/KolabService.wsdl",
    serviceName = "KolabService", portName = "KolabServicePort")
@WebContext(contextRoot = "/kolab-ws")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
/* cxf interceptor to fill principal to EJB context */
@InInterceptors(interceptors = { "org.jboss.wsf.stack.cxf.security.authentication.SubjectCreatingPolicyInterceptor" })
public class KolabServicePortTypeImpl implements KolabServicePortType {
	private static final Logger logger = Logger.getLogger(KolabServicePortTypeImpl.class.getName());
	private static final String JBOSS_PROPERTY_KEY_BASE_CONFIGURATION_DIRECTORY = "jboss.server.config.dir";
	private static long errorId = 0;
	@PersistenceUnit(unitName = "KolabWs")
	private EntityManagerFactory emf;
	@Resource
	private SessionContext sessionContext;

	@PostConstruct
	public void
	setUp()
	{
		/* Set utc as default time zone. This is important for formatting
		 * xsd date objects with org.w3._2001.xmlschema.Adapter2#marshal(Date).
		 * If Time is UTC this looks like:   2009-10-14Z
		 * If not it looks for example like: 2009-10-14+02:00
		 * This will result in a wrong date object in the client.
		 * xsd datetime values are not affected by this.
		 */
		TimeZone.setDefault(org.evolvis.bsi.kolab.xml.Helper.getUTCTimeZone());

		try {
			// initialize service
			String path = System.getProperty(JBOSS_PROPERTY_KEY_BASE_CONFIGURATION_DIRECTORY);
			String fileName = ServiceConf.PROPERTIES_FILE.substring(
			    ServiceConf.PROPERTIES_FILE.lastIndexOf('/') + 1);
			File cfile = new File(path, fileName);

			if (!cfile.exists()) { // create config file
				InputStream in = getClass().getResourceAsStream(ServiceConf.PROPERTIES_FILE);
				OutputStream out = new FileOutputStream(cfile);
				Helper.copyStream(in, out);
				in.close();
				out.close();
			}
			ServiceConf.setPropertiesFile(cfile);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Profiler.setUp();
	}

	@PreDestroy
	public void
	tearDown()
	{
		Profiler.tearDown();
	}

	// implementation

	@Override
	public boolean
	addAttachment(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "attachmentId") String attachmentId,
	    @WebParam(name = "attachmentData") Attachment attachmentData,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface accessor = getKolabAccessor();
			return accessor.addAttachment(folder, id, attachmentId, attachmentData, deviceId,
			    lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public String
	addContact(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "contact") Contact contact)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.addContact(folder, deviceId, contact);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public String
	addEvent(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "event") Event event)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.addEvent(folder, deviceId, event);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public String
	addNote(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "note") Note note)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.addNote(folder, deviceId, note);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public String
	addTask(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "task") Task task)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.addTask(folder, deviceId, task);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public Attachment
	getAttachment(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "attachmentId") String attachmentId)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getAttachment(folder, id, attachmentId);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public Contact
	getContact(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "id") String id)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getContact(folder, id);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<IdAndChangeTime>
	getContactChangeTimes(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getContactChangeTimes(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<Folder>
	getContactFolders()
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getContactFolders();
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getContactIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getContactIDs(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public Date
	getCurrentTime()
	{
		return new Date();
	}

	@Override
	public Event
	getEvent(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "id") String id)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getEvent(folder, id);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<IdAndChangeTime>
	getEventChangeTimes(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getEventChangeTimes(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<Folder>
	getEventFolders()
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getEventFolders();
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getEventIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getEventIDs(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getNewContactIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNewContactIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getNewEventIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNewEventIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getNewNoteIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNewNoteIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getNewTaskIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNewTaskIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public Note
	getNote(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "id") String id)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNote(folder, id);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<IdAndChangeTime>
	getNoteChangeTimes(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNoteChangeTimes(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<Folder>
	getNoteFolders()
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNoteFolders();
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getNoteIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getNoteIDs(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getRemovedContactIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getRemovedContactIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getRemovedEventIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getRemovedEventIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getRemovedNoteIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getRemovedNoteIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getRemovedTaskIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getRemovedTaskIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public Task
	getTask(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "id") String id)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getTask(folder, id);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<IdAndChangeTime>
	getTaskChangeTimes(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getTaskChangeTimes(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<Folder>
	getTaskFolders()
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getTaskFolders();
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getTaskIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getTaskIDs(folder, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getUpdatedContactIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getUpdatedContactIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getUpdatedEventIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getUpdatedEventIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getUpdatedNoteIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getUpdatedNoteIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public List<String>
	getUpdatedTaskIDs(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime,
	    @WebParam(name = "updateTime") Date updateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.getUpdatedTaskIDs(folder, deviceId, lastUpdateTime, updateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	removeAttachment(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "attachmentId") String attachmentId,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface accessor = getKolabAccessor();
			return accessor.removeAttachment(folder, id, attachmentId, deviceId, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	removeContact(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.removeContact(folder, deviceId, id, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	removeEvent(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.removeEvent(folder, deviceId, id, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	removeNote(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.removeNote(folder, deviceId, id, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	removeTask(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.removeTask(folder, deviceId, id, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	updateAttachment(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "id") String id,
	    @WebParam(name = "attachmentId") String attachmentId,
	    @WebParam(name = "attachmentData") Attachment attachmentData,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface accessor = getKolabAccessor();
			return accessor.updateAttachment(folder, id, attachmentId, attachmentData, deviceId,
			    lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	updateContact(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "contact") Contact contact,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.updateContact(folder, deviceId, contact, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	updateEvent(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "event") Event event,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.updateEvent(folder, deviceId, event, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	updateNote(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "note") Note note,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.updateNote(folder, deviceId, note, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	@Override
	public boolean
	updateTask(@WebParam(name = "folder") Folder folder,
	    @WebParam(name = "deviceId") String deviceId,
	    @WebParam(name = "task") Task task,
	    @WebParam(name = "lastUpdateTime") Date lastUpdateTime)
	throws KolabServiceFault_Exception
	{
		try {
			KolabAccessorInterface kolab = getKolabAccessor();
			return kolab.updateTask(folder, deviceId, task, lastUpdateTime);
		} catch (Exception e) {
			throw createException(e);
		}
	}

	// helpers

	private synchronized KolabServiceFault_Exception
	createException(Exception exception)
	{
		if (exception instanceof KolabServiceFault_Exception)
			return (KolabServiceFault_Exception)exception;
		String message = "internal error";

		if (exception instanceof StoreClosedException) {
			message = "Failed to open folder!";
		}

		logger.error(message + " (id: " + errorId + ")", exception);
		KolabServiceFault ksf = new KolabServiceFault();
		ksf.setId(errorId);
		errorId++;
		return new KolabServiceFault_Exception(message, ksf);
	}

	private KolabAccessorInterface
	getKolabAccessor()
	{
		KolabAccessorInterface kolabAccessor;
		//		if (kolabAccessor == null) {
		String usermailid = getUsername();
		String password = getPassword();
		int idx = usermailid.indexOf('@');
		String userpath = usermailid.substring(0, idx);
		kolabAccessor = new KolabAccessor(
		    Profiler.profileQueryExecution(emf).createEntityManager(),
		    ServiceConf.get("imap.host"),
		    Integer.parseInt(ServiceConf.get("imap.port")),
		    Boolean.parseBoolean(ServiceConf.get("imap.ssl")),
		    usermailid, password, userpath
		);
		//		}
		if (Profiler.INSTANCE.isActive())
			kolabAccessor = Profiler.profileInstance(kolabAccessor);
		return kolabAccessor;
	}

	private String
	getPassword()
	{
		Message message = PhaseInterceptorChain.getCurrentMessage();
		UsernameToken token = (UsernameToken)message.getContextualProperty(
		    org.apache.cxf.common.security.SecurityToken.class.getName());
		/* there was a to-do note here, but we don’t know for what */
		return token.getPassword();
	}

	private String
	getUsername()
	{
		return sessionContext.getCallerPrincipal().getName();
	}
}
