package org.evolvis.bsi.kolab;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.Helper.FolderState;
import org.evolvis.bsi.kolab.db.DBFolderLock.FolderActionWithString;
import org.evolvis.bsi.kolab.db.DBSynchronizer;
import org.evolvis.bsi.kolab.db.DBUtil;
import org.evolvis.bsi.kolab.db.KolabItem;
import org.evolvis.bsi.kolab.imap.ImapHelper;
import org.evolvis.bsi.kolab.service.Attachment;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.IdAndChangeTime;
import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Task;
import org.evolvis.bsi.kolab.service.Validator;
import org.evolvis.bsi.kolab.xml.KolabParseException;
import org.evolvis.bsi.kolab.xml.KolabSerializer;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import static org.evolvis.bsi.kolab.imap.ImapHelper.getAttachmentXmlStream;
import static org.evolvis.bsi.kolab.imap.ImapHelper.getFoldersContact;
import static org.evolvis.bsi.kolab.imap.ImapHelper.getFoldersEvent;
import static org.evolvis.bsi.kolab.imap.ImapHelper.getFoldersNote;
import static org.evolvis.bsi.kolab.imap.ImapHelper.getFoldersTask;
import static org.evolvis.bsi.kolab.imap.ImapHelper.getKolabXmlStream;

/**
 * @author Hendrik Helwich
 */
public class KolabAccessor implements KolabAccessorInterface {
	private static final Logger logger = Logger.getLogger(KolabAccessor.class.getName());

	private static final boolean SESSION_DEBUG = false;
	protected IMAPStore store;
	private EntityManager entityManager;
	private String host;
	private String password;
	private int port;
	private Session session;
	private boolean useSSL;
	private String user;
	private String userPath;

	public KolabAccessor(EntityManager em, String host, int port, boolean useSSL,
	    String user, String password, String userPath)
	{
		this.entityManager = em;
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;
		this.userPath = userPath;
		this.useSSL = useSSL;
	}

	protected synchronized void
	open() throws MessagingException
	{
		if (store == null) {
			final Properties props = new Properties();
			final String prefix = useSSL ? "mail.imaps." : "mail.imap.";
			props.setProperty(prefix + "host", host);
			props.setProperty(prefix + "port", Integer.toString(port));
			props.setProperty(prefix + "user", user);
			props.setProperty(prefix + "pwd", password);
			session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication
				getPasswordAuthentication()
				{
					return new PasswordAuthentication(props.getProperty(prefix + "user"),
					    props.getProperty(prefix + "pwd"));
				}
			});
			session.setDebug(SESSION_DEBUG);
			store = (IMAPStore)session.getStore(useSSL ? "imaps" : "imap");
			store.connect();
		}
	}

	private IMAPFolder
	openFolder(final Folder folder, final boolean forceSync)
	throws MessagingException, KolabConfigException
	{
		open();
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());
		DBSynchronizer.syncFolderIds(ifolder, forceSync, entityManager, null);
		return ifolder;
	}

	// implementation

	@Override
	public boolean
	addAttachment(Folder folder, String id, String attachmentId, Attachment attachment,
	    String deviceId, Date lastUpdateTime)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		final IMAPFolder ifolder = openFolder(folder, true);
		KolabItem item = DBUtil.getVisibleItemForKolabId(entityManager, folder.getName(), id);
		if (item == null)
			throw new RuntimeException("item with id " + id +
			    " not found in folder " + ifolder.getFullName());
		final long newMailId = ImapHelper.addAttachment(session, ifolder,
		    item.getMailId(), attachment, attachmentId);
		DBUtil.substituteItemByKolabItem(entityManager, folder.getName(),
		    item.getMailId(), newMailId, item, deviceId, userPath);

		// return true if we reach this statement, else we would fail in a proper exception
		return true;
	}

	@Override
	public String
	addContact(Folder folder, String deviceId, Contact contact)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(contact);
		return addItem(folder, deviceId, contact);
	}

	@Override
	public String
	addEvent(Folder folder, String deviceId, Event event)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(event);
		return addItem(folder, deviceId, event);
	}

	@Override
	public String
	addNote(Folder folder, String deviceId, Note note)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(note);
		return addItem(folder, deviceId, note);
	}

	@Override
	public String
	addTask(Folder folder, String deviceId, Task task)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(task);
		return addItem(folder, deviceId, task);
	}

	@Override
	public Attachment
	getAttachment(Folder folder, String id, String attachmentId)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		return getAttachmentStream(entityManager, folder, id, false, attachmentId);
	}

	@Override
	public Contact
	getContact(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		InputStream in = getItemStream(entityManager, folder, id, false);
		if (in == null)
			return null;
		Contact contact = KolabSerializer.readContact(in);
		if (checkKolabId(entityManager, id, contact.getUid(), folder.getName()))
			return contact;
		return getContact(folder, id);
	}

	@Override
	public List<IdAndChangeTime>
	getContactChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDAndChangeTime(folder, updateTime);
	}

	@Override
	public List<Folder>
	getContactFolders()
	throws MessagingException, KolabConfigException
	{
		open();
		List<IMAPFolder> folders = getFoldersContact(store, userPath, false);
		return extractFolderNames(folders);
	}

	@Override
	public List<String>
	getContactIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDs(folder, updateTime);
	}

	// handled higher up: Date getCurrentTime();

	@Override
	public Event
	getEvent(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		InputStream in = getItemStream(entityManager, folder, id, false);
		if (in == null)
			return null;
		Event event = KolabSerializer.readEvent(in);
		if (checkKolabId(entityManager, id, event.getUid(), folder.getName()))
			return event;
		return getEvent(folder, id);
	}

	@Override
	public List<IdAndChangeTime>
	getEventChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDAndChangeTime(folder, updateTime);
	}

	@Override
	public List<Folder>
	getEventFolders()
	throws MessagingException, KolabConfigException
	{
		open();
		List<IMAPFolder> folders = getFoldersEvent(store, userPath, false);
		return extractFolderNames(folders);
	}

	@Override
	public List<String>
	getEventIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDs(folder, updateTime);
	}

	@Override
	public List<String>
	getNewContactIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getNewIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getNewEventIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getNewIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getNewNoteIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getNewIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getNewTaskIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getNewIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public Note
	getNote(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		InputStream in = getItemStream(entityManager, folder, id, false);
		if (in == null)
			return null;
		Note note = KolabSerializer.readNote(in);
		if (checkKolabId(entityManager, id, note.getUid(), folder.getName()))
			return note;
		return getNote(folder, id);
	}

	@Override
	public List<IdAndChangeTime>
	getNoteChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDAndChangeTime(folder, updateTime);
	}

	@Override
	public List<Folder>
	getNoteFolders()
	throws MessagingException, KolabConfigException
	{
		open();
		List<IMAPFolder> folders = getFoldersNote(store, userPath, false);
		return extractFolderNames(folders);
	}

	@Override
	public List<String>
	getNoteIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDs(folder, updateTime);
	}

	@Override
	public List<String>
	getRemovedContactIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getDeletedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getRemovedEventIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getDeletedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getRemovedNoteIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getDeletedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getRemovedTaskIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getDeletedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public Task
	getTask(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		InputStream in = getItemStream(entityManager, folder, id, false);
		if (in == null)
			return null;
		Task task = KolabSerializer.readTask(in);
		if (checkKolabId(entityManager, id, task.getUid(), folder.getName()))
			return task;
		return getTask(folder, id);
	}

	@Override
	public List<IdAndChangeTime>
	getTaskChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDAndChangeTime(folder, updateTime);
	}

	@Override
	public List<Folder>
	getTaskFolders()
	throws MessagingException, KolabConfigException
	{
		open();
		List<IMAPFolder> folders = getFoldersTask(store, userPath, false);
		return extractFolderNames(folders);
	}

	@Override
	public List<String>
	getTaskIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getItemIDs(folder, updateTime);
	}

	@Override
	public List<String>
	getUpdatedContactIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getUpdatedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getUpdatedEventIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getUpdatedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getUpdatedNoteIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getUpdatedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public List<String>
	getUpdatedTaskIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return getUpdatedIDs(folder, deviceId, lastUpdateTime, updateTime);
	}

	@Override
	public boolean
	removeAttachment(Folder folder, String id, String attachmentId,
	    String deviceId, Date lastUpdateTime)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		final IMAPFolder ifolder = openFolder(folder, true);
		KolabItem item = DBUtil.getVisibleItemForKolabId(entityManager,
		    folder.getName(), id);
		final long newMailId = ImapHelper.removeAttachment(session, ifolder,
		    item.getMailId(), attachmentId);
		DBUtil.substituteItemByKolabItem(entityManager, folder.getName(),
		    item.getMailId(), newMailId, item, deviceId, userPath);

		// return true if we reach this statement, else we would fail in a proper exception
		return true;
	}

	@Override
	public boolean
	removeContact(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		return removeItem(folder, deviceId, id, lastUpdateTime);
	}

	@Override
	public boolean
	removeEvent(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		return removeItem(folder, deviceId, id, lastUpdateTime);
	}

	@Override
	public boolean
	removeNote(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		return removeItem(folder, deviceId, id, lastUpdateTime);
	}

	@Override
	public boolean
	removeTask(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		return removeItem(folder, deviceId, id, lastUpdateTime);
	}

	@Override
	public boolean
	updateAttachment(Folder folder, String id, String attachmentId, Attachment attachment,
	    String deviceId, Date lastUpdateTime)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		final IMAPFolder ifolder = openFolder(folder, true);
		KolabItem item = DBUtil.getVisibleItemForKolabId(entityManager,
		    folder.getName(), id);
		final long newMailId = ImapHelper.updateAttachment(session, ifolder,
		    item.getMailId(), attachment, attachmentId);
		DBUtil.updateItemByKolabItem(entityManager, folder.getName(),
		    item.getMailId(), newMailId, item, deviceId, userPath);

		// return true if we reach this statement, else we would fail in a proper exception
		return true;
	}

	@Override
	public boolean
	updateContact(Folder folder, String deviceId, Contact contact, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(contact);
		return updateItem(folder, deviceId, contact, lastUpdateTime);
	}

	@Override
	public boolean
	updateEvent(Folder folder, String deviceId, Event event, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(event);
		return updateItem(folder, deviceId, event, lastUpdateTime);
	}

	@Override
	public boolean
	updateNote(Folder folder, String deviceId, Note note, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(note);
		return updateItem(folder, deviceId, note, lastUpdateTime);
	}

	@Override
	public boolean
	updateTask(Folder folder, String deviceId, Task task, Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		Validator.validate(task);
		return updateItem(folder, deviceId, task, lastUpdateTime);
	}

	// helper operations

	/**
	 * After reading an item from the kolab server this operation can be used to
	 * check if the kolab uid in the xml data is different to the id which is
	 * stored in the corresponding database entry of this application.
	 * If they are different the imap mail index was probably rebuild and all
	 * informations stored for this folder should be discarded.
	 */
	private static boolean
	checkKolabId(EntityManager entityManager, String id1, String id2, String folderName)
	{
		if (id1.equals(id2))
			return true;
		logger.warn("content of mail in folder " + folderName + " with uid " +
		    "did change! Mail index must have been rebuild. Delete all folder meta data.");
		DBUtil.deleteFolderItems(entityManager, folderName);
		return false;
	}

	/**
	 * Get the stream of the kolab xml data which is stored in the imap mail
	 * by the specified kolab uid and the imap folder.
	 * The parameter <code>forceSync</code> must be set to <code>false</code>.
	 */
	private InputStream
	getItemStream(EntityManager entityManager, Folder folder, String id, boolean forceSync)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		final IMAPFolder ifolder = openFolder(folder, forceSync);
		KolabItem item = DBUtil.getVisibleItemForKolabId(entityManager,
		    folder.getName(), id);
		if (item == null)
			return null;
		InputStream in = getKolabXmlStream(ifolder, item.getMailId());
		if (in != null || /* already */ forceSync)
			return in;
		// the mail must have been deleted since the last synchronisation
		// => redo operation and force sync before
		return getItemStream(entityManager, folder, id, true);
	}

	/**
	 * Get the stream of the kolab xml data which is stored in the imap mail
	 * by the specified kolab uid and the imap folder.
	 * The parameter <code>forceSync</code> must be set to <code>false</code>.
	 */
	private Attachment
	getAttachmentStream(EntityManager entityManager, Folder folder,
	    String id, boolean forceSync, String attachmentId)
	throws MessagingException, KolabConfigException, IOException, KolabParseException
	{
		final IMAPFolder ifolder = openFolder(folder, forceSync);
		KolabItem item = DBUtil.getVisibleItemForKolabId(entityManager,
		    folder.getName(), id);
		if (item == null)
			return null;
		Attachment in = getAttachmentXmlStream(ifolder, item.getMailId(), attachmentId);
		if (in != null || /* already */ forceSync)
			return in;
		// the mail must have been deleted since the last synchronisation
		// => redo operation and force sync before
		return getAttachmentStream(entityManager, folder, id, true, attachmentId);
	}

	private String
	addItem(final Folder folder, final String deviceId, final CollaborationItem item)
	throws MessagingException, KolabConfigException
	{
		open();
		//TODO set product id prefix
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());
		final String uid = getNewKolabUID();

		FolderActionWithString folderActionWithString = new FolderActionWithString() {
			private String resultuid;

			@Override
			public String
			getString()
			{
				return resultuid;
			}

			@Override
			public void
			synchronizedAction()
			throws MessagingException
			{
				final String itemuid = item.getUid();

				if (itemuid != null && !"".equals(itemuid.trim()) &&
				    DBUtil.getItemForKolabId(entityManager, folder.getName(), itemuid) == null) {
					resultuid = itemuid;
				} else if (DBUtil.getItemForKolabId(entityManager, folder.getName(), uid) != null) {
					throw new RuntimeException("uid is not unique, implementation cannot cope");
				} else {
					// overwrite UID only if not set or not usable
					item.setUid(uid);
					resultuid = uid;
				}

				// write mail
				long mailId = ImapHelper.writeKolabMailWithAttachment(session, ifolder, item,
				    getSenderEmailAddress(user), null);
				// create database entry
				DBUtil.createNewItem(entityManager, folder.getName(), item, mailId, deviceId, userPath);
			}
		};

		DBSynchronizer.syncFolderIds(ifolder, false, entityManager, folderActionWithString);
		return folderActionWithString.getString();
	}

	private boolean
	updateItem(final Folder folder, final String deviceId,
	    final CollaborationItem item, final Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		open();
		//TODO set product id prefix
		//TODO check item attachment fields must be null
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());
		final boolean[] ret = new boolean[] { false };
		DBSynchronizer.syncFolderIds(ifolder, false, entityManager, () -> {
			KolabItem kitem = DBUtil.getVisibleItemForKolabId(entityManager,
			    folder.getName(), item.getUid());
			Date lutEpsilon = new Date(lastUpdateTime.getTime() + 1000);
			if (kitem != null && kitem.getChangeDate().before(lutEpsilon)) {
				// create mail
				long newMailId = ImapHelper.writeKolabMail(session,
				    ifolder, item, getSenderEmailAddress(user));
				// delete mail
				if (!ImapHelper.deleteKolabMail(ifolder,
				    kitem.getMailId(), true)) {
					//TODO handle duplicate ?
					logger.warn("mail with uid " + kitem.getMailId() +
					    " in folder " + folder.getName() +
					    " could not been removed during update");
				}
				// modify database entry
				DBUtil.updateItem(entityManager, folder.getName(),
				    kitem.getMailId(), newMailId, item, deviceId, userPath);
				ret[0] = true;
			}
		});
		return ret[0];
	}

	private static String
	getNewKolabUID()
	{
		return UUID.randomUUID().toString();
	}

	private static String
	getSenderEmailAddress(String user)
	{
		return user;
	}

	private boolean
	removeItem(final Folder folder, final String deviceId,
	    final String id, final Date lastUpdateTime)
	throws MessagingException, KolabConfigException
	{
		open();
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());
		final boolean[] ret = new boolean[] { false };
		DBSynchronizer.syncFolderIds(ifolder, false, entityManager, () -> {
			KolabItem item = DBUtil.getVisibleItemForKolabId(entityManager,
			    folder.getName(), id);
			Date lutEpsilon = new Date(lastUpdateTime.getTime() + 1000);
			if (item != null && item.getChangeDate().before(lutEpsilon))
				// delete mail
				if (ImapHelper.deleteKolabMail(ifolder,
				    item.getMailId(), true)) {
					// modify database entry
					DBUtil.deleteItem(entityManager,
					    folder.getName(), item.getMailId(),
					    deviceId, userPath);
					ret[0] = true;
				}
		});
		return ret[0];
	}

	@SuppressWarnings("unchecked")
	private List<String>
	getItemIDs(final Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return (List<String>)getItemIDsEtc(folder, updateTime, false)[0];
	}

	@SuppressWarnings("unchecked")
	private List<IdAndChangeTime>
	getItemIDAndChangeTime(final Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		return (List<IdAndChangeTime>)getItemIDsEtc(folder, updateTime, true)[0];
	}

	private List<?>[]
	getItemIDsEtc(final Folder folder, final Date updateTime, final boolean withChangeTime)
	throws MessagingException, KolabConfigException
	{
		open();
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());
		final List<?>[] ret = new List[1];

		if (!Helper.getFolderState(entityManager, ifolder, user, null,
		    updateTime).isVisible()) {
			ret[0] = Collections.EMPTY_LIST;
			return ret;
		}

		DBSynchronizer.syncFolderIds(ifolder, false, entityManager, () ->
		    ret[0] = withChangeTime ?
			DBUtil.getVisibleItemsWithChangeTime(entityManager, folder.getName()) :
			DBUtil.getVisibleItems(entityManager, folder.getName()));
		return ret;
	}

	@SuppressWarnings("unchecked")
	private List<String>
	getNewIDs(final Folder folder, final String deviceId,
	    final Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		open();
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());

		final FolderState fstate = Helper.getFolderState(entityManager,
		    ifolder, user, lastUpdateTime, updateTime);
		if (!fstate.isVisible())
			return Collections.EMPTY_LIST;

		final List<String>[] ret = new List[1];
		DBSynchronizer.syncFolderIds(ifolder, false, entityManager, () -> {
			if (fstate.isChanged())
				// full sync => get all folder items
				ret[0] = DBUtil.getVisibleItems(entityManager, folder.getName());
			else
				// normal sync => get all new folder items
				ret[0] = DBUtil.getNewIDsForFolder(entityManager, folder.getName(),
				    lastUpdateTime, deviceId, userPath);
		});
		return ret[0];
	}

	@SuppressWarnings("unchecked")
	private List<String>
	getUpdatedIDs(final Folder folder, final String deviceId,
	    final Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		open();
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());

		final FolderState fstate = Helper.getFolderState(entityManager,
		    ifolder, user, null, updateTime);
		if (!fstate.isVisible() || fstate.isChanged())
			return Collections.EMPTY_LIST;

		final List<String>[] ret = new List[1];
		DBSynchronizer.syncFolderIds(ifolder, false, entityManager,
		    () -> ret[0] = DBUtil.getUpdatedIDsForFolder(entityManager,
			folder.getName(), lastUpdateTime, deviceId, userPath));
		return ret[0];
	}

	@SuppressWarnings("unchecked")
	private List<String>
	getDeletedIDs(final Folder folder, final String deviceId,
	    final Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException
	{
		open();
		final IMAPFolder ifolder = (IMAPFolder)store.getFolder(folder.getName());

		final FolderState fstate = Helper.getFolderState(entityManager,
		    ifolder, user, lastUpdateTime, updateTime);
		if (fstate.isVisible() == fstate.isChanged())
			return Collections.EMPTY_LIST;

		final List<String>[] ret = new List[1];
		DBSynchronizer.syncFolderIds(ifolder, false, entityManager, () -> {
			if (fstate.isChanged()) {
				// full sync => get all folder items which are visible last time
				ret[0] = DBUtil.getBeforeVisibleItemIDs(entityManager,
				    folder.getName(), lastUpdateTime);
			} else {
				// normal sync => get all deleted folder items
				ret[0] = DBUtil.getDeletedIDsForFolder(entityManager,
				    folder.getName(), lastUpdateTime, deviceId, userPath);
			}
		});
		return ret[0];
	}

	// transformation of types

	private static List<Folder>
	extractFolderNames(List<IMAPFolder> folders)
	{
		List<Folder> retList = new ArrayList<>(folders.size());
		for (IMAPFolder folder : folders) {
			Folder f = new Folder();
			f.setName(folder.getFullName());
			retList.add(f);
		}
		return retList;
	}
}
