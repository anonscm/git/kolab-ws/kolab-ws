package org.evolvis.bsi.kolab;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolder;
import org.evolvis.bsi.kolab.db.DBUtil;
import org.evolvis.bsi.kolab.db.FolderVisibility;
import org.evolvis.bsi.kolab.db.ImapFolder;
import org.evolvis.bsi.kolab.db.User;
import org.evolvis.bsi.kolab.imap.ImapHelper;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

/**
 * @author Hendrik Helwich
 */
public class Helper {
	private Helper()
	{
	}

	private static Boolean IS_IN_TEST_MODE;

	public static final boolean
	isInTestMode()
	{
		if (IS_IN_TEST_MODE == null) {
			IS_IN_TEST_MODE = false;
			for (StackTraceElement te : new Throwable().getStackTrace())
				if (te.getClassName().startsWith("org.junit")) {
					IS_IN_TEST_MODE = true;
					break;
				}
		}
		return IS_IN_TEST_MODE;
	}

	private static String VERSION;

	static {
		if (isInTestMode()) {
			VERSION = "in test mode";
		} else {
			try {
				Properties prop = new Properties();
				prop.load(Helper.class
				    .getResourceAsStream("/META-INF/maven/org.evolvis.bsi/kolab-ws/pom.properties"));
				VERSION = prop.getProperty("version");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String
	getVersion()
	{
		return VERSION;
	}

	/**
	 * Pipe the content of the input stream to the output stream
	 */
	public static void
	copyStream(InputStream in, OutputStream out) throws IOException
	{
		byte[] buffer = new byte[1028];
		int len;
		while ((len = in.read(buffer)) > 0)
			out.write(buffer, 0, len);
	}

	/**
	 * Get the actual visibility of the given folder. It is also returned if the
	 * folder state has changed since the last synchronization of this folder by
	 * the given user.
	 */
	public static synchronized FolderState
	getFolderState(EntityManager manager, IMAPFolder folder, String userName,
	    Date lastSyncTime, Date syncTime)
	throws MessagingException, KolabConfigException
	{
		String folderName = folder.getFullName();

		Boolean stateNow = DBUtil.isSyncFolder(manager, folderName, userName, syncTime, false);
		// check the folder visibility stored in the db. If null this is the first synchronization of this folder by the given user
		Boolean stateLast = DBUtil.isSyncFolder(manager, folderName, userName, lastSyncTime, true);

		if (stateNow == null) { // store actual folder state from imap annotations in the db
			// check the folder visibility stored in the imap
			stateNow = ImapHelper.isSyncFolder(folder);

			if (stateLast == null || stateNow != stateLast) { // store actual imap state in the db
				User user = DBUtil.addUser(manager, userName);

				EntityTransaction transaction = manager.getTransaction();
				transaction.begin();

				try {
					ImapFolder dbFolder = DBUtil.getFolder(manager, folderName);

					if (dbFolder == null) {
						dbFolder = new ImapFolder(folder.getFullName(),
						    ImapHelper.getFolderType(folder));
						manager.persist(dbFolder);
					}

					FolderVisibility v = new FolderVisibility();
					if (syncTime == null)
						syncTime = new Date();
					v.setChangeDate(syncTime);
					v.setVisible(stateNow);
					v.setUser(user);
					v.setFolder(dbFolder);
					manager.persist(v);
					transaction.commit();
				} finally {
					if (transaction.isActive())
						transaction.rollback();
				}
			}
		}

		return new FolderState(stateNow,
		    stateLast == null ? false : stateNow != stateLast);
	}

	public static class FolderState {
		private final boolean visible;
		private final boolean changed;

		public FolderState(boolean visible, boolean changed)
		{
			this.visible = visible;
			this.changed = changed;
		}

		public boolean
		isVisible()
		{
			return visible;
		}

		public boolean
		isChanged()
		{
			return changed;
		}
	}
}
