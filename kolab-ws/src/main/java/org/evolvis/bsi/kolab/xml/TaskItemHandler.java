package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.Status;
import org.evolvis.bsi.kolab.service.Task;
import org.evolvis.bsi.kolab.service.Validator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.Date;
import java.util.Locale;

import static org.evolvis.bsi.kolab.xml.Helper.parseDate;
import static org.evolvis.bsi.kolab.xml.Helper.parseDatetime;
import static org.evolvis.bsi.kolab.xml.Helper.parseInteger;

/**
 * Used to fill the kolab xml data which is task specific to an {@link Task}
 * instance.
 *
 * @author Hendrik Helwich
 */
public class TaskItemHandler extends ItemHandler<Task> {
	private Task task = new Task();

	@Override
	public void
	checkRootName(String name) throws KolabParseException
	{
		if (!ELEMENT_TASK.equals(name.toLowerCase(Locale.ENGLISH)))
			throw new KolabParseException("unexpected task root element name: " + name);
	}

	@Override
	public boolean
	readElement(String name, XMLStreamReader parser)
	throws XMLStreamException, KolabParseException
	{
		if (readElementIncidence(task, name, parser))
			return true;
		else if (ELEMENT_PRIORITY.equals(name.toLowerCase(Locale.ENGLISH)))
			task.setPriority(parseInteger(parser.getElementText()));
		else if (ELEMENT_COMPLETED.equals(name.toLowerCase(Locale.ENGLISH)))
			task.setCompleted(parseInteger(parser.getElementText()));
		else if (ELEMENT_STATUS.equals(name.toLowerCase(Locale.ENGLISH))) {
			String status = parser.getElementText();
			if (VALUE_STATUS_NOT_STARTED.equals(status.toLowerCase(Locale.ENGLISH)))
				task.setStatus(Status.NOT_STARTED);
			else if (VALUE_STATUS_IN_PROGRESS.equals(status.toLowerCase(Locale.ENGLISH)))
				task.setStatus(Status.IN_PROGRESS);
			else if (VALUE_STATUS_COMPLETED.equals(status.toLowerCase(Locale.ENGLISH)))
				task.setStatus(Status.COMPLETED);
			else if (VALUE_STATUS_WAITING_ON_SOMEONE_ELSE.equals(status.toLowerCase(Locale.ENGLISH)))
				task.setStatus(Status.WAITING_ON_SOMEONE_ELSE);
			else if (VALUE_STATUS_DEFERRED.equals(status.toLowerCase(Locale.ENGLISH)))
				task.setStatus(Status.DEFERRED);
			else
				throw new KolabParseException("invalid task status value: '" + status + "'");
		} else if (ELEMENT_DUE_DATE.equals(name.toLowerCase(Locale.ENGLISH))) {
			String dateStr = parser.getElementText();
			Date date;
			try {
				date = parseDatetime(dateStr);
				task.setDueDateAllDay(false);
			} catch (KolabParseException e) { // maybe all day date
				try {
					date = parseDate(dateStr);
					task.setDueDateAllDay(true);
				} catch (KolabParseException e2) {
					e2.initCause(e);
					throw e2;
				}
			}
			task.setDueDate(date);
		} else if (ELEMENT_PARENT.equals(name.toLowerCase(Locale.ENGLISH)))
			task.setParent(parser.getElementText());
		else
			return false;
		return true;
	}

	@Override
	public void
	validate() throws KolabParseException
	{
		try {
			Validator.validate(task);
		} catch (IllegalArgumentException e) {
			throw new KolabParseException(e.getMessage());
		}
	}

	@Override
	public Task
	getItem()
	{
		return task;
	}
}
