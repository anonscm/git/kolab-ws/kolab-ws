package org.evolvis.bsi.kolab;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.profiler.Profiler;
import org.evolvis.bsi.kolab.service.Attachment;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.IdAndChangeTime;
import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Task;
import org.evolvis.bsi.kolab.xml.KolabParseException;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * This interface is needed to wrap the class {@link KolabAccessor} by a
 * profiling proxy.
 *
 * @author Hendrik Helwich
 * @see KolabAccessor
 * @see Profiler#profileInstance(Object)
 */
public interface KolabAccessorInterface {
	//TODO use interface KolabServicePortType instead of this one

	boolean
	addAttachment(Folder folder, String id, String attachmentId, Attachment attachment,
	    String deviceId, Date lastUpdateTime)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	String
	addContact(Folder folder, String deviceId, Contact contact)
	throws MessagingException, KolabConfigException;

	String
	addEvent(Folder folder, String deviceId, Event event)
	throws MessagingException, KolabConfigException;

	String
	addNote(Folder folder, String deviceId, Note note)
	throws MessagingException, KolabConfigException;

	String
	addTask(Folder folder, String deviceId, Task task)
	throws MessagingException, KolabConfigException;

	Attachment
	getAttachment(Folder folder, String id, String attachmentId)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	Contact
	getContact(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	List<IdAndChangeTime>
	getContactChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<Folder>
	getContactFolders()
	throws MessagingException, KolabConfigException;

	List<String>
	getContactIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	// handled higher up: Date getCurrentTime();

	Event
	getEvent(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	List<IdAndChangeTime>
	getEventChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<Folder>
	getEventFolders()
	throws MessagingException, KolabConfigException;

	List<String>
	getEventIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getNewContactIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getNewEventIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getNewNoteIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getNewTaskIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	Note
	getNote(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	List<IdAndChangeTime>
	getNoteChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<Folder>
	getNoteFolders()
	throws MessagingException, KolabConfigException;

	List<String>
	getNoteIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getRemovedContactIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getRemovedEventIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getRemovedNoteIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getRemovedTaskIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	Task
	getTask(Folder folder, String id)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	List<IdAndChangeTime>
	getTaskChangeTimes(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<Folder>
	getTaskFolders()
	throws MessagingException, KolabConfigException;

	List<String>
	getTaskIDs(Folder folder, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getUpdatedContactIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getUpdatedEventIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getUpdatedNoteIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	List<String>
	getUpdatedTaskIDs(Folder folder, String deviceId, Date lastUpdateTime, Date updateTime)
	throws MessagingException, KolabConfigException;

	boolean
	removeAttachment(Folder folder, String id, String attachmentId,
	    String deviceId, Date lastUpdateTime)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	boolean
	removeContact(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	removeEvent(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	removeNote(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	removeTask(Folder folder, String deviceId, String id, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	updateAttachment(Folder folder, String id, String attachmentId, Attachment attachment,
	    String deviceId, Date lastUpdateTime)
	throws MessagingException, KolabConfigException, IOException, KolabParseException;

	boolean
	updateContact(Folder folder, String deviceId, Contact contact, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	updateEvent(Folder folder, String deviceId, Event event, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	updateNote(Folder folder, String deviceId, Note note, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;

	boolean
	updateTask(Folder folder, String deviceId, Task task, Date lastUpdateTime)
	throws MessagingException, KolabConfigException;
}
