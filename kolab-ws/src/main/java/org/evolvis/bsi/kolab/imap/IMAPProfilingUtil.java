package org.evolvis.bsi.kolab.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.iap.Argument;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.AppendUID;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPFolder.ProtocolCommand;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPResponse;
import org.evolvis.bsi.kolab.profiler.Profiler;

import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

/**
 * Collection of static operations to wrap operations in the java mail api which
 * are used in {@link ImapHelper}. If the kolab ws profiler is active, runtime
 * informations of this calls are written to a log file.
 *
 * @author Hendrik Helwich
 * @see Profiler#isActive()
 */
class IMAPProfilingUtil {
	// first parameter is the instance on which the method is called followed
	// by the arguments

	// folder

	public static Message
	IMAPFFolder_getMessageByUID(IMAPFolder folder, long uid)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.getMessageByUID(uid);
		return (Message)profileInvokeMExcep(folder, "getMessageByUID",
		    (long)uid);
	}

	public static int
	IMAPFFolder_getMode(IMAPFolder folder)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.getMode();
		return (Integer)Profiler.profileInvoke(folder, "getMode");
	}

	public static AppendUID[]
	IMAPFolder_appendUIDMessages(IMAPFolder folder, Message[] messages)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.appendUIDMessages(messages);
		return (AppendUID[])profileInvokeMExcep(folder,
		    "appendUIDMessages", (Object)messages);
	}

	public static void
	IMAPFolder_close(IMAPFolder folder, boolean expunge)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			folder.close(expunge);
		else
			profileInvokeMExcep(folder, "close", expunge);
	}

	public static Object
	IMAPFolder_doOptionalCommand(IMAPFolder folder, String err, ProtocolCommand cmd)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.doOptionalCommand(err, cmd);
		return (Object)profileInvokeMExcep(folder, "doOptionalCommand",
		    err, cmd);
	}

	public static Message[]
	IMAPFolder_expunge(IMAPFolder folder)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.expunge();
		return (Message[])profileInvokeMExcep(folder, "expunge");
	}

	public static String
	IMAPFolder_getFullName(Folder folder)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.getFullName();
		return (String)Profiler.profileInvoke(folder, "getFullName");
	}

	public static IMAPStore
	IMAPFolder_getStore(IMAPFolder folder)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return (IMAPStore)folder.getStore();
		return (IMAPStore)Profiler.profileInvoke(folder, "getStore");
	}

	public static boolean
	IMAPFolder_isOpen(IMAPFolder folder)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.isOpen();
		return (Boolean)Profiler.profileInvoke(folder, "isOpen");
	}

	public static Folder[]
	IMAPFolder_list(IMAPFolder folder)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return folder.list();
		return (Folder[])profileInvokeMExcep(folder, "list");
	}

	public static void
	IMAPFolder_open(IMAPFolder folder, int mode)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			folder.open(mode);
		else
			profileInvokeMExcep(folder, "open", mode);
	}

	// store

	public static Folder
	IMAPStore_getFolder(IMAPStore store, String name)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return store.getFolder(name);
		return (Folder)profileInvokeMExcep(store, "getFolder", name);
	}

	public static IMAPProtocol
	IMAPStore_getProtocol(IMAPStore store, IMAPFolder folder)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return store.getProtocol(folder);
		return (IMAPProtocol)profileInvokeMExcep(store, "getProtocol",
		    folder);
	}

	public static Folder[]
	IMAPStore_getUserNamespaces(IMAPStore store, String user)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return store.getUserNamespaces(user);
		return (Folder[])profileInvokeMExcep(store, "getUserNamespaces",
		    user);
	}

	// message

	@SuppressWarnings("unchecked")
	public static Enumeration<Header>
	IMAPMessage_getAllHeaders(Message message)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return message.getAllHeaders();
		return (Enumeration<Header>)profileInvokeMExcep(message,
		    "getAllHeaders");
	}

	public static Object
	IMAPMessage_getContent(Message message)
	throws IOException, MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return message.getContent();
		return (Object)profileInvokeMEIOxcep(message, "getContent");
	}

	public static String
	IMAPMessage_getSubject(Message message)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return message.getSubject();
		return (String)profileInvokeMExcep(message, "getSubject");
	}

	public static void
	IMAPMessage_setContent(Message message, MimeMultipart multipart)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			message.setContent(multipart);
		else
			profileInvokeMExcep(message, "setContent", multipart);
	}

	public static void
	IMAPMessage_setFlag(Message msg, Flag flag, boolean set)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			msg.setFlag(flag, set);
		else
			profileInvokeMExcep(msg, "setFlag", flag, set);
	}

	public static void
	IMAPMessage_setFrom(Message msg, InternetAddress address)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			msg.setFrom(address);
		else
			profileInvokeMExcep(msg, "setFrom", address);
	}

	public static void
	IMAPMessage_setHeader(Message message, String name, String value)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			message.setHeader(name, value);
		else
			profileInvokeMExcep(message, "setHeader", name, value);
	}

	public static void
	IMAPMessage_setSubject(Message message, String subject)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			message.setSubject(subject);
		else
			profileInvokeMExcep(message, "setSubject", subject);
	}

	// other

	public static String
	IMAPBodyPart_getContentType(BodyPart bp)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return bp.getContentType();
		return (String)profileInvokeMExcep(bp, "getContentType");
	}

	public static String[]
	IMAPBodyPart_getHeader(BodyPart bp, String headerName)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return bp.getHeader(headerName);
		return (String[])profileInvokeMExcep(bp, "getHeader", headerName);
	}

	public static InputStream
	IMAPBodyPart_getInputStream(BodyPart bp)
	throws IOException, MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return bp.getInputStream();
		InputStream is = (InputStream)profileInvokeMEIOxcep(bp,
		    "getInputStream");
		return is;// TODOProfiler.profileInstance(is); // also profile inputstream
	}

	public static InternetAddress
	IMAPInternetAddress_new(String address)
	throws AddressException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return new InternetAddress(address);
		// TODO profile constructor
		return new InternetAddress(address);
	}

	public static void
	IMAPInternetHeaders_addHeaderLine(InternetHeaders attachmentBodyHeaders, String line)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			attachmentBodyHeaders.addHeaderLine(line);
		else
			Profiler.profileInvoke(attachmentBodyHeaders, "addHeaderLine", line);
	}

	public static InternetHeaders
	IMAPInternetHeaders_new()
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return new InternetHeaders();
		// TODO profile constructor
		return new InternetHeaders();
	}

	public static MimeBodyPart
	IMAPMimeBodyPart_new(InternetHeaders headers, byte[] content)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return new MimeBodyPart(headers, content);
		// TODO profile constructor
		return new MimeBodyPart(headers, content);
	}

	public static void
	IMAPMimeBodyPart_setDescription(MimeBodyPart mbp, String description)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			mbp.setDescription(description);
		else
			profileInvokeMExcep(mbp, "setDescription", description);
	}

	public static void
	IMAPMimeBodyPart_setFileName(MimeBodyPart mbp, String filename)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			mbp.setFileName(filename);
		else
			profileInvokeMExcep(mbp, "setFileName", filename);
	}

	public static Message
	IMAPMimeMessage_new(Session session)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return new MimeMessage(session);
		// TODO profile constructor
		return new MimeMessage(session);
	}

	public static void
	IMAPMimeMultipart_addBodyPart(MimeMultipart multipart, MimeBodyPart part)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			multipart.addBodyPart(part);
		else
			profileInvokeMExcep(multipart, "addBodyPart", part);
	}

	public static BodyPart
	IMAPMimeMultipart_getBodyPart(MimeMultipart mmp, int index)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return mmp.getBodyPart(index);
		return (BodyPart)profileInvokeMExcep(mmp, "getBodyPart", index);
	}

	public static int
	IMAPMimeMultipart_getCount(MimeMultipart mmp)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return mmp.getCount();
		return (Integer)profileInvokeMExcep(mmp, "getCount");
	}

	public static void
	IMAPMimeMultipart_removeBodyPart(MimeMultipart multipart, int index)
	throws MessagingException
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			multipart.removeBodyPart(index);
		else
			profileInvokeMExcep(multipart, "removeBodyPart", index);
	}

	public static Response[]
	IMAPProtocol_command(IMAPProtocol protocol, String commandGetannotation, Argument args)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return protocol.command(commandGetannotation, args);
		return (Response[])Profiler.profileInvoke(protocol, "command",
		    commandGetannotation, args);
	}

	public static String
	IMAPResponse_getKey(IMAPResponse iresp)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return iresp.getKey();
		return (String)Profiler.profileInvoke(iresp, "getKey");
	}

	public static String
	IMAPResponse_getRest(IMAPResponse iresp)
	{
		if (!Profiler.INSTANCE.isActive()) // only needed for better performance
			return iresp.getRest();
		return (String)Profiler.profileInvoke(iresp, "getRest");
	}

	private IMAPProfilingUtil()
	{
	}

	/**
	 * Proxy method to
	 * {@link Profiler#profileInvokeCatch(Object, String, Object...)}
	 * which is expecting that only {@link MessagingException}s can be thrown
	 * by the invoked method and therefore wrap other exception in a
	 * {@link RuntimeException}.
	 */
	public static Object
	profileInvokeMExcep(Object obj, String methodName, Object... args)
	throws MessagingException
	{
		try {
			return Profiler.profileInvokeCatch(obj, methodName, args);
		} catch (MessagingException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException("unexpected exception", e);
		}
	}

	/**
	 * Proxy method to
	 * {@link Profiler#profileInvokeCatch(Object, String, Object...)}
	 * which is expecting that only {@link MessagingException}s or
	 * {@link IOException}s can be thrown by the invoked method and therefore
	 * wrap other exception in a {@link RuntimeException}.
	 */
	public static Object
	profileInvokeMEIOxcep(Object obj, String methodName, Object... args)
	throws MessagingException, IOException
	{
		try {
			return Profiler.profileInvokeCatch(obj, methodName, args);
		} catch (MessagingException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException("unexpected exception", e);
		}
	}
}
