package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.service.ServiceConf;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This thread should only be used by the {@link Profiler} class.
 * If the profiler is active, one instance of this thread could be started to
 * periodically log the CPU load of the current process.
 * Needs to be on a Linux System with procfs under "/proc" and with the
 * "ps" command installed.
 *
 * @author Hendrik Helwich
 */
class CPUMonitor extends Thread {
	private String pid; // process id of the current process
	private static final Logger logger = Logger.getLogger(CPUMonitor.class.getName());

	public CPUMonitor()
	{
		try {
			// try to get java PID via the procfs
			String pid = new File("/proc/self").getCanonicalFile().getName();
			try {
				// check if the PID is a valid number
				Integer.parseInt(pid);
				this.pid = pid; // PID is ok => store
				logger.info("process id is: " + pid);
			} catch (NumberFormatException e) {
				logger.warn("cannot get process id via procfs", e);
			}
		} catch (IOException e) {
			logger.warn("cannot get process id via procfs", e);
		}
	}

	@Override
	public void
	run()
	{
		if (pid == null) // unable to get pid in construction => do not start thread
			return;
		byte[] buf = new byte[10];
		int bufSize;
		while (!isInterrupted()) {
			// get CPU load
			long time;
			try {
				Runtime runtime = Runtime.getRuntime();
				time = System.currentTimeMillis();
				Process p = runtime.exec("ps -p " + pid + " -o pcpu=");
				InputStream is = p.getInputStream();
				InputStream es = p.getErrorStream();
				OutputStream os = p.getOutputStream();
				bufSize = is.read(buf);
				if (bufSize <= 0 || bufSize >= buf.length)
					break;
				// middle of the time measured before and after the call
				time += (System.currentTimeMillis() - time) / 2;
				is.close();
				// seems that its needed to close unneeded streams to prevent
				// an IOException on longer runtimes.
				es.close();
				os.close();
			} catch (IOException e) {
				logger.warn("cannot get CPU load", e);
				break;
			}
			// parse CPU load
			float load;
			try {
				load = Float.parseFloat(new String(buf, 0, bufSize));
			} catch (NumberFormatException e) {
				logger.warn("cannot get CPU load", e);
				break;
			}
			// log CPU load
			Profiler.INSTANCE.logCpu(time, load);
			// sleep a configured interval
			try {
				String millis = ServiceConf.get("profiler.cpu.interval");
				long millis_;
				try {
					millis_ = Long.parseLong(millis);
				} catch (NumberFormatException e) {
					logger.warn("profiler CPU interval is misconfigured");
					millis_ = 1000;
				}
				sleep(millis_);
			} catch (InterruptedException e) {
				break;
			}
		}
	}
}
