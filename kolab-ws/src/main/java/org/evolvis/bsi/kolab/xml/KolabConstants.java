package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/**
 * See: http://www.kolab.org/doc/kolabformat-2.0rc7-html/index.html
 *
 * @author Hendrik Helwich, tarent GmbH
 */
public interface KolabConstants {
	public static final String ATTRIBUTE_VERSION = "version";
	public static final String VALUE_VERSION_1_0 = "1.0";

	public static final String VALUE_TRUE = "true";
	public static final String VALUE_FALSE = "false";

	// general collaboration item constants

	public static final String ELEMENT_UID =
	    "uid";                         //    1, string,
	public static final String ELEMENT_BODY =
	    "body";                        // 0..1, string,   default ""
	public static final String ELEMENT_CATEGORIES =
	    "categories";                  // 0..1, string,   default ""
	public static final String ELEMENT_CREATION_DATE =
	    "creation-date";               //    1, datetime
	public static final String ELEMENT_LAST_MODIFICATION_DATE =
	    "last-modification-date";      //    1, datetime
	public static final String ELEMENT_SENSITIVITY =
	    "sensitivity";                 // 0..1, string,   default "public"
	public static final String ELEMENT_INLINE_ATTACHMENT =
	    "inline-attachment";           // 0..N, string,   no default
	public static final String ELEMENT_LINK_ATTACHMENT =
	    "link-attachment";             // 0..N, string,   no default
	public static final String ELEMENT_PRODUCT_ID =
	    "product-id";                  // 0..1, string,   default ""
	public static final String VALUE_SENSITIVITY_PRIVATE =
	    "private";
	public static final String VALUE_SENSITIVITY_CONFIDENTIAL =
	    "confidential";
	public static final String VALUE_SENSITIVITY_PUBLIC =
	    "public";

	// incidence constants

	public static final String ELEMENT_SUMMARY =
	    "summary";                     // string, default empty
	public static final String ELEMENT_LOCATION =
	    "location";                    // string, default empty
	public static final String ELEMENT_ORGANIZER =
	    "organizer";
	public static final String ELEMENT_ORGANIZER_DISPLAY_NAME =
	    "display-name";                // string, default empty
	public static final String ELEMENT_ORGANIZER_SMTP_ADDRESS =
	    "smtp-address";                // string, default empty
	public static final String ELEMENT_START_DATE =
	    "start-date";                  // date or datetime, default not present
	public static final String ELEMENT_ALARM =
	    "alarm";                       // number, no default
	public static final String ELEMENT_RECURRENCE =
	    "recurrence";
	public static final String ATTRIBUTE_RECURRENCE_CYCLE =
	    "cycle";                       // 1,
	public static final String VALUE_RECURRENCE_CYCLE_DAILY =
	    "daily";
	public static final String VALUE_RECURRENCE_CYCLE_WEEKLY =
	    "weekly";
	public static final String VALUE_RECURRENCE_CYCLE_MONTHLY =
	    "monthly";
	public static final String VALUE_RECURRENCE_CYCLE_YEARLY =
	    "yearly";
	public static final String ATTRIBUTE_RECURRENCE_TYPE =
	    "type";                        // 0..1,
	public static final String VALUE_RECURRENCE_TYPE_DAYNUMBER =  // possible cycles: monthly
	    "daynumber";
	public static final String VALUE_RECURRENCE_TYPE_WEEKDAY =    // possible cycles: monthly, yearly
	    "weekday";
	public static final String VALUE_RECURRENCE_TYPE_MONTHDAY =   // possible cycles: yearly
	    "monthday";
	public static final String VALUE_RECURRENCE_TYPE_YEARDAY =    // possible cycles: yearly
	    "yearday";
	public static final String ELEMENT_RECURRENCE_INTERVAL =
	    "interval";                    // number, default 1
	public static final String ELEMENT_RECURRENCE_DAY =
	    "day";                         // 0..N, string, no default
	public static final String[] VALUE_RECURRENCE_DAY = new String[] {
	    "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"
	};
	public static final String ELEMENT_RECURRENCE_DAYNUMBER =
	    "daynumber";                   // number, no default
	public static final String ELEMENT_RECURRENCE_MONTH =
	    "month";
	public static final String[] VALUE_RECURRENCE_MONTH = new String[] {
	    "january", "february", "march", "april", "may", "june", "july",
	    "august", "september", "october", "november", "december"
	};
	public static final String ELEMENT_RECURRENCE_RANGE =
	    "range";                       // date or number or nothing, no default
	public static final String ATTRIBUTE_RECURRENCE_RANGE_TYPE =
	    "type";
	public static final String VALUE_RECURRENCE_RANGE_TYPE_NONE =
	    "none";
	public static final String VALUE_RECURRENCE_RANGE_TYPE_NUMBER =
	    "number";
	public static final String VALUE_RECURRENCE_RANGE_TYPE_DATE =
	    "date";
	public static final String ELEMENT_RECURRENCE_EXCLUSION =
	    "exclusion";                   // 0..N, date, no default
	public static final String ELEMENT_ATTENDEE =
	    "attendee";                    // 0..N,
	public static final String ELEMENT_ATTENDEE_DISPLAY_NAME =
	    "display-name";                // string, default empty
	public static final String ELEMENT_ATTENDEE_SMTP_ADDRESS =
	    "smtp-address";                // string, default empty
	public static final String ELEMENT_ATTENDEE_STATUS =
	    "status";                      // string, default none
	public static final String VALUE_ATTENDEE_STATUS_NONE =
	    "none";
	public static final String VALUE_ATTENDEE_STATUS_TENTATIVE =
	    "tentative";
	public static final String VALUE_ATTENDEE_STATUS_ACCEPTED =
	    "accepted";
	public static final String VALUE_ATTENDEE_STATUS_DECLINED =
	    "declined";
	public static final String VALUE_ATTENDEE_STATUS_NEEDS_ACTION =
	    "needs-action";
	public static final String VALUE_ATTENDEE_STATUS_DELEGATED =
	    "delegated";
	public static final String ELEMENT_ATTENDEE_REQUEST_RESPONSE =
	    "request-response";            // bool, default true

	public static final String ELEMENT_ATTENDEE_ROLE =
	    "role";                        // string, default required
	public static final String VALUE_ATTENDEE_ROLE_REQUIRED =
	    "required";
	public static final String VALUE_ATTENDEE_ROLE_OPTIONAL =
	    "optional";
	public static final String VALUE_ATTENDEE_ROLE_RESOURCE =
	    "resource";
	public static final String VALUE_ATTENDEE_ROLE_REQ_PARTICIPANT =
	    "req-participant";
	// event constants

	public static final String ELEMENT_EVENT =
	    "event";
	public static final String ELEMENT_SHOW_TIME_AS =
	    "show-time-as";                // string, default busy, one of: free, tentative, busy, or outofoffice.
	public static final String VALUE_SHOW_TIME_AS_FREE =
	    "free";
	public static final String VALUE_SHOW_TIME_AS_TENTATIVE =
	    "tentative";
	public static final String VALUE_SHOW_TIME_AS_BUSY =
	    "busy";
	public static final String VALUE_SHOW_TIME_AS_OUTOFOFFICE =
	    "outofoffice";
	public static final String ELEMENT_COLOR_LABEL =
	    "color-label";                 // string, default none
	public static final String VALUE_COLOR_LABEL_NONE =
	    "none";
	public static final String VALUE_COLOR_LABEL_IMPORTANT =
	    "important";
	public static final String VALUE_COLOR_LABEL_BUSINESS =
	    "business";
	public static final String VALUE_COLOR_LABEL_PERSONAL =
	    "personal";
	public static final String VALUE_COLOR_LABEL_VACATION =
	    "vacation";
	public static final String VALUE_COLOR_LABEL_MUST_ATTEND =
	    "must-attend";
	public static final String VALUE_COLOR_LABEL_TRAVEL_REQUIRED =
	    "travel-required";
	public static final String VALUE_COLOR_LABEL_NEEDS_PREPARATION =
	    "needs-preparation";
	public static final String VALUE_COLOR_LABEL_BIRTHDAY =
	    "birthday";
	public static final String VALUE_COLOR_LABEL_ANNIVERSARY =
	    "anniversary";
	public static final String VALUE_COLOR_LABEL_PHONE_CALL =
	    "phone-call";
	public static final String ELEMENT_END_DATE =
	    "end-date";                    // date or datetime, default not present

	// contact constants

	public static final String ELEMENT_CONTACT =
	    "contact";
	public static final String ELEMENT_NAME =
	    "name";                        // grouping field
	public static final String ELEMENT_NAME_GIVEN_NAME =
	    "given-name";                  // string, default empty
	public static final String ELEMENT_NAME_MIDDLE_NAMES =
	    "middle-names";                // string, default empty
	public static final String ELEMENT_NAME_LAST_NAME =
	    "last-name";                   // string, default empty
	public static final String ELEMENT_NAME_FULL_NAME =
	    "full-name";                   // string, default empty
	public static final String ELEMENT_NAME_INITIALS =
	    "initials";                    // string, default empty
	public static final String ELEMENT_NAME_PREFIX =
	    "prefix";                      // string, default empty
	public static final String ELEMENT_NAME_SUFFIX =
	    "suffix";                      // string, default empty
	// end of name fields
	public static final String ELEMENT_FREE_BUSY_URL =
	    "free-busy-url";               // string, default empty
	public static final String ELEMENT_ORGANIZATION =
	    "organization";                // string, default empty
	public static final String ELEMENT_WEB_PAGE =
	    "web-page";                    // string, default empty
	public static final String ELEMENT_IM_ADDRESS =
	    "im-address";                  // string, default empty
	public static final String ELEMENT_DEPARTMENT =
	    "department";                  // string, default empty
	public static final String ELEMENT_OFFICE_LOCATION =
	    "office-location";             // string, default empty
	public static final String ELEMENT_PROFESSION =
	    "profession";                  // string, default empty
	public static final String ELEMENT_JOB_TITLE =
	    "job-title";                   // string, default empty
	public static final String ELEMENT_MANAGER_NAME =
	    "manager-name";                // string, default empty
	public static final String ELEMENT_ASSISTANT =
	    "assistant";                   // string, default empty
	public static final String ELEMENT_NICK_NAME =
	    "nick-name";                   // string, default empty
	public static final String ELEMENT_SPOUSE_NAME =
	    "spouse-name";                 // string, default empty
	public static final String ELEMENT_BIRTHDAY =
	    "birthday";                    // date, no default
	public static final String ELEMENT_ANNIVERSARY =
	    "anniversary";                 // date, no default
	public static final String ELEMENT_PICTURE =
	    "picture";                     // string(attachment filename), default empty
	public static final String ELEMENT_CHILDREN =
	    "children";                    // string, default empty
	public static final String ELEMENT_GENDER =
	    "gender";                      // string, default empty
	public static final String VALUE_GENDER_MALE =
	    "male";
	public static final String VALUE_GENDER_FEMALE =
	    "female";
	public static final String ELEMENT_LANGUAGE =
	    "language";                    // string, default empty
	public static final String ELEMENT_PHONE =
	    "phone";                       // grouping field
	public static final String ELEMENT_PHONE_TYPE =
	    "type";                        // string, no default
	public static final String VALUE_PHONE_TYPE_BUSINESS_1 =
	    "business1";
	public static final String VALUE_PHONE_TYPE_BUSINESS_2 =
	    "business2";
	public static final String VALUE_PHONE_TYPE_BUSINESSFAX =
	    "businessfax";
	public static final String VALUE_PHONE_TYPE_CALLBACK =
	    "callback";
	public static final String VALUE_PHONE_TYPE_CAR =
	    "car";
	public static final String VALUE_PHONE_TYPE_COMPANY =
	    "company";
	public static final String VALUE_PHONE_TYPE_HOME_1 =
	    "home1";
	public static final String VALUE_PHONE_TYPE_HOME_2 =
	    "home2";
	public static final String VALUE_PHONE_TYPE_HOMEFAX =
	    "homefax";
	public static final String VALUE_PHONE_TYPE_ISDN =
	    "isdn";
	public static final String VALUE_PHONE_TYPE_MOBILE =
	    "mobile";
	public static final String VALUE_PHONE_TYPE_PAGER =
	    "pager";
	public static final String VALUE_PHONE_TYPE_PRIMARY =
	    "primary";
	public static final String VALUE_PHONE_TYPE_RADIO =
	    "radio";
	public static final String VALUE_PHONE_TYPE_TELEX =
	    "telex";
	public static final String VALUE_PHONE_TYPE_TTYTDD =
	    "ttytdd";
	public static final String VALUE_PHONE_TYPE_ASSISTANT =
	    "assistant";
	public static final String VALUE_PHONE_TYPE_OTHER =
	    "other";
	public static final String ELEMENT_PHONE_NUMBER =
	    "number";                      // string, default empty
	// end of phone fields
	public static final String ELEMENT_EMAIL =
	    "email";                       // grouping field
	public static final String ELEMENT_EMAIL_DISPLAY_NAME =
	    "display-name";                // string, default empty
	public static final String ELEMENT_EMAIL_SMTP_ADDRESS =
	    "smtp-address";                // string, default empty
	// end of email fields
	public static final String ELEMENT_ADDRESS =
	    "address";                     // grouping field
	public static final String ELEMENT_ADDRESS_TYPE =
	    "type";                        // string, default empty
	public static final String VALUE_ADDRESS_TYPE_HOME =
	    "home";
	public static final String VALUE_ADDRESS_TYPE_BUSINESS =
	    "business";
	public static final String VALUE_ADDRESS_TYPE_OTHER =
	    "other";
	public static final String ELEMENT_ADDRESS_STREET =
	    "street";                      // string, default empty
	public static final String ELEMENT_ADDRESS_LOCALITY =
	    "locality";                    // string, default empty
	public static final String ELEMENT_ADDRESS_REGION =
	    "region";                      // string, default empty
	public static final String ELEMENT_ADDRESS_POSTAL_CODE =
	    "postal-code";                 // string, default empty
	public static final String ELEMENT_ADDRESS_COUNTRY =
	    "country";                     // string, default empty
	// end of address fields
	public static final String ELEMENT_PREFERRED_ADDRESS =
	    "preferred-address";           // string, default none
	public static final String VALUE_PREFERRED_ADDRESS_NONE =
	    "none";
	public static final String ELEMENT_LATITUDE =
	    "latitude";                    // float, no default
	public static final String ELEMENT_LONGITUDE =
	    "longitude";                   // float, no default

	// task constants

	public static final String ELEMENT_TASK =
	    "task";
	public static final String ELEMENT_PRIORITY =
	    "priority";                      // number, default 3
	public static final String ELEMENT_COMPLETED =
	    "completed";                      // number, default 0
	public static final String ELEMENT_STATUS =
	    "status";                      // string, default not-started
	public static final String ELEMENT_DUE_DATE =
	    "due-date";                      // date or datetime, default not present
	public static final String ELEMENT_PARENT =
	    "parent";                      // string, default empty
	public static final String VALUE_STATUS_NOT_STARTED =
	    "not-started";
	public static final String VALUE_STATUS_IN_PROGRESS =
	    "in-progress";
	public static final String VALUE_STATUS_COMPLETED =
	    "completed";
	public static final String VALUE_STATUS_WAITING_ON_SOMEONE_ELSE =
	    "waiting-on-someone-else";
	public static final String VALUE_STATUS_DEFERRED =
	    "deferred";

	// note constants

	public static final String ELEMENT_NOTE =
	    "note";
	public static final String ELEMENT_NOTE_SUMMARY =
	    "summary";                      // string, default empty
	public static final String ELEMENT_BACKGROUND_COLOR =
	    "background-color";                      // color, default #000000
	public static final String ELEMENT_FOREGROUND_COLOR =
	    "foreground-color";                      // color, default #ffff00
}
