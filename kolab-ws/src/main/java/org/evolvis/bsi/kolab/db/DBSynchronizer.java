package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolder;
import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.KolabConfigException;
import org.evolvis.bsi.kolab.db.DBFolderLock.FolderAction;
import org.evolvis.bsi.kolab.imap.ImapHelper;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.IncidenceWindowSubset;
import org.evolvis.bsi.kolab.service.Task;
import org.evolvis.bsi.kolab.util.FolderType;
import org.evolvis.bsi.kolab.xml.KolabParseException;
import org.evolvis.bsi.kolab.xml.KolabSerializer;

import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.evolvis.bsi.kolab.imap.ImapHelper.getKolabXmlStream;

/**
 * This class is holding the logic of synchronizing the kolab folder item ids
 * with the database used by this application.
 *
 * @author Hendrik Helwich
 */
public class DBSynchronizer {
	private static final Logger logger = Logger.getLogger(DBSynchronizer.class.getName());

	/**
	 * If the time difference of a synchronization of a folder is below this
	 * interval, nothing is done in operation "syncFolderIds".
	 */
	public static final long MIN_FOLDER_SYNC_INTERVAL = 1000;

	public static void
	syncFolderIds(IMAPFolder folder, EntityManager entityManager)
	throws MessagingException, KolabConfigException
	{
		syncFolderIds(folder, false, entityManager, null);
	}

	public static void
	syncFolderIds(IMAPFolder folder, boolean force)
	throws MessagingException, KolabConfigException
	{
		syncFolderIds(folder, force, null, null);
	}

	/**
	 * Check the given imap folder and update the database which is used by this
	 * application if necessary.
	 * This operation is synchronized for the same folder names, so it can
	 * safely be use by more than one thread.
	 * If the <code>force</code> argument is <code>null</code> and this
	 * operation is called before for the same folder and the time
	 * difference is below the value {@link #MIN_FOLDER_SYNC_INTERVAL}, nothing
	 * is done.
	 *
	 * @param folder     The imap folder which should be checked.
	 * @param folderType The type of this imap folder
	 * @param force      If this value is <code>true</code>, an update is always done,
	 *                   also if it is done before and the time difference is below the
	 *                   value {@link #MIN_FOLDER_SYNC_INTERVAL}.
	 */
	public static void
	syncFolderIds(final IMAPFolder folder, final boolean force,
	    final EntityManager entityManager, final FolderAction postAction)
	throws MessagingException, KolabConfigException
	{
		DBFolderLock.getInstance().doSynchronized(folder.getFullName(), new FolderAction() {
			@Override
			public void synchronizedAction() throws MessagingException, KolabConfigException
			{
				syncFolderIdsSynchronized(folder, force, entityManager);
				if (postAction != null)
					postAction.synchronizedAction();
			}
		});
	}

	/**
	 * At this point the operation is synchronized for the same imap folders
	 *
	 * @see #syncFolderIds(IMAPFolder, FolderType, boolean)
	 */
	private static void
	syncFolderIdsSynchronized(IMAPFolder folder, boolean force,
	    EntityManager entityManager)
	throws MessagingException, KolabConfigException
	{
		long now = System.currentTimeMillis();
		final String folderName = folder.getFullName();
		if (force || now - getSyncLastStartTime(folderName) > MIN_FOLDER_SYNC_INTERVAL) {
			setSyncStartTime(folderName, now);
			syncFolderIdsMain(folder, entityManager);
		} else
			logger.info("skipping synchronizing of imap folder " + folderName);
	}

	private static final Map<String, Long> SYNC_START_TIME = new HashMap<String, Long>();
	private static int syncStructPutCounter = 0;

	private static synchronized long
	getSyncLastStartTime(String folderName)
	{
		Long value = SYNC_START_TIME.get(folderName);
		return value == null ? 0 : value;
	}

	private static synchronized void
	setSyncStartTime(String folderName, long time)
	{
		SYNC_START_TIME.put(folderName, time);
		if (++syncStructPutCounter > 100) { // remove unused elements from struct
			long cpr = System.currentTimeMillis() - MIN_FOLDER_SYNC_INTERVAL;
			List<String> removeKeys = null;
			for (String key : SYNC_START_TIME.keySet()) {
				Long value = SYNC_START_TIME.get(key);
				if (cpr > value) {
					if (removeKeys == null)
						removeKeys = new LinkedList<String>();
					removeKeys.add(key);
				}
			}
			if (removeKeys != null)
				for (String key : removeKeys)
					SYNC_START_TIME.remove(key);
			syncStructPutCounter = 0;
		}
	}

	/**
	 * The main synchronization logic.
	 * It is assured before, that this operation is synchronized for the same
	 * folder names and is not called for the same folder name below the time
	 * difference {@link #MIN_FOLDER_SYNC_INTERVAL}.
	 */
	private static void
	syncFolderIdsMain(IMAPFolder folder, EntityManager entityManager)
	throws MessagingException, KolabConfigException
	{
		final String folderName = folder.getFullName();

		logger.info("Synchronizing ids of imap folder " + folderName + " with DB");

		final FolderType folderType;

		// get folder instance and folder type
		ImapFolder fdr = DBUtil.getFolder(entityManager, folderName);
		if (fdr == null)
			fdr = new ImapFolder(folder.getFullName(), ImapHelper.getFolderType(folder));
		folderType = fdr.getType();

		if (!folder.isOpen())
			folder.open(Folder.READ_ONLY);

		// get folder ids from db
		List<KolabItem> items;
		if (fdr.getPk() == null)
			items = Collections.emptyList();
		else
			items = DBUtil.getNotDeltedItems(entityManager, folderName);

		List<KolabItem> outdatedItems = null; // gets only initialized for event folder type
		if (folderType.isEvent())
			if (fdr.getPk() == null)
				outdatedItems = Collections.emptyList();
			else
				outdatedItems = DBUtil.getOutdatedEvents(entityManager, folderName,
				    IncidenceWindowSubset.getSyncWindowStart().getTime());

		//		System.out.println("1-------------------------------------------------------------");
		//		for (KolabItem i : items)
		//			System.out.println(i.getMailId());

		//		final long lbuid = items.size() == 0 ? Long.MIN_VALUE : // the biggest uid for this folder in db => all new imap messages must have a higher id
		//			items.get(items.size()-1).getMailId();

		// get folder message instances from imap server
		Message[] messages = folder.getMessages(); // in fact class: IMAPMessage

		//		System.out.println("2-------------------------------------------------------------");
		//		for (Message m : messages) {
		//			System.out.println(folder.getUID(m)+ " : "+(m.isSet(Flag.DELETED)?"Del":"   "));
		//		}

		// iterate over all uids in the db and check if they are still in the same state
		int itemIndex = 0, messageIndex = 0, previousItemIndex = -1, previousMessageIndex =
		    -1; // list indexes (also indexes of previous iteration)
		KolabItem item = null, previousItem; // db item and item of previous iteration
		long uid = Long.MIN_VALUE, previousUid; // imap mail uid and uid of previous iteration

		final UniqueIdIterable itemsToPersist = new UniqueIdIterable();
		final Set<ItemForMailId> itemsToDelete = new HashSet<ItemForMailId>();

		for (boolean itemsProcessed = itemIndex >= items.size(), messagesProcessed =
		    messageIndex >= messages.length;
		    !itemsProcessed || !messagesProcessed;
		    itemsProcessed = itemIndex >= items.size(), messagesProcessed =
			messageIndex >= messages.length) { // iterate over lists till both are finished

			if (!itemsProcessed && itemIndex != previousItemIndex) { // get next db id
				previousItem = item;
				item = items.get(itemIndex);
				if (previousItem != null && item.getMailId() <=
				    previousItem.getMailId()) // ids are not strictly monotonic increasing
					throw new RuntimeException("order by clause did not work as expected");
			}
			previousItemIndex = itemIndex;
			boolean markedDeleted = false;
			if (!messagesProcessed && messageIndex != previousMessageIndex) { // get next imap id
				Message msg = messages[messageIndex];
				long te2 = uid;
				try {
					uid = folder.getUID(msg);
					previousUid = te2;
				} catch (MessageRemovedException e) {
					messageIndex++;
					continue;
				}
				if (uid <= previousUid) // ids are not strictly monotonic increasing
					throw new RuntimeException(
					    "mail api did not return strictly monotonic increasing uids as expected");
				markedDeleted = msg.isSet(Flag.DELETED);
			}
			previousMessageIndex = messageIndex;
			//
			if (itemsProcessed || (item.getMailId() > uid &&
			    !messagesProcessed)) { // imap id e2 is missing in db or is marked deleted
				// if mail is marked deleted check if there is an entry in the db and skip reading of the mail in this case
				if (markedDeleted && fdr.getPk() != null) {
					KolabItem existingItem = DBUtil.getItemForMailId(entityManager, folderName,
					    uid);
					if (existingItem != null) {
						messageIndex++;
						continue;
					}
				}
				/*
				 * if it is an event, check if the db entry is marked deleted
				 * because the event is outdated. In this case leave the db
				 * entry deleted and resume synchronization
				 */
				if (folderType.isEvent() && containsMailId(outdatedItems, uid)) {
					messageIndex++;
					continue;
				}

				//				if (e2 <= lbuid) // mail id is to low. this can be due to manual remove of entries form the db (for example to reread an item which was invalid after a code change is done) or to a reorganization of the imap ids
				//					logger.warning("to low mail uid "+e2+" occured in imap folder "+folderName+". item is deleted from database or the imap index was rebuilded");

				// read meta data from kolab mail to create later a new entry in the kolab item table
				CollaborationItem citem = null;
				String kolabId = null;
				boolean valid = true;
				try {
					InputStream is = getKolabXmlStream(folder, uid);
					if (is != null) {
						if (folderType.isEvent())
							citem = KolabSerializer.readEvent(is);
						else if (folderType.isContact())
							citem = KolabSerializer.readContact(is);
						else if (folderType.isTask())
							citem = KolabSerializer.readTask(is);
						else if (folderType.isNote())
							citem = KolabSerializer.readNote(is);
						else
							throw new UnsupportedOperationException();
						kolabId = citem.getUid();
					} else // mail is deleted by another application while this operation is running
						markedDeleted = true;
				} catch (RuntimeException e) { // for example:  com.ctc.wstx.exc.WstxLazyException
					logger.error("Failed to parse kolab XML from mail with UID: " + uid +
					    " from imap folder: " + folderName + ". Marking item as invalid.", e);
					valid = false;
				} catch (KolabParseException e) {
					logger.error("Failed to parse kolab XML from mail with UID: " + uid +
					    " from imap folder: " + folderName + ". Marking item as invalid.", e);
					valid = false;
				} catch (IOException e) {
					logger.error("Failed to read kolab XML from mail with UID: " + uid +
					    " from imap folder: " + folderName + ". Skipping this item.", e);
					messageIndex++;
					continue;
				}

				KolabItem existingItem = DBUtil.getItemForKolabId(entityManager, folderName, kolabId);
				if (existingItem == null) {
					itemsToDelete.add(new ItemForMailId(folderName, uid));
					existingItem = new KolabItem();
				} else if (item != null && item.getMailId() <
				    existingItem.getMailId()) { // this mail is outdated => skip import
					messageIndex++;
					continue;
				} else if (uid < existingItem.getMailId()) { // this mail is outdated => skip import
					messageIndex++;
					continue;
				}

				// get creation date of message
				Message message = folder.getMessageByUID(uid); // can return null
				if (message == null) { // message must be deleted recently => skip import
					messageIndex++;
					continue;
				}
				Date creationDate;
				try {
					creationDate = message.getReceivedDate();
				} catch (MessageRemovedException e) { // message must be deleted recently => skip import
					messageIndex++;
					continue;
				}
				Date now = new Date();
				if (creationDate.after(now))
					creationDate = now;
				//ensure that creation date is after the last successful sync time of the folder
				Date lastSyncTime = fdr.getLastSyncTime();
				if (lastSyncTime != null && !creationDate.after(lastSyncTime))
					creationDate = new Date(lastSyncTime.getTime() + 1);

				if (folderType.isEvent())
					setEventDates(existingItem, (Event)citem);
				else if (folderType.isTask())
					setTaskDates(existingItem, (Task)citem);
				existingItem.setKolabId(kolabId);
				existingItem.setValid(valid);
				existingItem.setFolder(fdr);
				existingItem.setCreationDate(creationDate);
				existingItem.setChangeDate(creationDate);
				existingItem.setDeleted(markedDeleted);
				existingItem.setMailId(uid);
				existingItem.setChangeDeviceId(null); //TODO
				existingItem.setChangeUserId(null); //TODO
				itemsToPersist.add(existingItem);
				messageIndex++;
			} else if (messagesProcessed || item.getMailId() < uid) { // db id e1 is deleted
				item.setDeleted(true);
				item.setChangeDeviceId(null);
				item.setChangeUserId(null);
				item.setChangeDate(new Date());
				itemsToPersist.add(item);
				itemIndex++;
			} else { // db id is still available in the imap
				if (markedDeleted) { // mark mail in corresponding db entry as deleted
					item.setDeleted(true);
					item.setChangeDeviceId(null);
					item.setChangeUserId(null);
					item.setChangeDate(new Date());
					itemsToPersist.add(item);
				} else if (folderType.isEvent() && isOutdated(
				    item)) { // event is outdated => mark mail in corresponding db entry as deleted
					item.setDeleted(true);
					item.setChangeDeviceId(null);
					item.setChangeUserId(null);
					item.setChangeDate(new Date());
					itemsToPersist.add(item);
				}
				itemIndex++;
				messageIndex++;
			}
		}
		// persist changes
		EntityTransaction transaction = entityManager.getTransaction();
		final ImapFolder folderToUpdate = fdr;
		final EntityManager finalEntityManager = entityManager;

		DBUtil.executeInTransaction(transaction, new Runnable() {
			@Override
			public void run()
			{
				folderToUpdate.setLastSyncTime(new Date());
				finalEntityManager.persist(folderToUpdate);

				for (ItemForMailId itemToDelete : itemsToDelete) {
					DBUtil.deleteItemForMailId(finalEntityManager, itemToDelete.folderName,
					    itemToDelete.uid);
				}

				//				System.out.println("3-------------------------------------------------------------");
				for (KolabItem itemToPersist : itemsToPersist) {
					//					System.out.println(item.getMailId()+" : "+item.getKolabId()+" : "+item.getDeleted());
					try {
						// if entry with the same mail id exists => remove before writing
						finalEntityManager.persist(itemToPersist);
					} catch (PersistenceException e) {
						logger.error("Failed to persist kolab item: " +
						    itemToPersist.getKolabId(), e);
						throw e;
					}
				}
			}
		});
	}

	/**
	 * Returns <code>true</code> if the given item list contains an item with
	 * the given mail id.
	 * The items in the given list must be ascended sorted by the mailId.
	 */
	private static boolean
	containsMailId(List<KolabItem> items, long malId)
	{
		KolabItem it = new KolabItem();
		it.setMailId(malId);
		int index = Collections.binarySearch(items, it, new Comparator<KolabItem>() {
			@Override
			public int compare(KolabItem o1, KolabItem o2)
			{
				long thisVal = o1.getMailId();
				long anotherVal = o2.getMailId();
				return (thisVal < anotherVal ? -1 : (thisVal == anotherVal ? 0 : 1));
			}
		});
		return index >= 0;
	}

	private static boolean
	isOutdated(KolabItem item)
	{
		if (item.getIncidenceLastDate() != null)
			return item.getIncidenceLastDate().before(IncidenceWindowSubset.getSyncWindowStart());
		return false;
	}

	static void
	setTaskDates(KolabItem item, Task task)
	{
		// TODO implement a window for tasks?
		item.setIncidenceFirstDate(null);
		item.setIncidenceLastDate(null);
	}

	static void
	setEventDates(KolabItem item, Event event)
	{
		if (event != null) {
			Date firstDate = event.getStartDate(); // mandatory
			if (firstDate == null)
				throw new NullPointerException("start date must not be null in event");
			Date lastDate = event.getEndDate();  // optional
			if (lastDate == null)
				lastDate = firstDate;
			else if (event
			    .isAllDay()) // increase end date by one day if it is an allday event. This is because the time will be 00:00 in this case
				lastDate = new Date(lastDate.getTime() + 1000L * 60 * 60 * 24);
			// TODO handle recurrence
			if (event.getRecurrence() != null)
				lastDate = MAX_END_DATE;
			item.setIncidenceFirstDate(firstDate);
			item.setIncidenceLastDate(lastDate);
		}
	}

	// maximal date which can be stored in the db
	private static final Date MAX_END_DATE = new Date(253402300799000L);
	// Helper.parseDatetime("9999-12-31T23:59:59Z").getTime()

	/**
	 * A simple list structure. Items which have a kolab uid which is not
	 * <code>null</code> and which is added before are dismissed.
	 */
	private static class UniqueIdIterable implements Iterable<KolabItem> {
		private Map<String, KolabItem> items = new HashMap<String, KolabItem>();
		private List<KolabItem> itemsUidNull = new LinkedList<KolabItem>();

		public void
		add(KolabItem item)
		{
			if (item.getKolabId() == null)
				itemsUidNull.add(item);
			else
				items.put(item.getKolabId(), item);
		}

		@Override
		public Iterator<KolabItem>
		iterator()
		{
			return new Iterator<KolabItem>() {
				private Iterator<KolabItem> it1 = items.values().iterator();
				private Iterator<KolabItem> it2 = itemsUidNull.iterator();

				@Override
				public boolean
				hasNext()
				{
					return it1.hasNext() || it2.hasNext();
				}

				@Override
				public KolabItem
				next()
				{
					return it1.hasNext() ? it1.next() : it2.next();
				}

				@Override
				public void
				remove()
				{
					throw new UnsupportedOperationException();
				}
			};
		}
	}

	private static class ItemForMailId {
		private String folderName;
		private Long uid;

		public ItemForMailId(String folder, Long uid)
		{
			this.folderName = folder;
			this.uid = uid;
		}

		@Override
		public boolean
		equals(Object o)
		{
			if (this == o)
				return true;
			if (!(o instanceof ItemForMailId))
				return false;

			ItemForMailId that = (ItemForMailId)o;

			if (folderName != null ? !folderName.equals(that.folderName) : that.folderName != null)
				return false;
			return !(uid != null ? !uid.equals(that.uid) : that.uid != null);
		}

		@Override
		public int
		hashCode()
		{
			int result = folderName != null ? folderName.hashCode() : 0;
			result = 31 * result + (uid != null ? uid.hashCode() : 0);
			return result;
		}
	}
}
