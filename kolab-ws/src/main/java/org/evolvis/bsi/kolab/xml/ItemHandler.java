package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.service.Attendee;
import org.evolvis.bsi.kolab.service.AttendeeStatus;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Cycle;
import org.evolvis.bsi.kolab.service.Incidence;
import org.evolvis.bsi.kolab.service.Recurrence;
import org.evolvis.bsi.kolab.service.Role;
import org.evolvis.bsi.kolab.service.Sensitivity;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.Date;
import java.util.Locale;

import static org.evolvis.bsi.kolab.xml.Helper.parseDate;
import static org.evolvis.bsi.kolab.xml.Helper.parseDatetime;

/**
 * Abstract class which can be used to fill the kolab xml data to a specific
 * instance of {@link CollaborationItem}.
 *
 * @author Hendrik Helwich
 */
public abstract class ItemHandler<T extends CollaborationItem>
    implements KolabConstants, XMLStreamConstants {
	protected static final Logger logger = Logger.getLogger(ItemHandler.class.getName());

	public abstract void
	checkRootName(String name) throws KolabParseException;
	public abstract boolean
	readElement(String name, XMLStreamReader parser) throws XMLStreamException, KolabParseException;
	public abstract void
	validate() throws KolabParseException;
	public abstract T
	getItem();

	protected static Date
	readDate(XMLStreamReader parser)
	throws KolabParseException, XMLStreamException
	{
		return parseDate(parser.getElementText());
	}

	static Date
	readDateTime(XMLStreamReader parser)
	throws KolabParseException, XMLStreamException
	{
		return parseDatetime(parser.getElementText());
	}

	protected static Date
	readDateOrDateTime(XMLStreamReader parser)
	throws KolabParseException, XMLStreamException
	{
		String text = parser.getElementText();
		try {
			return parseDatetime(text);
		} catch (KolabParseException e) {
			return parseDate(text);
		}
	}

	protected static Boolean
	readBoolean(XMLStreamReader parser)
	throws KolabParseException, XMLStreamException
	{
		String value = parser.getElementText().trim();
		if (VALUE_TRUE.equals(value.toLowerCase(Locale.ENGLISH)))
			return Boolean.TRUE;
		else if (VALUE_FALSE.equals(value.toLowerCase(Locale.ENGLISH)))
			return Boolean.FALSE;
		else
			throw new KolabParseException("invalid boolean string: '" + value + "'");
	}

	/**
	 * Checks if the current xml element is an incidence field.
	 * In this case the element content is read and added to the given instance
	 * and the value <code>true</code> is returned. The parser is located on the
	 * ending element node.
	 * Otherwise nothing is done and the value <code>false</code> is returned.
	 */
	protected static boolean
	readElementIncidence(Incidence incidence, String name, XMLStreamReader parser)
	throws XMLStreamException, KolabParseException
	{
		if (readElementCollaborationItem(incidence, name, parser))
			return true;
		if (ELEMENT_SUMMARY.equals(name.toLowerCase(Locale.ENGLISH)))
			incidence.setSummary(parser.getElementText());
		else if (ELEMENT_LOCATION.equals(name.toLowerCase(Locale.ENGLISH)))
			incidence.setLocation(parser.getElementText());
		else if (ELEMENT_ORGANIZER.equals(name.toLowerCase(Locale.ENGLISH))) {
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_ORGANIZER_DISPLAY_NAME.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							incidence.setOrganizerDisplayName(parser.getElementText());
						else if (ELEMENT_ORGANIZER_SMTP_ADDRESS.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							incidence.setOrganizerSmtpAddress(parser.getElementText());
						else {
							logger.info("unhandled kolab data element: " +
							    ELEMENT_ORGANIZER + "/" + name); //TODO extract operation
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
		} else if (ELEMENT_START_DATE.equals(name.toLowerCase(Locale.ENGLISH))) {
			String dateStr = parser.getElementText();
			Date date;
			try {
				date = parseDatetime(dateStr);
				incidence.setAllDay(false);
			} catch (KolabParseException e) { // maybe all day date
				try {
					date = parseDate(dateStr);
					incidence.setAllDay(true);
				} catch (KolabParseException e2) {
					e2.initCause(e);
					throw e2;
				}
			}
			incidence.setStartDate(date);
		} else if (ELEMENT_ALARM.equals(name.toLowerCase(Locale.ENGLISH)))
			incidence.setAlarm(readInteger(name, parser));
		else if (ELEMENT_RECURRENCE.equals(name.toLowerCase(Locale.ENGLISH))) {
			Recurrence recurrence = new Recurrence();
			// read attributes
			Cycle cycle;
			try {
				String cycleStr = parser.getAttributeValue(null, ATTRIBUTE_RECURRENCE_CYCLE);
				if (VALUE_RECURRENCE_CYCLE_DAILY.equals(cycleStr.toLowerCase(Locale.ENGLISH)))
					cycle = Cycle.DAILY;
				else if (VALUE_RECURRENCE_CYCLE_WEEKLY.equals(cycleStr.toLowerCase(Locale.ENGLISH)))
					cycle = Cycle.WEEKLY;
				else if (VALUE_RECURRENCE_CYCLE_MONTHLY.equals(cycleStr.toLowerCase(Locale.ENGLISH)))
					cycle = Cycle.MONTHLY;
				else if (VALUE_RECURRENCE_CYCLE_YEARLY.equals(cycleStr.toLowerCase(Locale.ENGLISH)))
					cycle = Cycle.YEARLY;
				else
					throw new KolabParseException(
					    "recurrence cycle attribute value " + cycleStr + " is invalid");
				recurrence.setCycle(cycle);
			} catch (IllegalArgumentException e) {
				throw new KolabParseException("recurrence cycle attribute is invalid", e);
			}
			String type = parser.getAttributeValue(null, ATTRIBUTE_RECURRENCE_TYPE);
			int weekDays = 0;
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_RECURRENCE_INTERVAL.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							recurrence.setInterval(readInteger(name, parser));
						else if (ELEMENT_RECURRENCE_DAY.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							String day = parser.getElementText();
							int i = 0;
							for (; i < 7; i++)
								if (VALUE_RECURRENCE_DAY[i].equals(
								    day.toLowerCase(Locale.ENGLISH))) {
									weekDays |= 1 << i;
									break;
								}
							if (i == 7)
								throw new KolabParseException(
								    "invalid recurrence day " + day);
						} else if (ELEMENT_RECURRENCE_DAYNUMBER.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							Integer dnr = readInteger(name, parser);
							if (cycle == Cycle.MONTHLY) {
								if (VALUE_RECURRENCE_TYPE_DAYNUMBER.equals(
								    type.toLowerCase(Locale.ENGLISH)))
									recurrence.setMonthDay(dnr);
								else if (VALUE_RECURRENCE_TYPE_WEEKDAY.equals(
								    type.toLowerCase(Locale.ENGLISH)))
									recurrence.setWeek(dnr);
								else
									throw new KolabParseException(
									    "invalid recurrence type " + type);
							} else if (cycle == Cycle.YEARLY) {
								if (VALUE_RECURRENCE_TYPE_MONTHDAY.equals(
								    type.toLowerCase(Locale.ENGLISH)))
									recurrence.setMonthDay(dnr);
								else if (VALUE_RECURRENCE_TYPE_YEARDAY.equals(
								    type.toLowerCase(Locale.ENGLISH)))
									recurrence.setYearDay(dnr);
								else if (VALUE_RECURRENCE_TYPE_WEEKDAY.equals(
								    type.toLowerCase(Locale.ENGLISH)))
									recurrence.setWeek(dnr);
								else
									throw new KolabParseException(
									    "invalid recurrence type " + type);
							}
						} else if (ELEMENT_RECURRENCE_MONTH.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							String month = parser.getElementText();
							int i = 0;
							for (; i < 12; i++)
								if (VALUE_RECURRENCE_MONTH[i].equals(
								    month.toLowerCase(Locale.ENGLISH))) {
									recurrence.setMonth(i + 1);
									break;
								}
							if (i == 12)
								throw new KolabParseException(
								    "invalid recurrence month " + month);
						} else if (ELEMENT_RECURRENCE_RANGE.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							String rtype = parser.getAttributeValue(null,
							    ATTRIBUTE_RECURRENCE_RANGE_TYPE);
							if (VALUE_RECURRENCE_RANGE_TYPE_NONE.equals(
							    rtype.toLowerCase(Locale.ENGLISH))) {
								parser.getElementText(); // read till end tag
								recurrence.setRangeNumber(0);
							} else if (VALUE_RECURRENCE_RANGE_TYPE_NUMBER.equals(
							    rtype.toLowerCase(Locale.ENGLISH)))
								recurrence.setRangeNumber(readInteger(name, parser));
							else if (VALUE_RECURRENCE_RANGE_TYPE_DATE.equals(
							    rtype.toLowerCase(Locale.ENGLISH)))
								recurrence.setRangeDate(readDate(parser));
							else
								throw new KolabParseException(
								    "invalid range type " + rtype);
						} else if (ELEMENT_RECURRENCE_EXCLUSION.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							Date exclusion = readDate(parser);
							recurrence.getExclusion().add(exclusion);
						} else {
							logger.info("unhandled kolab data element: " + name);
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
			if (weekDays > 0)
				recurrence.setWeekDays(weekDays);
			incidence.setRecurrence(recurrence);
		} else if (ELEMENT_ATTENDEE.equals(name.toLowerCase(Locale.ENGLISH))) {
			Attendee attendee = new Attendee();
			for (int level = 0; level >= 0 && parser.hasNext(); ) {
				parser.next();
				int eventType = parser.getEventType();
				if (START_ELEMENT == eventType) {
					level++;
					name = parser.getLocalName();
					if (level == 1) {
						if (ELEMENT_ATTENDEE_DISPLAY_NAME.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							attendee.setDisplayName(parser.getElementText());
						else if (ELEMENT_ATTENDEE_SMTP_ADDRESS.equals(
						    name.toLowerCase(Locale.ENGLISH)))
							attendee.setSmtpAddress(parser.getElementText());
						else if (ELEMENT_ATTENDEE_STATUS.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							String status = parser.getElementText();
							if (VALUE_ATTENDEE_STATUS_NONE.equals(
							    status.toLowerCase(Locale.ENGLISH)))
								attendee.setStatus(AttendeeStatus.NONE);
							else if (VALUE_ATTENDEE_STATUS_NEEDS_ACTION.equals(
							    status.toLowerCase(Locale.ENGLISH)))
								attendee.setStatus(AttendeeStatus.NEEDS_ACTION);
							else if (VALUE_ATTENDEE_STATUS_TENTATIVE.equals(
							    status.toLowerCase(Locale.ENGLISH)))
								attendee.setStatus(AttendeeStatus.TENTATIVE);
							else if (VALUE_ATTENDEE_STATUS_ACCEPTED.equals(
							    status.toLowerCase(Locale.ENGLISH)))
								attendee.setStatus(AttendeeStatus.ACCEPTED);
							else if (VALUE_ATTENDEE_STATUS_DECLINED.equals(
							    status.toLowerCase(Locale.ENGLISH)))
								attendee.setStatus(AttendeeStatus.DECLINED);
							else if (VALUE_ATTENDEE_STATUS_DELEGATED.equals(
							    status.toLowerCase(Locale.ENGLISH)))
								attendee.setStatus(AttendeeStatus.DELEGATED);
							else
								throw new KolabParseException(
								    "invalid attendee status value '" + status + "'");
						} else if (ELEMENT_ATTENDEE_REQUEST_RESPONSE.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							Boolean value = readBoolean(parser);
							attendee.setRequestResponse(value);
						} else if (ELEMENT_ATTENDEE_ROLE.equals(
						    name.toLowerCase(Locale.ENGLISH))) {
							String role = parser.getElementText();
							if (VALUE_ATTENDEE_ROLE_REQUIRED.equals(
							    role.toLowerCase(Locale.ENGLISH)))
								attendee.setRole(Role.REQUIRED);
							else if (VALUE_ATTENDEE_ROLE_REQ_PARTICIPANT.equals(
							    role.toLowerCase(Locale.ENGLISH)))
								attendee.setRole(Role.REQ_PARTICIPANT);
							else if (VALUE_ATTENDEE_ROLE_OPTIONAL.equals(
							    role.toLowerCase(Locale.ENGLISH)))
								attendee.setRole(Role.OPTIONAL);
							else if (VALUE_ATTENDEE_ROLE_RESOURCE.equals(
							    role.toLowerCase(Locale.ENGLISH)))
								attendee.setRole(Role.RESOURCE);
							else
								throw new KolabParseException(
								    "invalid attendee role value '" + role + "'");
						} else {
							logger.info("unhandled kolab data element: " +
							    ELEMENT_ATTENDEE + "/" + name); //TODO extract operation
							continue;
						}
						level--;
					}
				} else if (END_ELEMENT == eventType)
					level--;
			}
			incidence.getAttendee().add(attendee);
		} else
			return false;
		return true;
	}

	/**
	 * Checks if the current xml element is a general collaboration item field.
	 * In this case the element content is read and added to the given instance
	 * and the value <code>true</code> is returned. The parser is located on the
	 * ending element node.
	 * Otherwise nothing is done and the value <code>false</code> is returned.
	 */
	protected static boolean
	readElementCollaborationItem(CollaborationItem item, String name, XMLStreamReader parser)
	throws XMLStreamException, KolabParseException
	{
		if (ELEMENT_UID.equals(name.toLowerCase(Locale.ENGLISH))) {
			String uid = parser.getElementText();
			logger.info("read uid: " + uid);
			item.setUid(uid);
		} else if (ELEMENT_BODY.equals(name.toLowerCase(Locale.ENGLISH)))
			item.setBody(parser.getElementText());
		else if (ELEMENT_CATEGORIES.equals(name.toLowerCase(Locale.ENGLISH)))
			item.setCategories(parser.getElementText());
		else if (ELEMENT_CREATION_DATE.equals(name.toLowerCase(Locale.ENGLISH)))
			item.setCreationDate(readDateTime(parser));
		else if (ELEMENT_LAST_MODIFICATION_DATE.equals(name.toLowerCase(Locale.ENGLISH)))
			item.setLastModificationDate(readDateTime(parser));
		else if (ELEMENT_SENSITIVITY.equals(name.toLowerCase(Locale.ENGLISH))) {
			String sensitivity = parser.getElementText();
			if (VALUE_SENSITIVITY_PUBLIC.equals(sensitivity.toLowerCase(Locale.ENGLISH)))
				item.setSensitivity(Sensitivity.PUBLIC);
			else if (VALUE_SENSITIVITY_CONFIDENTIAL.equals(sensitivity.toLowerCase(Locale.ENGLISH)))
				item.setSensitivity(Sensitivity.CONFIDENTIAL);
			else if (VALUE_SENSITIVITY_PRIVATE.equals(sensitivity.toLowerCase(Locale.ENGLISH)))
				item.setSensitivity(Sensitivity.PRIVATE);
			else
				throw new KolabParseException("invalid sensitivity value '" + sensitivity + "'");
		} else if (ELEMENT_INLINE_ATTACHMENT.equals(name.toLowerCase(Locale.ENGLISH)))
			item.getInlineAttachment().add(parser.getElementText());
		else if (ELEMENT_LINK_ATTACHMENT.equals(name.toLowerCase(Locale.ENGLISH)))
			item.getLinkAttachment().add(parser.getElementText());
		else if (ELEMENT_PRODUCT_ID.equals(name.toLowerCase(Locale.ENGLISH)))
			item.setProductId(parser.getElementText());
		else
			return false;
		return true;
	}

	protected static Integer
	readInteger(String name, XMLStreamReader parser)
	throws KolabParseException, XMLStreamException
	{
		Integer alarm;
		try {
			alarm = new Integer(parser.getElementText());
		} catch (NumberFormatException e) {
			throw new KolabParseException("content of field " + name + " is not a valid number", e);
		}
		return alarm;
	}
}
