package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.Helper;
import org.evolvis.bsi.kolab.KolabConfigException;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Writing operations on the kolab items database table can be synchronized
 * with this singleton globally for the same imap folders.
 *
 * @author Hendrik Helwich
 * @author Sven Schumann
 */
public class DBFolderLock {
	private static final Logger logger = Logger.getLogger(DBFolderLock.class);
	private static final DBFolderLock INSTANCE = new DBFolderLock();

	private Map<String, Lock> lockMap = new HashMap<String, Lock>();

	public void
	doSynchronized(String folderName, FolderAction action)
	throws MessagingException, KolabConfigException
	{
		final Lock folderLock = getFolderLock(folderName);

		try {
			logger.trace("Acquiring lock for '" + folderName + "' .");
			try {
				folderLock.lockInterruptibly();
			} catch (InterruptedException e) {
				//thread will be interrupted... we have to terminate
				throw new RuntimeException(e);
			}

			if (Helper.isInTestMode()) {
				//in the tests we have to wait because the test database supports on optimistic locks
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
			logger.trace("Successfully acquired lock for '" + folderName + "'.");
			action.synchronizedAction();
		} finally {
			folderLock.unlock();
			logger.trace("Released lock for '" + folderName + "'.");
		}
	}

	private synchronized Lock
	getFolderLock(String folderName)
	{
		if (!lockMap.containsKey(folderName)) {
			lockMap.put(folderName, new ReentrantLock());
		}

		return lockMap.get(folderName);
	}

	private DBFolderLock()
	{
	}

	public static DBFolderLock
	getInstance()
	{
		return INSTANCE;
	}

	public interface FolderAction {
		public void synchronizedAction()
		throws MessagingException, KolabConfigException;
	}

	public interface FolderActionWithString extends FolderAction {
		public String getString();
	}
}
