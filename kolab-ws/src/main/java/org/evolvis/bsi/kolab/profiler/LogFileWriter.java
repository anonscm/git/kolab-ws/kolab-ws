package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.apache.log4j.Logger;
import org.evolvis.bsi.kolab.Helper;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class should only be used by the {@link Profiler} class.
 * A profiler XML log file writer.
 * An instance can split the log information in more than one file.
 *
 * @author Hendrik Helwich
 */
class LogFileWriter {
	private static final String LOG_VERSION_0_1 = "0.1";

	private static final String NS_PROFILER_LOG = "http://ws.kolab.evolvis.org/profiler-log";
	private static final String NS_XML_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";

	private static final String XSD_SCHEMA_LOCATION = "http://ws.kolab.evolvis.org/profiler-log profiler-log.xsd";

	private static final Logger logger = Logger.getLogger(LogFileWriter.class.getName());

	private static final String ZIP_FILE_SUFFIX = ".zip";

	private static final SimpleDateFormat TIME_FORMAT_LOG_ENTRY =
	    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");

	private static final SimpleDateFormat TIME_FORMAT_LOG_FILE =
	    new SimpleDateFormat("yyyy-MM-dd");

	private static final String LOG_FILE_SUFFIX = ".log";

	public LogFileWriter()
	{
	}

	private File logFile;

	/**
	 * log file writer to the file {@link #logFile}.
	 */
	private XMLStreamWriter logFileWriter;

	/**
	 * range of the log file writer {@link #logFileWriter}. A new log file
	 * should be started automatically after this time.
	 */
	private Date logFileMaxTime = new Date(0);

	/**
	 * Used to synchronize write operations from different threads (CPU monitor,
	 * memory monitor, main thread) to the XML log file.
	 */
	private final ReentrantLock lock = new ReentrantLock();

	/**
	 * Zip the current unused and existing (this must be assured by this class)
	 * log file {@link #logFile} and add the suffix ".zip" to it.
	 * The uncompressed log file is deleted afterwards.
	 */
	private void
	zipLogFile() throws IOException
	{
		// create the ZIP file output stream
		String outFilename = logFile.getAbsolutePath() + ZIP_FILE_SUFFIX;
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFilename));

		// create current log file input stream
		FileInputStream in = new FileInputStream(logFile);

		// add log file ZIP entry to the output stream.
		out.putNextEntry(new ZipEntry(logFile.getName()));

		// copy bytes from the log file to the ZIP file
		Helper.copyStream(in, out);

		// complete the entry
		out.closeEntry();
		in.close();

		// complete the ZIP file
		out.close();

		// remove uncompressed log file
		if (!logFile.delete())
			logger.warn("unable to delete log file after zipping " + logFile.getAbsolutePath());

		logFile = null;
	}

	/**
	 * Closes the actual log file and zip it. Add the extension ".zip".
	 */
	public void
	close() throws XMLStreamException, IOException
	{
		if (logFileWriter != null) {
			logFileWriter.writeEndElement();
			logFileWriter.writeEndDocument();
			logFileWriter.close();
			logFileWriter = null;
			zipLogFile();
		}
	}

	/**
	 * Check if the given time of the next log entry is after the range of the
	 * current active log file {@link #logFileWriter}.
	 * In this case, the current log file is closed and zipped and a new log
	 * file is created to which further log entries are saved.
	 */
	private synchronized void
	createLogFileIfNecessary(Date logTime) throws XMLStreamException, IOException
	{
		if (logTime.after(logFileMaxTime)) { // finish old log file and create new log file

			close();

			// get log dir and create it if necessary
			File logDir = new File(System.getProperty("jboss.server.log.dir"));

			// set range of the new log file (ends at the start of the next day)
			Calendar cal = Calendar.getInstance();
			cal.setTime(logTime);
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);
			cal.add(Calendar.DATE, 1);
			logFileMaxTime = cal.getTime();

			// create log file (add unused number if file exists)
			String fprefix = "kolab-ws-profiler-" + TIME_FORMAT_LOG_FILE.format(logTime);
			File f, f2;
			int i = 0;
			do {
				String nr = i == 0 ? "" : "_(" + i + ")";
				f = new File(logDir, fprefix + nr + LOG_FILE_SUFFIX);
				f2 = new File(logDir, fprefix + nr + LOG_FILE_SUFFIX + ZIP_FILE_SUFFIX);
				i++;
			} while (f.exists() || f2.exists());
			logFile = f;

			// create XML log file writer
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			logFileWriter = factory.createXMLStreamWriter(new FileOutputStream(f));
			logFileWriter.writeStartDocument();
			logFileWriter.writeStartElement("kolab-ws-profiler");
			logFileWriter.writeAttribute("version", LOG_VERSION_0_1);
			logFileWriter.writeNamespace("", NS_PROFILER_LOG);
			logFileWriter.writeNamespace("xsi", NS_XML_SCHEMA_INSTANCE);
			logFileWriter.writeAttribute(NS_XML_SCHEMA_INSTANCE, "schemaLocation",
			    XSD_SCHEMA_LOCATION);
		}
	}

	/**
	 * Create an xml element with the given name in the log file and add the
	 * time (and possibly duration) values as xml attributes.
	 * If necessary, a new log file will be created.
	 *
	 * @param element The name of the xml element which should be created
	 * @param start   The start time at which the logged record has happened
	 * @param length  this value is only written if it is greater or equal to zero.
	 */
	private void
	initLogEntry(String element, long start, long length)
	throws XMLStreamException, IOException
	{
		Date now = new Date(start);
		createLogFileIfNecessary(now);
		String time = TIME_FORMAT_LOG_ENTRY.format(start);
		logFileWriter.writeStartElement(element);
		logFileWriter.writeAttribute("time", time);
		if (length >= 0)
			logFileWriter.writeAttribute("length", Long.toString(length));
	}

	/**
	 * Log the given memory usage to this log file.
	 *
	 * @param time      The time at which the CPU load was measured
	 * @param used      bytes of memory used by the JVM
	 * @param committed bytes of memory allocated by the JVM
	 * @param max       maximum bytes of memory which can be allocated by the JVM
	 */
	public void
	logMemoryUsage(long time, long used, long committed, long max)
	{
		lock.lock();
		try {
			initLogEntry("memory", time, -1);
			logFileWriter.writeAttribute("used", Long.toString(used));
			logFileWriter.writeAttribute("committed", Long.toString(committed));
			logFileWriter.writeAttribute("max", Long.toString(max));
			logFileWriter.writeEndElement();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Log the CPU load to this log file.
	 *
	 * @param time The time at which the CPU load was measured
	 * @param load The CPU load.
	 */
	public void
	logCpuLoad(long time, float load)
	{
		lock.lock();
		try {
			initLogEntry("cpu", time, -1);
			logFileWriter.writeCharacters(Float.toString(load));
			logFileWriter.writeEndElement();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Log a method call to the log file.
	 *
	 * @param startTime The time measured before the operation was called
	 * @param duration  The time the operation was active.
	 * @param stack     The operation stack. It can hold <code>null</code> values which
	 *                  should not be logged.
	 */
	public void logOperation(long startTime, long duration, StackTraceElement... stack)
	{
		lock.lock();
		try {
			initLogEntry("operation", startTime, duration);
			for (StackTraceElement st : stack) {
				if (st == null)
					continue;
				logFileWriter.writeStartElement("stack");
				logFileWriter.writeAttribute("class", st.getClassName());
				logFileWriter.writeAttribute("method", st.getMethodName());
				if (st.getFileName() != null) {
					logFileWriter.writeAttribute("file", st.getFileName());
					logFileWriter.writeAttribute("line", Integer.toString(st.getLineNumber()));
				}
				logFileWriter.writeEndElement();
			}
			logFileWriter.writeEndElement();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}
}
