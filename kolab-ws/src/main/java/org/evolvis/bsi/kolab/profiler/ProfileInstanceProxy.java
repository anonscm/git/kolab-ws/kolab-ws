package org.evolvis.bsi.kolab.profiler;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * This class should only be used by the {@link Profiler} class.
 * Use this class to wrap an instance of an interface and log informations of
 * all instance accesses.
 *
 * @author Hendrik Helwich
 */
class ProfileInstanceProxy implements InvocationHandler {
	private Object obj;

	private ProfileInstanceProxy(Object obj)
	{
		this.obj = obj;
	}

	@Override
	public Object
	invoke(Object proxy, Method method, Object[] args)
	throws Throwable
	{
		return Profiler.invoke(obj, method, args);
	}

	/**
	 * Create a proxy of an interface instance to profile method accesses.
	 *
	 * @param obj The interface instance which should be wrapped.
	 *
	 * @return The return value can be casted to the interface which the given
	 *     parameter is implementing.
	 */
	public static Object
	newInstance(Object obj)
	{
		return java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(),
		    obj.getClass().getInterfaces(), new ProfileInstanceProxy(obj));
	}
}
