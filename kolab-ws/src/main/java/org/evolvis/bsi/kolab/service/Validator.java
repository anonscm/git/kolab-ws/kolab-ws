package org.evolvis.bsi.kolab.service;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Check constraints which are not set in the Xml schema.
 *
 * @author Hendrik Helwich
 */
public class Validator {
	private static final void
	validate(CollaborationItem citem)
	{
		Date cd = citem.getCreationDate();
		if (cd == null)
			;//throw new IllegalArgumentException("creation date is null");
		else if (cd.getTime() % 1000 != 0)
			throw new IllegalArgumentException("milliseconds of creation date must be zero");
		Date lmd = citem.getLastModificationDate();
		if (lmd == null)
			;//throw new IllegalArgumentException("last modification date is null");
		else if (lmd.getTime() % 1000 != 0)
			throw new IllegalArgumentException("milliseconds of last modification date must be zero");
	}

	private static final void
	validate(Incidence incidence)
	{
		validate((CollaborationItem)incidence);
		Boolean allday = incidence.isAllDay();
		Date startDate = incidence.getStartDate();
		if (startDate != null) {
			if (allday == null)
				throw new IllegalArgumentException(
				    "all day property must be specified if start date is available");
			long mult = allday ? 86400000L : 1000L;
			if (startDate.getTime() % mult != 0)
				throw new IllegalArgumentException(allday ?
				    "time must not be present in start date on all day incidences" :
				    "milliseconds must not be present in start date");
		} else if (allday != null)
			throw new IllegalArgumentException(
			    "start date must not be null if all day property is not null");
		Recurrence recurrence = incidence.getRecurrence();
		if (recurrence != null) {
			if (recurrence.getCycle() == null)
				throw new IllegalArgumentException("cycle value must not be null");
			if (recurrence.getInterval() < 1)
				throw new IllegalArgumentException(
				    "interval value must be set to a value greater or equal to 1");
			int set = 0;
			set |= recurrence.getMonth() == null ? 0 : 1;
			set |= recurrence.getWeek() == null ? 0 : 2;
			set |= recurrence.getYearDay() == null ? 0 : 4;
			set |= recurrence.getMonthDay() == null ? 0 : 8;
			set |= recurrence.getWeekDays() == null ? 0 : 16;
			if (set != 4 && set != 9 && set != 19
			    && set != 8 && set != 18 && set != 16 && set != 0)
				throw new IllegalArgumentException("invalid combination of recurrence fields");
			if (
			    (recurrence.getRangeDate() != null && recurrence.getRangeNumber() != null) ||
				(recurrence.getRangeDate() == null && recurrence.getRangeNumber() == null)
			)
				throw new IllegalArgumentException("exact one range type must be set");
			if (recurrence.getRangeNumber() != null && recurrence.getRangeNumber() < 0)
				throw new IllegalArgumentException("range number must not be negative");
			if (recurrence.getMonth() != null && recurrence.getMonth() < 1 && recurrence.getMonth() > 12)
				throw new IllegalArgumentException("month value must be in [1 .. 12]");
			if (recurrence.getWeek() != null && recurrence.getWeek() < 1 && recurrence.getWeek() > 5)
				throw new IllegalArgumentException("week value must be in [1 .. 5]");
			if (recurrence.getYearDay() != null && recurrence.getYearDay() < 1 &&
			    recurrence.getYearDay() > 366)
				throw new IllegalArgumentException("year day value must be in [1 .. 366]");
			if (recurrence.getMonthDay() != null && recurrence.getMonthDay() < 1 &&
			    recurrence.getMonthDay() > 31)
				throw new IllegalArgumentException("year day value must be in [1 .. 31]");
			if (recurrence.getWeekDays() != null && recurrence.getWeekDays() < 1 &&
			    recurrence.getWeekDays() > 127)
				throw new IllegalArgumentException("year day value must be in [1 .. 127]");
		}
		for (Attendee attendee : incidence.getAttendee()) {
			if (attendee.getStatus() == null)
				throw new IllegalArgumentException("attendee status is null");
			if (attendee.getRole() == null)
				throw new IllegalArgumentException("attendee role is null");
		}
	}

	private static final Pattern PATTERN_COLOR = Pattern.compile("#[0-9a-fA-F]{6}");

	public static final void
	validate(Note note)
	{
		validate((CollaborationItem)note);
		String color = note.getBackgroundColor();
		if (color != null && !PATTERN_COLOR.matcher(color).matches())
			throw new IllegalArgumentException("illegal color value '" + color + "'");
		color = note.getForegroundColor();
		if (color != null && !PATTERN_COLOR.matcher(color).matches())
			throw new IllegalArgumentException("illegal color value '" + color + "'");
	}

	public static final void
	validate(Contact contact)
	{
		validate((CollaborationItem)contact);
		//		Set<PhoneType> ptypes = new TreeSet<PhoneType>();
		//		for (Phone phone : contact.getPhone()) {
		//			PhoneType ptype = phone.getType();
		//			if (ptype == null)
		//				;//throw new IllegalArgumentException("phone type is null");
		//			else {
		//				if (ptypes.contains(ptype))
		//					throw new IllegalArgumentException("phone type "+ptype+" occurs more than one time");
		//				ptypes.add(ptype);
		//			}
		//		}
		for (Address address : contact.getAddress()) {
			if (address.getType() == null)
				throw new IllegalArgumentException("address type is null");
		}
		//		if (contact.getPreferredAddress() == null)
		//			throw new IllegalArgumentException("preferred address type is null");
	}

	public static final void
	validate(Event event)
	{
		validate((Incidence)event);
		if (event.getStartDate() == null)
			throw new IllegalArgumentException("start date must not be null for an event");
		if (event.isAllDay() == null)
			throw new IllegalArgumentException("all day property must not be null for an event");
	}

	public static final void
	validate(Task task)
	{
		validate((Incidence)task);
		Integer priority = task.getPriority();
		if (priority != null && (priority < 1 || priority > 5))
			throw new IllegalArgumentException("priority must be between 1 and 5 but is " + priority);
		Integer completed = task.getCompleted();
		if (completed != null && (completed < 0 || completed > 100))
			throw new IllegalArgumentException("completed must be between 0 and 100 but is " + completed);
		Date dueDate = task.getDueDate();
		if (dueDate != null && dueDate.getTime() % 1000 != 0)
			throw new IllegalArgumentException("milliseconds of due date must be zero");
	}
}
