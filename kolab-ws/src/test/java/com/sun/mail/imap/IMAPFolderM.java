package com.sun.mail.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.protocol.IMAPProtocol;
import org.evolvis.bsi.kolab.util.FolderType;

import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Quota;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.event.ConnectionListener;
import javax.mail.event.FolderListener;
import javax.mail.event.MessageChangedListener;
import javax.mail.event.MessageCountListener;
import javax.mail.search.SearchTerm;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Hendrik Helwich
 */
public class IMAPFolderM extends IMAPFolder {
	private String fullName;
	protected final List<IMAPMessageM> messages;
	private final FolderType folderType;

	public IMAPFolderM(FolderType folderType, String fullName, List<IMAPMessageM> messages)
	{
		super(fullName, UNKNOWN_SEPARATOR, new IMAPStoreM(), true);
		this.fullName = fullName;
		this.messages = messages;
		this.folderType = folderType;
	}

	public IMAPFolderM(FolderType folderType, File store, String fullName)
	{
		this(folderType, fullName, readMessages(store, fullName));
	}

	public IMAPFolderM(FolderType folderType, String fullName)
	{
		this(folderType, IMAPStoreM.STORE, fullName);
	}

	private static List<IMAPMessageM> readMessages(File store, String fullName)
	{
		final String suffix = ".xml";
		if (!store.isDirectory())
			throw new IllegalArgumentException("not a directory: " + store.getAbsoluteFile());
		File folder = new File(store, fullName);
		if (!folder.isDirectory())
			throw new IllegalArgumentException("not a directory: " + folder.getAbsoluteFile());
		// create message array
		List<IMAPMessageM> msgList = new LinkedList<IMAPMessageM>();
		for (File file : folder.listFiles()) {
			if (file.getName().endsWith(suffix)) {
				String uidstr = file.getName().substring(0, file.getName().length() - suffix.length());
				boolean markedDeleted = false;
				if (uidstr.charAt(0) == 'd') { // marked deleted
					uidstr = uidstr.substring(1);
					markedDeleted = true;
				}
				long uid = Long.parseLong(uidstr);
				msgList.add(new IMAPMessageM(uid, file, markedDeleted));
			}
		}
		// sort messages to have increasing uid
		Collections.sort(msgList, new Comparator<IMAPMessageM>() {
			@Override
			public int compare(IMAPMessageM m1, IMAPMessageM m2)
			{
				return new Long(m1.getUid()).compareTo(m2.getUid());
			}
		});
		return msgList;
	}

	public void setFullName(String fullName)
	{
		this.fullName = fullName;
	}

	@Override
	public synchronized String getFullName()
	{
		return fullName;
	}

	@Override
	public synchronized boolean isOpen()
	{
		return true;
	}

	@Override
	public synchronized void open(int arg0) throws MessagingException
	{
		// do nothing
	}

	@Override
	public synchronized Message[] getMessages() throws MessagingException
	{
		return messages.toArray(new IMAPMessageM[messages.size()]);
	}

	@Override
	public synchronized long getUID(Message msg) throws MessagingException
	{
		IMAPMessageM message = (IMAPMessageM)msg;
		return message.getUid();
	}

	@Override
	public synchronized IMAPMessageM getMessageByUID(long uid)
	throws MessagingException
	{
		try {
			for (IMAPMessageM message : messages)
				if (message.getUid() == uid)
					return message;
		} catch (ConcurrentModificationException e) { // another thread has modified the message list => try again
			return getMessageByUID(uid);
		}
		return null; // as described in the java mail javadocs
	}

	@Override
	public synchronized int getMessageCount() throws MessagingException
	{
		return messages.size();
	}

	@Override
	public String toString()
	{
		return fullName;
	}

	public void removeMessage(long uid) throws Exception
	{
		for (IMAPMessageM message : messages)
			if (message.getUid() == uid) {
				if (!messages.remove(message))
					throw new Exception("unknown uid " + uid);
				break;
			}
	}

	@Override
	public Store getStore()
	{
		return new IMAPStoreM();
	}

	/* This is tricky:
	 * To avoid complexity in generalizing the imap api for test purposes
	 * this in testing environment unused operation can be used there to return
	 * any object.
	 * The reuse of an existing function is due to the testing classes are
	 * normally not available at compile time.
	 * The first argument can be used to indicate different return values. The
	 * second argument is unused.
	 */
	@Override
	public Object doOptionalCommand(String opcode, ProtocolCommand mustBeNull)
	throws MessagingException
	{
		if (mustBeNull == null) {
			if ("folderType".equals(opcode))
				return folderType;
			if ("syncFolder".equals(opcode))
				return Boolean.TRUE;
		}
		throw new UnsupportedOperationException();
	}

	// unused operations => disable

	@Override
	public void addACL(ACL acl) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void addMessageCountListener(MessageCountListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] addMessages(Message[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void addRights(ACL acl) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void appendMessages(Message[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized AppendUID[] appendUIDMessages(Message[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void close(boolean expunge) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void copyMessages(Message[] arg0, Folder arg1)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized boolean create(int type) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized boolean delete(boolean arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object doCommand(ProtocolCommand arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object doCommandIgnoreFailure(ProtocolCommand arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected Object doProtocolCommand(ProtocolCommand cmd)
	throws ProtocolException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized boolean exists() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] expunge() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] expunge(Message[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void fetch(Message[] msgs, FetchProfile fp)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void forceClose() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public ACL[] getACL() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized String[] getAttributes() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized int getDeletedMessageCount() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Folder getFolder(String name) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message getMessage(int msgnum)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] getMessagesByUID(long arg0, long arg1)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] getMessagesByUID(long[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized String getName()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized int getNewMessageCount() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Folder getParent() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Flags getPermanentFlags()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Quota[] getQuota() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized char getSeparator() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected synchronized IMAPProtocol getStoreProtocol()
	throws ProtocolException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized int getType() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized long getUIDNext() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized long getUIDValidity() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized int getUnreadMessageCount() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void handleResponse(Response arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized boolean hasNewMessages() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void idle() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized boolean isSubscribed()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Folder[] list(String pattern) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Rights[] listRights(String name) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Folder[] listSubscribed(String pattern) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Rights myRights() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected synchronized void releaseStoreProtocol(IMAPProtocol p)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeACL(String name) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeRights(ACL acl) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized boolean renameTo(Folder f) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] search(SearchTerm arg0, Message[] arg1)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] search(SearchTerm arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void setFlags(Message[] arg0, Flags arg1, boolean arg2)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setQuota(Quota quota) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void setSubscribed(boolean subscribe)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void addConnectionListener(ConnectionListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void addFolderListener(FolderListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void addMessageChangedListener(MessageChangedListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void finalize() throws Throwable
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] getMessages(int[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized Message[] getMessages(int arg0, int arg1)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getMode()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public URLName getURLName() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Folder[] list() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Folder[] listSubscribed() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void notifyConnectionListeners(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void notifyFolderListeners(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void notifyFolderRenamedListeners(Folder arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void notifyMessageAddedListeners(Message[] msgs)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void notifyMessageChangedListeners(int type, Message msg)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void notifyMessageRemovedListeners(boolean removed, Message[] msgs)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void removeConnectionListener(ConnectionListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void removeFolderListener(FolderListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void removeMessageChangedListener(
	    MessageChangedListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void removeMessageCountListener(MessageCountListener l)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void setFlags(int[] arg0, Flags arg1, boolean arg2)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public synchronized void setFlags(int arg0, int arg1, Flags arg2,
	    boolean arg3) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}
}
