package com.sun.mail.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.protocol.IMAPProtocol;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Hendrik Helwich
 */
public class IMAPStoreM extends IMAPStore {
	public static final File STORE = new File(IMAPStore.class.getResource("/imapstore/").getFile());

	public IMAPStoreM()
	{
		super(Session.getInstance(getProperties()), null);
	}

	static Properties getProperties()
	{
		return new Properties();
	}

	/**
	 * This operation will always return <code>null</code> to indicate that this
	 * is a mock up for the class {@link IMAPStore}.
	 * This can be done because the real implementation does always return a
	 * value which is not <code>null</code>.
	 */
	@Override
	public IMAPProtocol getProtocol(IMAPFolder folder) throws MessagingException
	{
		return null;
	}

	private Map<String, IMAPFolderM> folders;

	public void addFolder(IMAPFolderM folder)
	{
		if (folders == null)
			folders = new HashMap<String, IMAPFolderM>();
		folders.put(folder.getFullName(), folder);
	}

	@Override
	public synchronized Folder getFolder(String name) throws MessagingException
	{
		return folders == null ? null : folders.get(name);
	}
}
