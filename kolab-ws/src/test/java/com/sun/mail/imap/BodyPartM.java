package com.sun.mail.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

/**
 * @author Hendrik Helwich
 */
public class BodyPartM extends javax.mail.BodyPart {
	private final File content;

	public BodyPartM(File content)
	{
		this.content = content;
	}

	@Override
	public InputStream getInputStream() throws IOException, MessagingException
	{
		return new FileInputStream(content);
	}

	@Override
	public String getContentType() throws MessagingException
	{
		return "application/x-vnd.kolab.bla";
	}

	// unused operations => disable

	@Override
	public void addHeader(String arg0, String arg1) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Enumeration<?> getAllHeaders() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object getContent() throws IOException, MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public DataHandler getDataHandler() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getDescription() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getDisposition() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getFileName() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String[] getHeader(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getLineCount() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Enumeration<?> getMatchingHeaders(String[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Enumeration<?> getNonMatchingHeaders(String[] arg0)
	throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getSize() throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isMimeType(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeHeader(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setContent(Multipart arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setContent(Object arg0, String arg1) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDataHandler(DataHandler arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDescription(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setDisposition(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setFileName(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setHeader(String arg0, String arg1) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setText(String arg0) throws MessagingException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void writeTo(OutputStream arg0) throws IOException,
	    MessagingException
	{
		throw new UnsupportedOperationException();
	}
}
