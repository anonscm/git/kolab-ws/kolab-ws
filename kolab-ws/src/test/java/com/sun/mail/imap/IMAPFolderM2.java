package com.sun.mail.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.util.FolderType;

import javax.mail.MessagingException;

import static org.junit.Assert.assertEquals;

/**
 * Special folder implementation to check if the access is synchronized or not.
 */
public class IMAPFolderM2 extends IMAPFolderM {
	private final boolean appendThreadId;

	private Long threadId = null;
	public AssertionError assertionError;

	public IMAPFolderM2(FolderType folderType, String fullName, boolean appendThreadId)
	{
		super(folderType, fullName);
		this.appendThreadId = appendThreadId;
	}

	@Override
	public synchronized String getFullName()
	{
		return appendThreadId ?
		    super.getFullName() + Thread.currentThread().getId()
		    : super.getFullName();
	}

	@Override
	public synchronized IMAPMessageM getMessageByUID(long uid)
	throws MessagingException
	{
		// it is checked that the message queue is always been checked by one
		// thread only
		IMAPMessageM msg = (IMAPMessageM)super.getMessageByUID(uid);
		Long id = Thread.currentThread().getId();
		try {
			if (threadId == null ||
			    threadId != id) { // first call of this operation or a differnet thread is calling
				assertEquals(msg, messages.get(0)); // must be the first message
				threadId = id;
			}
		} catch (AssertionError e) {
			this.assertionError = e;
		}
		return msg;
	}
}
