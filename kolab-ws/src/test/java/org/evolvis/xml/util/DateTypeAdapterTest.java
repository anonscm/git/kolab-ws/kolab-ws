package org.evolvis.xml.util;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.evolvis.xml.util.DateTypeAdapter.parseDate;
import static org.evolvis.xml.util.DateTypeAdapter.parseDateTime;
import static org.evolvis.xml.util.DateTypeAdapter.printDate;
import static org.evolvis.xml.util.DateTypeAdapter.printDateTime;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DateTypeAdapterTest {
	@Before
	public void setUp() throws Exception
	{
		//		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kuala_Lumpur"));
	}

	@After
	public void tearDown() throws Exception
	{
	}

	private static final Date _24_09_02 = new Date(1032825600000L);
	private static final Date _24_09_02_9 = new Date(_24_09_02.getTime() + 9 * 60 * 60 * 1000);

	@Test
	public void testParseDate()
	{
		assertNull(parseDate(null));
		assertEquals(_24_09_02, parseDate("2002-09-24"));
		assertEquals(_24_09_02, parseDate("2002-09-24Z"));
		assertEquals(_24_09_02, parseDate("2002-09-24-06:00"));
		assertEquals(_24_09_02, parseDate("2002-09-24+06:00"));
	}

	@Test
	public void testPrintDate()
	{
		assertNull(printDate(null));
		assertEquals("2002-09-24Z", printDate(_24_09_02));
	}

	@Test(expected = RuntimeException.class)
	public void testPrintDateWithTime()
	{
		printDate(new Date(_24_09_02.getTime() + 1));
	}

	@Test
	public void testParseDateTime()
	{
		assertNull(parseDateTime(null));
		assertEquals(_24_09_02_9, parseDateTime("2002-09-24T09:00:00"));
		assertEquals(_24_09_02_9, parseDateTime("2002-09-24T09:00:00Z"));
		assertEquals(_24_09_02_9, parseDateTime("2002-09-24T12:00:00+03:00"));
		assertEquals(_24_09_02_9, parseDateTime("2002-09-24T08:00:00-01:00"));
	}

	@Test
	public void testPrintDateTime()
	{
		assertNull(printDateTime(null));
		assertEquals("2002-09-24T00:00:00Z", printDateTime(_24_09_02));
		assertEquals("2002-09-24T09:00:00Z", printDateTime(_24_09_02_9));
	}
}
