package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;

/**
 * @author Hendrik Helwich
 */
public class NoteSerializationTest {
	@Test
	public void testSerialization() throws XMLStreamException, KolabParseException
	{
		Note note;
		note = new Note();
		note.setCreationDate(ContactSerializationTest.createDate());
		note.setLastModificationDate(ContactSerializationTest.createDate());
		note.setSensitivity(Sensitivity.PUBLIC);
		checkSerialization(note);
		note = createFullNote();
		checkSerialization(note);
	}

	private static void checkSerialization(Note note) throws XMLStreamException, KolabParseException
	{
		ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(note, xmlStream);
		byte[] xml = xmlStream.toByteArray();
		Note note2 = KolabSerializer.readNote(new ByteArrayInputStream(xml));
		assertEquals_(note, note2);
		// convert again to xml string and compare to get an error if a field is forgotten
		xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(note2, xmlStream);
		byte[] xml2 = xmlStream.toByteArray();
		assertEquals(new String(xml), new String(xml2));
	}

	private static Note createFullNote()
	{
		Note note = new Note();
		ContactSerializationTest.fillCommonFields(note);
		note.setSummary("sumr1");
		note.setBackgroundColor("#00ff11");
		note.setForegroundColor("#334432");
		return note;
	}

	private static void assertEquals_(Note n1, Note n2)
	{
		if (n1 == null && n2 == null)
			return;
		if (n1 == null || n2 == null)
			throw new AssertionError("one note is null");
		ContactSerializationTest.assertEquals_((CollaborationItem)n1, (CollaborationItem)n2);
		assertEquals(n1.getSummary(), n2.getSummary());
		assertEquals(n1.getBackgroundColor(), n2.getBackgroundColor());
		assertEquals(n1.getForegroundColor(), n2.getForegroundColor());
	}
}
