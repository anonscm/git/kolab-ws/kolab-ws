package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.Attendee;
import org.evolvis.bsi.kolab.service.AttendeeStatus;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.ColorLabel;
import org.evolvis.bsi.kolab.service.Cycle;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Incidence;
import org.evolvis.bsi.kolab.service.Recurrence;
import org.evolvis.bsi.kolab.service.Role;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.evolvis.bsi.kolab.service.ShowTimeAs;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Hendrik Helwich
 */
public class EventSerializationTest {
	@Test
	public void testSerialization() throws XMLStreamException, KolabParseException
	{
		Event event;
		event = new Event();
		event.setCreationDate(ContactSerializationTest.createDate());
		event.setLastModificationDate(ContactSerializationTest.createDate());
		event.setStartDate(ContactSerializationTest.createDate());
		event.setAllDay(false);
		event.setSensitivity(Sensitivity.PUBLIC);
		checkSerialization(event);
		for (int i = 0; i < 84; i++) {
			event = createFullEvent(i);
			checkSerialization(event);
		}
	}

	private static void checkSerialization(Event event) throws XMLStreamException, KolabParseException
	{
		ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(event, xmlStream);
		byte[] xml = xmlStream.toByteArray();
		Event event2 = KolabSerializer.readEvent(new ByteArrayInputStream(xml));
		assertEquals_(event, event2);
		// convert again to xml string and compare to get an error if a field is forgotten
		xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(event2, xmlStream);
		byte[] xml2 = xmlStream.toByteArray();
		assertEquals(new String(xml), new String(xml2));
	}

	private static Event createFullEvent(int type)
	{
		Event event = new Event();
		ContactSerializationTest.fillCommonFields(event);
		fillIncidenceFields(event, type);
		event.setShowTimeAs(ShowTimeAs.OUTOFOFFICE);
		event.setColorLabel(ColorLabel.NEEDS_PREPARATION);
		boolean allDay = type < 42;
		event.setEndDate(new Date((allDay ? 86400000L : 1000L) * 321));
		return event;
	}

	static void fillIncidenceFields(Incidence incidence, int type /* 0 - 83  (84=2*7*3*2) */)
	{
		incidence.setSummary("smry1");
		incidence.setLocation("loca1");
		incidence.setOrganizerDisplayName("orgdn1");
		incidence.setOrganizerSmtpAddress("orgsmtp1");
		boolean allDay = type < 42;
		type %= 42;
		incidence.setStartDate(new Date((allDay ? 86400000L : 1000L) * 123));
		incidence.setAllDay(allDay);
		incidence.setAlarm(41);
		Recurrence recurrence = new Recurrence();
		switch (type / 6) {
		case 0:
			recurrence.setCycle(Cycle.DAILY);
			recurrence.setInterval(111);
			break;
		case 1:
			recurrence.setCycle(Cycle.WEEKLY);
			recurrence.setInterval(111);
			recurrence.setWeekDays(42);
			break;
		case 2:
			recurrence.setCycle(Cycle.MONTHLY);
			recurrence.setInterval(111);
			recurrence.setWeekDays(42);
			recurrence.setWeek(12);
			break;
		case 3:
			recurrence.setCycle(Cycle.MONTHLY);
			recurrence.setInterval(111);
			recurrence.setMonthDay(31);
			break;
		case 4:
			recurrence.setCycle(Cycle.YEARLY);
			recurrence.setInterval(111);
			recurrence.setWeekDays(42);
			recurrence.setWeek(12);
			recurrence.setMonth(11);
			break;
		case 5:
			recurrence.setCycle(Cycle.YEARLY);
			recurrence.setInterval(111);
			recurrence.setMonthDay(31);
			recurrence.setMonth(11);
			break;
		case 6:
			recurrence.setCycle(Cycle.YEARLY);
			recurrence.setInterval(111);
			recurrence.setYearDay(210);
		}
		type %= 6;
		switch (type / 2) {
		case 0:
			recurrence.setRangeNumber(0);
			break;
		case 1:
			recurrence.setRangeNumber(66);
			break;
		case 2:
			recurrence.setRangeDate(new Date(86400000L * 555));
			break;
		}
		type %= 2;
		switch (type) {
		case 0:
			recurrence.getExclusion().add(new Date(86400000L * 444));
			recurrence.getExclusion().add(new Date(86400000L * 333));
			recurrence.getExclusion().add(new Date(86400000L * 222));
			break;
		case 1:
			// no exclusion
			break;
		}
		incidence.setRecurrence(recurrence);
		Attendee attendee = new Attendee();
		attendee.setDisplayName("adpn1");
		attendee.setSmtpAddress("asmtpa1");
		attendee.setStatus(AttendeeStatus.TENTATIVE);
		attendee.setRequestResponse(false);
		attendee.setRole(Role.RESOURCE);
		incidence.getAttendee().add(attendee);
		attendee = new Attendee();
		attendee.setDisplayName("adpn2");
		attendee.setSmtpAddress("asmtpa2");
		attendee.setStatus(AttendeeStatus.DECLINED);
		attendee.setRequestResponse(true);
		attendee.setRole(Role.OPTIONAL);
		incidence.getAttendee().add(attendee);
	}

	private static void assertEquals_(Event e1, Event e2)
	{
		if (e1 == null && e2 == null)
			return;
		if (e1 == null || e2 == null)
			throw new AssertionError("one event is null");
		ContactSerializationTest.assertEquals_((CollaborationItem)e1, (CollaborationItem)e2);
		// check incidence fields
		assertEquals(e1.getSummary(), e2.getSummary());
		assertEquals(e1.getLocation(), e2.getLocation());
		assertEquals(e1.getOrganizerDisplayName(), e2.getOrganizerDisplayName());
		assertEquals(e1.getOrganizerSmtpAddress(), e2.getOrganizerSmtpAddress());
		assertEquals(e1.getStartDate(), e2.getStartDate());
		assertEquals(e1.getAlarm(), e2.getAlarm());
		Recurrence r1 = e1.getRecurrence();
		Recurrence r2 = e2.getRecurrence();
		if (r1 != null || r2 != null) {
			assertNotNull(r1);
			assertNotNull(r2);
			assertEquals(r1.getCycle(), r2.getCycle());
			assertEquals(r1.getInterval(), r2.getInterval());
			assertEquals(r1.getMonth(), r2.getMonth());
			assertEquals(r1.getWeek(), r2.getWeek());
			assertEquals(r1.getYearDay(), r2.getYearDay());
			assertEquals(r1.getMonthDay(), r2.getMonthDay());
			assertEquals(r1.getWeekDays(), r2.getWeekDays());
			assertEquals(r1.getRangeNumber(), r2.getRangeNumber());
			assertEquals(r1.getRangeDate(), r2.getRangeDate());
			assertEquals(r1.getExclusion(), r2.getExclusion());
		}
		List<Attendee> al1 = e1.getAttendee();
		List<Attendee> al2 = e2.getAttendee();
		assertEquals(al1.size(), al2.size());
		for (int i = 0; i < al1.size(); i++) {
			Attendee a1 = al1.get(i);
			Attendee a2 = al2.get(i);
			assertEquals(a1.getDisplayName(), a2.getDisplayName());
			assertEquals(a1.getSmtpAddress(), a2.getSmtpAddress());
			assertEquals(a1.getStatus(), a2.getStatus());
			assertEquals(a1.isRequestResponse(), a2.isRequestResponse());
			assertEquals(a1.getRole(), a2.getRole());
		}
		// check event fields
		assertEquals(e1.getShowTimeAs(), e2.getShowTimeAs());
		assertEquals(e1.getColorLabel(), e2.getColorLabel());
		assertEquals(e1.getEndDate(), e2.getEndDate());
	}
}
