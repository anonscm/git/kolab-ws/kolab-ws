package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * @author Hendrik Helwich
 */
public class DBTestUtil {
	private static final String QUERY_SELECT_ITEM_BY_MAIL_ID =
	    "SELECT DISTINCT i FROM ImapFolder f JOIN f.items i WHERE f.name=:folderName AND i.mailId=:mailId";
	private static final String QUERY_SELECT_ITEMS =
	    "SELECT DISTINCT i FROM ImapFolder f JOIN f.items i WHERE f.name=:folderName";

	public static void clearKolabItemTable(EntityManager em)
	{
		Query query = em.createNativeQuery("DELETE FROM " + KolabItem.TABLE_NAME);
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		query.executeUpdate();
		transaction.commit();
	}

	public static void clearKolabItemTableByFolderName(EntityManager em, String folderName)
	{
		if (folderName == null) {
			throw new NullPointerException();
		}
		Query query = em.createNativeQuery("DELETE FROM " + KolabItem.TABLE_NAME + " WHERE " +
		    KolabItem.TABLE_NAME + "." + KolabItem.CNAME_PK_ITEM + " IN (SELECT " +
		    KolabItem.TABLE_NAME + "." + KolabItem.CNAME_PK_ITEM + " FROM " +
		    KolabItem.TABLE_NAME + " JOIN " + ImapFolder.TABLE_NAME + " ON " +
		    KolabItem.TABLE_NAME + "." + KolabItem.CNAME_FK_FOLDER + " = " + ImapFolder.TABLE_NAME + "." +
		    ImapFolder.CNAME_PK_FOLDER + " WHERE " +
		    ImapFolder.TABLE_NAME + "." + ImapFolder.CNAME_NAME + " = '" + folderName + "');");
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		query.executeUpdate();
		transaction.commit();
	}

	public static void clearAllTables(EntityManager em)
	{
		clearKolabItemTable(em);
		Query query = em.createNativeQuery("DELETE FROM " + ImapFolder.TABLE_NAME);
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		query.executeUpdate();
		transaction.commit();
	}

	public static KolabItem getKolabItem(EntityManager em, String folderName, long mailId)
	{
		Query query = em.createQuery(QUERY_SELECT_ITEM_BY_MAIL_ID);
		query.setParameter("folderName", folderName);
		query.setParameter("mailId", mailId);
		try {
			Object result = query.getSingleResult();
			return (KolabItem)result;
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<KolabItem> getKolabItems(EntityManager em, String folderName)
	{
		Query query = em.createQuery(QUERY_SELECT_ITEMS);
		query.setParameter("folderName", folderName);
		try {
			Object result = query.getResultList();
			return (List<KolabItem>)result;
		} catch (NoResultException e) {
			return null;
		}
	}
}
