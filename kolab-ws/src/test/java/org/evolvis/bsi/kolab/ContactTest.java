package org.evolvis.bsi.kolab;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolderM;
import com.sun.mail.imap.IMAPStoreM;
import org.evolvis.bsi.kolab.db.DBTestUtil;
import org.evolvis.bsi.kolab.service.Address;
import org.evolvis.bsi.kolab.service.AddressType;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.Gender;
import org.evolvis.bsi.kolab.service.Phone;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.evolvis.bsi.kolab.service.PreferredAddress;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.evolvis.bsi.kolab.util.FolderType.CONTACT;
import static org.evolvis.bsi.kolab.xml.Helper.parseDate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Hendrik Helwich
 */
public class ContactTest {
	private static EntityManager em = Persistence.createEntityManagerFactory("KolabWsTest").createEntityManager();

	@BeforeClass
	public static void setUpOnce()
	{
		/*
		 * ensure detecting test mode
		 */
		assertTrue(Helper.isInTestMode());
	}

	@Before
	public void startUp() throws MessagingException
	{
		DBTestUtil.clearAllTables(em);
	}

	@After
	public void tearDown()
	{
		DBTestUtil.clearAllTables(em);
	}

	/**
	 * Test if all possible fields of a contact a deserialized correctly from the
	 * kolab storage format.
	 */
	@Test
	public void testFields() throws Exception
	{
		IMAPStoreM store = new IMAPStoreM();
		store.addFolder(new IMAPFolderM(CONTACT, "/user1/Kontakte"));
		KolabTestAccessor ac = new KolabTestAccessor(em, store);

		Folder f = new Folder();
		f.setName("/user1/Kontakte");
		Contact c = ac.getContact(f, "allfieldsset");
		assertEquals("gn", c.getGivenName());
		assertEquals("mn", c.getMiddleNames());
		assertEquals("ln", c.getLastName());
		assertEquals("fn", c.getFullName());
		assertEquals("in", c.getInitials());
		assertEquals("pre", c.getPrefix());
		assertEquals("suf", c.getSuffix());
		assertEquals("fbu", c.getFreeBusyUrl());
		assertEquals("org", c.getOrganization());
		assertEquals("wp", c.getWebPage());
		assertEquals("ia", c.getImAddress());
		assertEquals("dep", c.getDepartment());
		assertEquals("ofl", c.getOfficeLocation());
		assertEquals("prof", c.getProfession());
		assertEquals("jt", c.getJobTitle());
		assertEquals("mn", c.getManagerName());
		assertEquals("as", c.getAssistant());
		assertEquals("nin", c.getNickName());
		assertEquals("sn", c.getSpouseName());
		assertEquals(parseDate("1911-11-11"), c.getBirthday());
		assertEquals(parseDate("1912-12-12"), c.getAnniversary());
		assertEquals("picid", c.getPictureAttachmentId());
		assertEquals("cd", c.getChildren());
		assertEquals(Gender.FEMALE, c.getGender());
		assertEquals("lang", c.getLanguage());

		assertEquals(2, c.getPhone().size());
		Phone p = c.getPhone().get(0);
		assertEquals(PhoneType.HOMEFAX, p.getType());
		assertEquals("nr1", p.getNumber());
		p = c.getPhone().get(1);
		assertEquals(PhoneType.PAGER, p.getType());
		assertEquals("nr2", p.getNumber());

		assertEquals(2, c.getEmail().size());
		Email e = c.getEmail().get(0);
		assertEquals("edn1", e.getDisplayName());
		assertEquals("ead1", e.getSmtpAddress());
		e = c.getEmail().get(1);
		assertEquals("edn2", e.getDisplayName());
		assertEquals("ead2", e.getSmtpAddress());

		assertEquals(2, c.getAddress().size());
		Address a = c.getAddress().get(0);
		assertEquals(AddressType.BUSINESS, a.getType());
		assertEquals("adst1", a.getStreet());
		assertEquals("adlo1", a.getLocality());
		assertEquals("adre1", a.getRegion());
		assertEquals("adpc1", a.getPostalCode());
		assertEquals("adco1", a.getCountry());
		a = c.getAddress().get(1);
		assertEquals(AddressType.OTHER, a.getType());
		assertEquals("adst2", a.getStreet());
		assertEquals("adlo2", a.getLocality());
		assertEquals("adre2", a.getRegion());
		assertEquals("adpc2", a.getPostalCode());
		assertEquals("adco2", a.getCountry());

		assertEquals(PreferredAddress.BUSINESS, c.getPreferredAddress());
		assertEquals(1.1d, c.getLatitude());
		assertEquals(2.2d, c.getLongitude());
	}
}
