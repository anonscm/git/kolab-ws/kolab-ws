package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolderM;
import org.evolvis.bsi.kolab.xml.Helper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Date;
import java.util.List;

import static org.evolvis.bsi.kolab.util.FolderType.CONTACT;
import static org.evolvis.bsi.kolab.util.FolderType.EVENT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author Hendrik Helwich
 */
public class DBSynchronizerTest {
	private static EntityManager em = Persistence.createEntityManagerFactory("KolabWsTest").createEntityManager();

	@Before
	public void startUp() throws MessagingException
	{
		DBTestUtil.clearAllTables(em);
	}

	@After
	public void tearDown()
	{
		DBTestUtil.clearAllTables(em);
	}

	/**
	 * Synchronize a test folder and check if all database values are created as
	 * expected.
	 */
	@Test
	public void testFolder() throws Exception
	{
		String imapFolderName = "/user1/Kalender";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		long time1 = System.currentTimeMillis();
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		long time2 = System.currentTimeMillis();
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);

		assertEquals(3, items.size());
		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			long mailId = item.getMailId();
			if (mailId == 12) {
				assertEquals("uid1", item.getKolabId());
				assertTrue(item.getValid());
				assertFalse(item.getDeleted());
				assertEquals(item.getChangeDate(), item.getCreationDate());
				assertTrue(item.getChangeDate().getTime() >= time1);
				assertTrue(item.getChangeDate().getTime() <= time2);
				Date date = Helper.parseDatetime("2009-07-23T07:00:00Z");
				Date date2 = Helper.parseDatetime("2009-07-23T08:00:00Z");
				assertEquals(date, item.getIncidenceFirstDate());
				assertEquals(date2, item.getIncidenceLastDate());
			} else if (mailId == 13) {
				assertEquals("uid2", item.getKolabId());
				assertTrue(item.getValid());
				assertFalse(item.getDeleted());
				assertEquals(item.getChangeDate(), item.getCreationDate());
				assertTrue(item.getChangeDate().getTime() >= time1);
				assertTrue(item.getChangeDate().getTime() <= time2);
				Date date = Helper.parseDatetime("2009-07-24T23:59:59Z");
				Date date2 = Helper.parseDatetime("2009-07-23T15:00:00Z");
				assertEquals(date, item.getIncidenceFirstDate());
				assertEquals(date2, item.getIncidenceLastDate());
			} else if (mailId == 312) {
				assertEquals("uid3", item.getKolabId());
				assertTrue(item.getValid());
				assertFalse(item.getDeleted());
				assertEquals(item.getChangeDate(), item.getCreationDate());
				assertTrue(item.getChangeDate().getTime() >= time1);
				assertTrue(item.getChangeDate().getTime() <= time2);
				Date date = Helper.parseDatetime("3009-12-23T00:00:00Z");
				assertEquals(date, item.getIncidenceFirstDate());
				assertEquals(date, item.getIncidenceLastDate());
			} else
				fail("unknown mail id " + mailId);
		}
		assertEquals(0, items.size());
	}

	/**
	 * Synchronize a test folder and check if a mail which is marked as deleted
	 * gets a database entry as expected.
	 */
	@Test
	public void testMarkDeleted() throws Exception
	{
		String imapFolderName = "/user1/markDeleted";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);

		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			long mailId = item.getMailId();
			assertEquals("wrong value for mail id " + mailId, mailId == 13, item.getDeleted());
		}
	}

	/**
	 * Synchronize a test folder and check if a mail which holds an invalid
	 * kolab xml attachment gets a database entry as expected.
	 */
	@Test
	public void testValid() throws Exception
	{
		String imapFolderName = "/user1/invalid";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);

		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			long mailId = item.getMailId();
			assertEquals("wrong value for mail id " + mailId, mailId != 22, item.getValid());
		}
	}

	/**
	 * Synchronize a test folder, delete a mail in this folder and synchronize
	 * again. Test afterwards if the database entry for this mail is marked as
	 * deleted as expected.
	 */
	@Test
	public void testDeleted() throws Exception
	{
		String imapFolderName = "/user1/Kontakte";
		IMAPFolderM folder = new IMAPFolderM(CONTACT, imapFolderName);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(3, items.size());
		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			assertFalse(item.getDeleted());
			assertEquals(item.getCreationDate(), item.getChangeDate());
		}
		folder.removeMessage(11);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(3, items.size());
		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			long mailId = item.getMailId();
			if (mailId == 11) {
				assertTrue(item.getDeleted());
				assertTrue(item.getCreationDate().getTime() < item.getChangeDate().getTime());
			} else {
				assertFalse(item.getDeleted());
				assertEquals(item.getCreationDate(), item.getChangeDate());
			}
		}
	}

	/**
	 * Synchronize a folder with two mails which have the same kolab id.
	 * Test that only one db entry is created and that it is the one with the
	 * higher mail id (by checking the start date).
	 */
	@Test
	public void testDuplicate() throws Exception
	{
		String imapFolderName = "/user1/duplicate";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(1, items.size());
		Date date = Helper.parseDatetime("2009-07-23T07:00:00Z");
		assertEquals(date, items.get(0).getIncidenceFirstDate());
	}

	/**
	 * Synchronize a folder with two mails, add a mail and synchronize again.
	 * Test that 3 db entries with the expected mail ids are existing.
	 */
	@Test
	public void testNew() throws Exception
	{
		String imapFolderName = "/user1/Kalender";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		folder.removeMessage(312);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(2, items.size());
		folder = new IMAPFolderM(EVENT, imapFolderName);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(3, items.size());
		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			long mailId = item.getMailId();
			assertTrue(mailId == 12 || mailId == 13 || mailId == 312);
		}
		assertEquals(0, items.size());
	}

	/**
	 * Synchronize a folder with one mail, add a mail with the same kolab id and
	 * remove the other mail and synchronize again.
	 * Test that one db entry with the expected mail id exists.
	 */
	@Test
	public void testUpdate() throws Exception
	{
		testUpdate(false);
	}

	/**
	 * Like {@link #testUpdate()} but the mail is only marked deleted instead
	 * of deleted.
	 */
	@Test
	public void testUpdateMarkDeleted() throws Exception
	{
		testUpdate(true);
	}

	public void testUpdate(boolean mark) throws Exception
	{
		String imapFolderName = "/user1/duplicate";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		folder.removeMessage(22);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(1, items.size());
		folder = new IMAPFolderM(EVENT, imapFolderName);
		if (mark)
			folder.getMessageByUID(12).setMarkedDeleted(true);
		else
			folder.removeMessage(12);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(1, items.size());
		KolabItem item = items.get(0);
		assertEquals(22, item.getMailId());
	}

	/**
	 * Synchronize a folder with two mails with uids 13 and 312. Synchronize the
	 * same folder again with the mail uids 12 and 13.
	 * The sync algorithm should print a warning that the imap index could have
	 * been rebuild.
	 * Test if the database holds the entries (for uid 12, 13 and 312).
	 */
	@Test
	public void testMailIdTooLow() throws Exception
	{
		String imapFolderName = "/user1/Kalender";
		IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
		folder.removeMessage(12);
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(2, items.size());
		folder = new IMAPFolderM(EVENT, "/user1/markDeleted");
		folder.setFullName("/user1/Kalender");
		DBSynchronizer.syncFolderIds(folder, true, em, null);
		items = DBTestUtil.getKolabItems(em, imapFolderName);
		assertEquals(3, items.size());
		while (items.size() > 0) {
			KolabItem item = items.remove(0);
			long mailId = item.getMailId();
			assertTrue(mailId == 12 || mailId == 13 || mailId == 312);
		}
		assertEquals(0, items.size());
	}
}
