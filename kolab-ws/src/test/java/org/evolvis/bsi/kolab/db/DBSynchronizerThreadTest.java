package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolderM2;
import org.evolvis.bsi.kolab.Helper;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.evolvis.bsi.kolab.util.FolderType.EVENT;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test if the synchronization algorithm is synchronized for the same folder
 * names and not synchronized otherwise.
 *
 * @author Hendrik Helwich
 */
public class DBSynchronizerThreadTest {
	private static EntityManager em = Persistence.createEntityManagerFactory("KolabWsTest").createEntityManager();

	String imapFolderName;

	@BeforeClass
	public static void setUpOnce()
	{
		/*
		 * ensure detecting test mode
		 */
		assertTrue(Helper.isInTestMode());
	}

	@Before
	public void startUp()
	{
		imapFolderName = "/user1/Kalender";
		DBTestUtil.clearAllTables(em);
	}

	@After
	public void tearDown()
	{
		DBTestUtil.clearAllTables(em);
	}

	/**
	 * Start two threads which do some synchronizations of the same folder.
	 * It is tested if the synchronization algorithm is implemented in a
	 * synchronized way.
	 */
	@Test
	public void testSyncFolderIdsSameName() throws Exception
	{
		testSyncFolderIds(false, 20);
	}

	/**
	 * Start two threads which do some synchronizations of different folders.
	 * It is tested if the synchronization algorithm is implemented in a
	 * not synchronized way.
	 */
	@Test
	public void testSyncFolderIds() throws Exception
	{
		testSyncFolderIds(true, 100);
	}

	public void testSyncFolderIds(boolean appendThreadId, int iterations) throws Exception
	{
		IMAPFolderM2 folder = new IMAPFolderM2(EVENT, imapFolderName, appendThreadId);
		SyncRunnable r1 = new SyncRunnable(
		    Persistence.createEntityManagerFactory("KolabWsTest").createEntityManager(), folder, iterations);
		SyncRunnable r2 = new SyncRunnable(
		    Persistence.createEntityManagerFactory("KolabWsTest").createEntityManager(), folder, iterations);
		Thread thread1 = new Thread(r1);
		Thread thread2 = new Thread(r2);
		thread1.start();
		thread2.start();
		while (thread1.isAlive() || thread2.isAlive()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
		if (r1.exception != null) {
			throw r1.exception;
		}
		if (r2.exception != null) {
			throw r2.exception;
		}
		// assertion should occur if folder with different names is used
		if (appendThreadId) {
			if (folder.assertionError == null) {
				fail("assertion should occur if folder with different names is used");
			}
		} else if (folder.assertionError != null) {
			throw folder.assertionError;
		}
	}

	/**
	 * Can be used to do multiple synchronizations in a thread.
	 */
	private class SyncRunnable implements Runnable {

		private EntityManager em;
		public Exception exception;
		private final IMAPFolderM2 folder;
		private int iterations;

		public SyncRunnable(EntityManager em, IMAPFolderM2 folder, int iterations)
		{
			this.em = em;
			this.folder = folder;
			this.iterations = iterations;
		}

		@Override
		public void run()
		{
			try {
				for (int i = 0; folder.assertionError == null && i < iterations; i++) {
					// delete all folder items
					DBTestUtil.clearKolabItemTableByFolderName(em, folder.getFullName());

					// start sync
					DBSynchronizer.syncFolderIds(folder, true, em, null);

					if (i % 10 == 0) {
						System.out.print(i + " ");
					}
				}
			} catch (Exception e) {
				this.exception = e;
			}
		}
	}

	;
}
