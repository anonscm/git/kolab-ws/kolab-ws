package org.evolvis.bsi.kolab.imap;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.util.FolderType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;

public class ImapHelperTest {
	/**
	 * Test if integer values of the {@link FolderType} enum are unique.
	 */
	@Test
	public void testFolderTypeIntValuesUnique()
	{
		FolderType[] values = FolderType.values();
		List<Integer> ids = new ArrayList<Integer>(values.length);
		for (FolderType ft : values) {
			assertFalse(ids.contains(ft.intValue()));
			ids.add(ft.intValue());
		}
	}

	/**
	 * Test {@link FolderType#getType(int)} and {@link FolderType#intValue()}.
	 */
	@Test
	public void testFolderTypeGetType()
	{
		for (FolderType ft : FolderType.values()) {
			assertEquals(ft, FolderType.getType(ft.intValue()));
			assertNotSame(ft.intValue(), 0);
		}
	}
}
