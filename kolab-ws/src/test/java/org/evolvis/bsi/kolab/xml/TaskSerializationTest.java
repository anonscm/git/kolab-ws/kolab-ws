package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.evolvis.bsi.kolab.service.Status;
import org.evolvis.bsi.kolab.service.Task;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author Hendrik Helwich
 */
public class TaskSerializationTest {
	@Test
	public void testSerialization() throws XMLStreamException, KolabParseException
	{
		Task task;
		task = new Task();
		task.setCreationDate(ContactSerializationTest.createDate());
		task.setLastModificationDate(ContactSerializationTest.createDate());
		task.setSensitivity(Sensitivity.PUBLIC);
		checkSerialization(task);
		for (int i = 0; i < 84; i++) {
			task = createFullTask(i, i % 2 == 0);
			checkSerialization(task);
		}
	}

	private static void checkSerialization(Task task) throws XMLStreamException, KolabParseException
	{
		ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(task, xmlStream);
		byte[] xml = xmlStream.toByteArray();
		Task task2 = KolabSerializer.readTask(new ByteArrayInputStream(xml));
		assertEquals_(task, task2);
		// convert again to xml string and compare to get an error if a field is forgotten
		xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(task2, xmlStream);
		byte[] xml2 = xmlStream.toByteArray();
		assertEquals(new String(xml), new String(xml2));
	}

	private static Task createFullTask(int type, boolean dueDateAllDay)
	{
		Task task = new Task();
		ContactSerializationTest.fillCommonFields(task);
		EventSerializationTest.fillIncidenceFields(task, type);
		task.setPriority(5);
		task.setCompleted(12);
		task.setStatus(Status.DEFERRED);
		task.setDueDateAllDay(dueDateAllDay);
		task.setDueDate(new Date((dueDateAllDay ? 86400000L : 1000L * 87654) * 12345));
		task.setParent("prnt1");
		return task;
	}

	private static void assertEquals_(Task t1, Task t2)
	{
		if (t1 == null && t2 == null)
			return;
		if (t1 == null || t2 == null)
			throw new AssertionError("one task is null");
		ContactSerializationTest.assertEquals_((CollaborationItem)t1, (CollaborationItem)t2);
		assertEquals(t1.getPriority(), t2.getPriority());
		assertEquals(t1.getCompleted(), t2.getCompleted());
		assertEquals(t1.getStatus(), t2.getStatus());
		assertEquals(t1.getDueDate(), t2.getDueDate());
		assertEquals(t1.isDueDateAllDay(), t2.isDueDateAllDay());
		assertEquals(t1.getParent(), t2.getParent());
	}
}
