package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.junit.Test;

import java.text.ParseException;
import java.util.Date;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class HelperTest {
	@Test
	public void testToNumberString()
	{
		assertEquals("0", Helper.toNumberString(0d));
		assertEquals("-0", Helper.toNumberString(-0d));
		assertEquals("1", Helper.toNumberString(1d));
		assertEquals("-1", Helper.toNumberString(-1d));
		assertEquals("1.1", Helper.toNumberString(1.1));
		assertEquals("-1.1", Helper.toNumberString(-1.1));
		assertEquals("0.001", Helper.toNumberString(0.001));
		assertEquals("12345.6789", Helper.toNumberString(12345.6789));
		assertEquals(
		    "0.0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000049",
		    Helper.toNumberString(Double.MIN_VALUE));
		assertEquals(
		    "179769313486231570000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
		    Helper.toNumberString(Double.MAX_VALUE));
	}

	@Test
	public void testParseDouble() throws ParseException
	{
		assertEquals(0, Helper.parseDouble("0"));
		assertEquals(0, Helper.parseDouble("-0"));
		assertEquals(1, Helper.parseDouble("1"));
		assertEquals(-1, Helper.parseDouble("-1"));
		assertEquals(-1.1, Helper.parseDouble("-1.1"));
		assertEquals(.001, Helper.parseDouble("0.001"));
		assertEquals(12345.6789, Helper.parseDouble("12345.6789"));
		assertEquals(Double.MIN_VALUE, Helper.parseDouble(
		    "0.0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000049"
		));
		assertEquals(Double.MAX_VALUE, Helper.parseDouble(
		    "179769313486231570000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
		));
	}

	@Test
	public void testRandomBijectiveDoubleConversion() throws ParseException
	{
		Random random = new Random();
		for (int i = 0; i < 1000; i++) {
			// create random double
			double d = random.nextDouble();
			if (random.nextBoolean())
				d = -d;
			d *= random.nextInt(100) + 1;
			// convert to string and back
			String s = Helper.toNumberString(d);
			double d2 = Helper.parseDouble(s);
			// check doubles have the same value
			assertEquals(d, d2);
		}
	}

	@Test
	public void testParseDateTime() throws KolabParseException
	{
		final long _2004_05_04__15_00 = 1083682800000L;
		Date date;
		date = Helper.parseDatetime("2004-05-04T15:00:00Z");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-04T15:00:00+00:00");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-04T16:00:00+01:00");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-04T16:23:00+01:23");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-04T13:37:00-01:23");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-04T13:37:00-01:23");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-05T01:00:00+10:00");
		assertEquals(_2004_05_04__15_00, date.getTime());
		date = Helper.parseDatetime("2004-05-04T17:00:00.525+02:00");
		assertEquals(_2004_05_04__15_00, date.getTime());
	}
}
