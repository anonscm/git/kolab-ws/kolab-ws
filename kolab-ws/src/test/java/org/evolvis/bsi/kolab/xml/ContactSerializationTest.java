package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.service.Address;
import org.evolvis.bsi.kolab.service.AddressType;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.Gender;
import org.evolvis.bsi.kolab.service.Phone;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.evolvis.bsi.kolab.service.PreferredAddress;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * @author Hendrik Helwich
 */
public class ContactSerializationTest {
	@Test
	public void testSerialization() throws XMLStreamException, KolabParseException
	{
		Contact contact;
		contact = new Contact();
		contact.setCreationDate(createDate());
		contact.setLastModificationDate(createDate());
		contact.setPreferredAddress(PreferredAddress.NONE);
		contact.setSensitivity(Sensitivity.PUBLIC);
		checkSerialization(contact);
		contact = createFullContact();
		checkSerialization(contact);
	}

	private static void checkSerialization(Contact contact) throws XMLStreamException, KolabParseException
	{
		ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(contact, xmlStream);
		byte[] xml = xmlStream.toByteArray();
		Contact contact2 = KolabSerializer.readContact(new ByteArrayInputStream(xml));
		assertEquals_(contact, contact2);
		// convert again to xml string and compare to get an error if a field is forgotten
		xmlStream = new ByteArrayOutputStream();
		KolabSerializer.write(contact2, xmlStream);
		byte[] xml2 = xmlStream.toByteArray();
		assertEquals(new String(xml), new String(xml2));
	}

	static void fillCommonFields(CollaborationItem item)
	{
		item.setUid("uid1");
		item.setBody("bdy1");
		item.setCategories("cat1");
		item.setCreationDate(new Date(1000L * 123));
		item.setLastModificationDate(new Date(1000L * 66666));
		item.setSensitivity(Sensitivity.CONFIDENTIAL);
		item.getInlineAttachment().add("ilattch1");
		item.getInlineAttachment().add("ilattch2");
		item.getLinkAttachment().add("lkattch1");
		item.getLinkAttachment().add("lkattch2");
		item.setProductId("pdctid1");
	}

	private static Contact createFullContact()
	{
		Contact contact = new Contact();
		fillCommonFields(contact);

		contact.setGivenName("gn1");
		contact.setMiddleNames("mn1");
		contact.setLastName("ln1");
		contact.setFullName("fn1");
		contact.setInitials("in1");
		contact.setPrefix("pref1");
		contact.setSuffix("sf1");
		contact.setFreeBusyUrl("fbu1");
		contact.setOrganization("org1");
		contact.setWebPage("web1");
		contact.setImAddress("ia1");
		contact.setDepartment("dep1");
		contact.setOfficeLocation("ofl1");
		contact.setProfession("prof1");
		contact.setJobTitle("jobt1");
		contact.setManagerName("man1");
		contact.setAssistant("as1");
		contact.setNickName("nin1");
		contact.setSpouseName("spn1");
		contact.setBirthday(new Date(86400000L * 123)); // how many days since 1.1.1970 (no time allowed here)
		contact.setAnniversary(new Date(86400000L * 10001)); // see above
		contact.setPictureAttachmentId("pica1");
		contact.setChildren("ch1");
		contact.setGender(new Random().nextBoolean() ? Gender.FEMALE : Gender.MALE);
		contact.setLanguage("lang1");

		Phone phone = new Phone();
		phone.setType(PhoneType.BUSINESS_2);
		phone.setNumber("nbr1");
		contact.getPhone().add(phone);
		phone = new Phone();
		phone.setType(PhoneType.HOMEFAX);
		phone.setNumber("nbr2");
		contact.getPhone().add(phone);

		Email email = new Email();
		email.setDisplayName("dspn1");
		email.setSmtpAddress("smtpa1");
		contact.getEmail().add(email);
		email = new Email();
		email.setDisplayName("dspn2");
		email.setSmtpAddress("smtpa2");
		contact.getEmail().add(email);

		Address address = new Address();
		address.setType(AddressType.HOME);
		address.setStreet("str1");
		address.setLocality("loc1");
		address.setRegion("reg1");
		address.setPostalCode("postc1");
		address.setCountry("count1");
		contact.getAddress().add(address);
		address = new Address();
		address.setType(AddressType.OTHER);
		address.setStreet("str2");
		address.setLocality("loc2");
		address.setRegion("reg2");
		address.setPostalCode("postc2");
		address.setCountry("count2");
		contact.getAddress().add(address);

		contact.setPreferredAddress(PreferredAddress.NONE);
		contact.setLatitude(1.23456);
		contact.setLongitude(234234234.0234234);
		return contact;
	}

	/**
	 * Create a {@link Date} instance with the current time but with
	 * milliseconds set to zero.
	 */
	static Date createDate()
	{
		long now = System.currentTimeMillis();
		now = now / 1000 * 1000;
		return new Date(now);
	}

	static void assertEquals_(CollaborationItem i1, CollaborationItem i2)
	{
		assertEquals(i1.getUid(), i2.getUid());
		assertEquals(i1.getBody(), i2.getBody());
		assertEquals(i1.getCategories(), i2.getCategories());
		assertEquals(i1.getCreationDate(), i2.getCreationDate());
		assertEquals(i1.getLastModificationDate(), i2.getLastModificationDate());
		assertEquals(i1.getSensitivity(), i2.getSensitivity());
		assertEquals(i1.getInlineAttachment(), i2.getInlineAttachment());
		assertEquals(i1.getProductId(), i2.getProductId());
	}

	private static void assertEquals_(Contact c1, Contact c2)
	{
		if (c1 == null && c2 == null)
			return;
		if (c1 == null || c2 == null)
			throw new AssertionError("one contact is null");
		assertEquals_((CollaborationItem)c1, (CollaborationItem)c2);
		assertEquals(c1.getGivenName(), c2.getGivenName());
		assertEquals(c1.getMiddleNames(), c2.getMiddleNames());
		assertEquals(c1.getLastName(), c2.getLastName());
		assertEquals(c1.getFullName(), c2.getFullName());
		assertEquals(c1.getInitials(), c2.getInitials());
		assertEquals(c1.getPrefix(), c2.getPrefix());
		assertEquals(c1.getSuffix(), c2.getSuffix());
		assertEquals(c1.getFreeBusyUrl(), c2.getFreeBusyUrl());
		assertEquals(c1.getOrganization(), c2.getOrganization());
		assertEquals(c1.getWebPage(), c2.getWebPage());
		assertEquals(c1.getImAddress(), c2.getImAddress());
		assertEquals(c1.getDepartment(), c2.getDepartment());
		assertEquals(c1.getOfficeLocation(), c2.getOfficeLocation());
		assertEquals(c1.getProfession(), c2.getProfession());
		assertEquals(c1.getJobTitle(), c2.getJobTitle());
		assertEquals(c1.getManagerName(), c2.getManagerName());
		assertEquals(c1.getAssistant(), c2.getAssistant());
		assertEquals(c1.getNickName(), c2.getNickName());
		assertEquals(c1.getSpouseName(), c2.getSpouseName());
		assertEquals(c1.getBirthday(), c2.getBirthday());
		assertEquals(c1.getAnniversary(), c2.getAnniversary());
		assertEquals(c1.getPictureAttachmentId(), c2.getPictureAttachmentId());
		assertEquals(c1.getChildren(), c2.getChildren());
		assertEquals(c1.getGender(), c2.getGender());
		assertEquals(c1.getLanguage(), c2.getLanguage());
		List<Phone> pl1 = c1.getPhone();
		List<Phone> pl2 = c2.getPhone();
		assertEquals(pl1.size(), pl2.size());
		for (int i = 0; i < pl1.size(); i++) {
			Phone p1 = pl1.get(i);
			Phone p2 = pl2.get(i);
			assertEquals(p1.getType(), p2.getType());
			assertEquals(p1.getNumber(), p2.getNumber());
		}
		List<Email> el1 = c1.getEmail();
		List<Email> el2 = c2.getEmail();
		assertEquals(el1.size(), el2.size());
		for (int i = 0; i < el1.size(); i++) {
			Email e1 = el1.get(i);
			Email e2 = el2.get(i);
			assertEquals(e1.getDisplayName(), e2.getDisplayName());
			assertEquals(e1.getSmtpAddress(), e2.getSmtpAddress());
		}
		List<Address> al1 = c1.getAddress();
		List<Address> al2 = c2.getAddress();
		assertEquals(al1.size(), al2.size());
		for (int i = 0; i < al1.size(); i++) {
			Address a1 = al1.get(i);
			Address a2 = al2.get(i);
			assertEquals(a1.getType(), a2.getType());
			assertEquals(a1.getStreet(), a2.getStreet());
			assertEquals(a1.getLocality(), a2.getLocality());
			assertEquals(a1.getRegion(), a2.getRegion());
			assertEquals(a1.getPostalCode(), a2.getPostalCode());
			assertEquals(a1.getCountry(), a2.getCountry());
		}
		assertEquals(c1.getPreferredAddress(), c2.getPreferredAddress());
		assertEquals(c1.getLatitude(), c2.getLatitude());
		assertEquals(c1.getLongitude(), c2.getLongitude());
	}
}
