package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import com.sun.mail.imap.IMAPFolderM;
import org.evolvis.bsi.kolab.Helper;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Random;

import static org.evolvis.bsi.kolab.util.FolderType.EVENT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author Hendrik Helwich
 */
public class DBSynchronizerExternalWriteTest {
	private static EntityManager em = Persistence.createEntityManagerFactory("KolabWsTest").createEntityManager();

	@BeforeClass
	public static void setUpOnce()
	{
		/*
		 * ensure detecting test mode
		 */
		assertTrue(Helper.isInTestMode());
	}

	@Before
	public void setUp() throws Exception
	{
		DBTestUtil.clearAllTables(em);
	}

	@After
	public void tearDown() throws Exception
	{
		DBTestUtil.clearAllTables(em);
	}

	/**
	 * Start two threads: One which is doing synchronizations of the same folder
	 * and another which is randomly deleting mails from this folder.
	 * Test that no errors do occur.
	 */
	@Test
	public void testDeleted() throws Exception
	{
		FolderHolder holder = new FolderHolder();
		SyncRunnable syncR = new SyncRunnable(holder);
		DeletingAppRunnable exteranlApp = new DeletingAppRunnable(holder);
		Thread thread1 = new Thread(syncR);
		Thread thread2 = new Thread(exteranlApp);
		thread1.start();
		thread2.start();
		while (thread1.isAlive())
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		thread2.interrupt();
		if (syncR.exception != null)
			throw syncR.exception;
		if (exteranlApp.exception != null)
			throw exteranlApp.exception;
	}

	class FolderHolder {

		private IMAPFolderM folder;

		public IMAPFolderM getFolder()
		{
			return folder;
		}

		public void setFolder(IMAPFolderM folder)
		{
			this.folder = folder;
		}
	}

	class SyncRunnable implements Runnable {

		private static final int ITERATIONS = 20;

		private FolderHolder holder;
		Exception exception;

		public SyncRunnable(FolderHolder holder)
		{
			this.holder = holder;
		}

		@Override
		public void run()
		{

			try {
				String imapFolderName = "/user1/Kalender";
				for (int i = 0; i < ITERATIONS; i++) {
					IMAPFolderM folder = new IMAPFolderM(EVENT, imapFolderName);
					holder.setFolder(folder);
					DBTestUtil.clearAllTables(em);
					DBSynchronizer.syncFolderIds(folder, true, em, null);
					List<KolabItem> items = DBTestUtil.getKolabItems(em, imapFolderName);
					while (items.size() > 0) {
						KolabItem item = items.remove(0);
						long mailId = item.getMailId();
						if (mailId != 12 && mailId != 312 && mailId != 13)
							fail("unknown mail id " + mailId);
					}
					assertEquals(0, items.size());
				}
			} catch (Exception e) {
				exception = e;
			}
		}
	}

	;

	class DeletingAppRunnable implements Runnable {

		private FolderHolder holder;
		Exception exception;

		public DeletingAppRunnable(FolderHolder holder)
		{
			this.holder = holder;
		}

		@Override
		public void run()
		{
			try {
				Random random = new Random();
				while (true) {
					IMAPFolderM folder = holder.getFolder();
					if (folder != null) {
						switch (random.nextInt(3)) {
						case 0:
							folder.removeMessage(12);
							break;
						case 1:
							folder.removeMessage(13);
							break;
						case 2:
							folder.removeMessage(312);
							break;
						}
					}
					Thread.sleep(random.nextInt(100));
				}
			} catch (Exception e) {
				exception = e;
			}
		}
	}

	;
}
