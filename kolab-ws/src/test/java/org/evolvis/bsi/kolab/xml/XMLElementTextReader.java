package org.evolvis.bsi.kolab.xml;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

class XMLElementTextReader implements XMLStreamReader {
	public XMLElementTextReader(String text)
	{
		this.text = text;
	}

	private String text;

	@Override
	public String getElementText() throws XMLStreamException
	{
		return text;
	}

	@Override
	public void close() throws XMLStreamException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getAttributeCount()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAttributeLocalName(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public QName getAttributeName(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAttributeNamespace(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAttributePrefix(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAttributeType(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAttributeValue(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAttributeValue(String arg0, String arg1)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getCharacterEncodingScheme()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getEncoding()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getEventType()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getLocalName()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Location getLocation()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public QName getName()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getNamespaceCount()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getNamespacePrefix(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getNamespaceURI()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getNamespaceURI(String arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getNamespaceURI(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getPIData()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getPITarget()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getPrefix()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object getProperty(String arg0) throws IllegalArgumentException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getText()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public char[] getTextCharacters()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getTextCharacters(int arg0, char[] arg1, int arg2, int arg3)
	throws XMLStreamException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getTextLength()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int getTextStart()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String getVersion()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasName()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasNext() throws XMLStreamException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasText()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isAttributeSpecified(int arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isCharacters()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEndElement()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isStandalone()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isStartElement()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isWhiteSpace()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int next() throws XMLStreamException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int nextTag() throws XMLStreamException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void require(int arg0, String arg1, String arg2)
	throws XMLStreamException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean standaloneSet()
	{
		throw new UnsupportedOperationException();
	}
}
