-- kolab-ws database schema; AUTOMATICALLY GENERATED, DO NOT EDIT!

    create table folder (
        pk_folder int8 not null,
        last_sync_time int8,
        name varchar(255) not null unique,
        shared bool,
        type int4 not null,
        primary key (pk_folder)
    );

    create table folder_visibility (
        pk_folder_visibility int8 not null,
        change_date int8 not null,
        visible bool not null,
        fk_folder int8 not null,
        fk_user int8 not null,
        primary key (pk_folder_visibility),
        unique (fk_folder, fk_user, change_date)
    );

    create table item (
        pk_item int8 not null,
        change_date int8 not null,
        change_device_id varchar(255),
        change_user_id varchar(255),
        create_device_id varchar(255),
        create_user_id varchar(255),
        creation_date int8 not null,
        deleted bool not null,
        incidence_first_date int8,
        incidence_last_date int8,
        kolab_id varchar(255),
        mail_id int8 not null,
        valid bool not null,
        fk_folder int8,
        primary key (pk_item),
        unique (fk_folder, mail_id),
        unique (fk_folder, kolab_id)
    );

    create table user_tbl (
        pk_user int8 not null,
        name varchar(255) not null unique,
        primary key (pk_user)
    );

    alter table folder_visibility
        add constraint FK50832B83EA80437F
        foreign key (fk_user)
        references user_tbl;

    alter table folder_visibility
        add constraint FK50832B839ACEC138
        foreign key (fk_folder)
        references folder;

    alter table item
        add constraint FK317B139ACEC138
        foreign key (fk_folder)
        references folder;

    create sequence hibernate_sequence;

-- end of kolab-ws database schema
