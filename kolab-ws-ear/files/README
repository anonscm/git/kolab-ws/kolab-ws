Kurzinstallationsanleitung für den Kolab-Webservice
===================================================

Die Installation unter Zuhilfename von OpenJDK 8 aus jessie-backports
auf Debian jessie wird angenommen, bitte installieren Sie dies zuerst.
Eine Installation mit OpenJDK 8 auf Debian squeeze ist auch möglich.

Konkret wird das Paket openjdk-8-jre-headless benötigt. Außerdem muß
ein PostgreSQL-Datenbankserver (Paket postgresql) eingerichtet sein.
Desweiteren wird das Paket libpostgresql-jdbc-java aus jessie (keiner
älteren Distribution!) benötigt.

Die aktuelle Version des Kolab-Webservice kann nicht mit SimKolab vor
Version 4.1 benutzt werden.

Im Lieferumfang finden Sie im folgenden benötigte Dateien:
- schema.sql
- standalone.xml
- javax.mail-1.5.3.jar
- kolab-ws.properties
- commons-collections-3.2.2.jar

In javax.mail-1.5.3-src.tar.gz finden Sie den Quellkode für das, von
uns (siehe unten) angepaßte, javax.mail-1.5.3.jar Paket.

In commons-collections-3.2.2-src.tar.gz finden Sie den vollständigen
korrespondierenden Quellkode und Lizenzbestimmungen für das JAR, von
Upstream unverändert durchgereicht. Dieses Artefakt ist lediglich zu
Ihrer Konvenienz mitgeliefert und nicht Bestandteil unserer Software
und daher ohne Gewährleistung von uns.


Datenbank erstellen
-------------------

sudo -u postgres createuser -D -P -R -S kolab-ws
sudo -u postgres createdb -E UTF-8 -O kolab-ws -T template0 kolab-ws
psql -h 127.0.0.1 -U kolab-ws -W -f schema.sql kolab-ws

Bei einem Upgrade von kolab-ws 1.1.x beachten Sie bitte, daß sich
der Name der Tabelle „user“ in „user_tbl“ geändert hat. Falls Sie
die Datenbank nicht mit den Altdaten füllen synchronisieren Sie die
Endgeräte neu.


WildFly (JBoss Application Server)
----------------------------------

- Laden Sie die neueste 9.x-Version (Java EE7 Full & Web Distribution, TGZ)
  von http://wildfly.org/downloads/ herunter, z.B: wildfly-9.0.2.Final.tar.gz
- Entpacken Sie diese z.B. nach /opt; im folgenden wird mit $WILDFLY_DIR
  der Installationspfad /opt/wildfly-9.0.2.Final bezeichnet
- Ersetzen Sie entweder $WILDFLY_DIR/standalone/configuration/standalone.xml
  durch die mitgelieferte standalone.xml oder wenden Sie den mitgelieferten
  Patch standalone.diff auf Ihre Konfiguration an; unsere Dateien basieren
  auf der Version 9.0.2.Final
- Passen Sie nun in der standalone.xml einige Einträge an; insbesondere an
  allen mit „XXX“ markierten Stellen:
  * datasource jndi-name="java:jboss/datasources/KolabWSDS"
    enthält die Datenbankkonfiguration (früher kolabws-ds.xml)
  * security-domain name="LdapAuth" & security-domain name="jaspi"
    enthalten die LDAP-Konfiguration (früher login-config.xml)
  Nehmen Sie weitere Änderungen (z.B. Ports und IP-Binding) nach Bedarf vor
- Kopieren Sie /usr/share/java/postgresql-jdbc4-9.2.jar aus dem
  Debian-Paket libpostgresql-jdbc-java (= 9.2-1002-1) in das
  Verzeichnis $WILDFLY_DIR/standalone/deployments/
- Kopieren Sie die von uns mitgelieferte javax.mail-1.5.3.jar nach
  $WILDFLY_DIR/modules/system/layers/base/javax/mail/api/main/
- Kopieren Sie die commons-collections-3.2.2.jar (siehe oben) nach
  $WILDFLY_DIR/modules/system/layers/base/org/apache/commons/collections/main/
  und passen dann in demselben Verzeichnis in der Datei module.xml den
  Dateinamen an und *löschen* commons-collections-3.2.1.jar (CVE-2015-4852)!
- Installieren Sie die kolab-ws-ear-<VERSION>.ear als
  $WILDFLY_DIR/standalone/deployments/kolab-ws-ear.ear
- Passen Sie die Datei kolab-ws.properties an (bei einem Upgrade:
  übernehmen Sie die alte Datei) und kopieren sie nach
  $WILDFLY_DIR/standalone/configuration/

Sie können nun vermittels $WILDFLY_DIR/bin/standalone.sh den
Applikationsserver starten, zum Beispiel durch Hinzufügen folgenden
Aufrufs in /etc/rc.local:

($WILDFLY_DIR/bin/standalone.sh >$WILDFLY_DIR/standalone/log/console.log 2>&1 || :) &


SimKolab
--------

In /etc/simkolab/common.php müssen die WSDL-Adressen nach folgendem
Schema angepaßt werden:
- SYNCPHONY_SERVICE_WSDL:
  http://<host>:<port>/kolab-ws/KolabService/KolabServicePortTypeImpl?wsdl
- SYNCPHONY_SERVICE_STATUS_WSDL
  http://<host>:<port>/kolab-ws/KolabServiceStatus/KolabServiceStatusPortTypeImpl?wsdl


Test
----

Zunächst einmal können Sie die Version der ausgebrachten (deployed)
Applikation abfragen:

curl http://<host>:<port>/kolab-ws-ear/artifact-version

Zum Testen müssen Sie einen User in Kolab anlegen und dann die Datei
/usr/share/doc/simkolab-common/examples/test-soap-call.php in Ihr
Homeverzeichnis kopieren und anpassen: SYNCPHONY_SERVICE_WSDL, falls
nötig, $x_username (die volle eMail-Adresse), sowie $x_password.
Dann rufen Sie einfach „php test-soap-call.php“ auf; erwartet wird:

stdClass Object
(
    [return] => 2016-04-22T12:20:09.100Z
)


Externe Komponenten (nur gebündelt, nicht Teil von kolab-ws)
-------------------

- Die WildFly-Konfiguration standalone.xml ist nicht formell durch
  Urheberrecht geschützt; WildFly selbst wird unter der LGPLv2.1
  vertrieben.

- JavaMail wird unter der CDDLv1.0 vertrieben; den Lizenztext finden
  Sie unter META-INF/LICENSE.txt in der jar-Datei selbst.
  * §3.1: Sie sind hiermit informiert, daß Sie die Quellkodeform in
    dem mitgelieferten gzip-komprimierten POSIX-ustar-Archiv vorfinden.
  * §3.3: Die Modifikation ist durch die Versionsnummer gekennzeichnet
    (Bundle-Version: 1.5.3.tarent_7 in META-INF/MANIFEST.MF).

- commons-collections-3.2.2.jar sollte eigentlich von WildFly geliefert
  werden; da es kein Sicherheitsupdate für 9.0.2.Final gibt müssen wir
  die, als WildFly-Anwender, halt selbst patchen. Es wird unter der
  Apache-Lizenz 2.0 mit NOTICE-Datei (welche Sie im Quellkodetarball
  finden) vertrieben, siehe oben.
