package org.evolvis.xml.util;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * A Converter class which converts between an xsd date or xsd dateTime and a
 * {@link Date} instance. When parsing a string which holds no time zone,
 * UTC time zone is used. When parsing an xsd date string, time zone is ignored.
 * When creating an xml string, UTC time zone is always added.
 * Due to this approach The conversions by this class are independent from the
 * systems time zone.
 * Previously the class  org.apache.cxf.tools.common.DataTypeAdapter was
 * used, but it is not independent of the system time zone (default time zone).
 *
 * @author Hendrik Helwich
 */
public class DateTypeAdapter {
	private DateTypeAdapter()
	{
	}

	private static final DatatypeFactory DATATYPE_FACTORY;
	private static final DateFormat DATE_FORMAT;
	private static final TimeZone TIME_ZONE_UTC;

	static {
		try {
			DATATYPE_FACTORY = DatatypeFactory.newInstance();
			TIME_ZONE_UTC = TimeZone.getTimeZone("UTC");
			DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'Z'");
			DATE_FORMAT.setTimeZone(TIME_ZONE_UTC);
		} catch (DatatypeConfigurationException e) {
			throw new Error(e);
		}
	}

	public static Date
	parseDate(String s)
	{
		if (s == null)
			return null;
		XMLGregorianCalendar gcal = DATATYPE_FACTORY.newXMLGregorianCalendar(s);
		gcal.setTimezone(0);
		GregorianCalendar gc = gcal.toGregorianCalendar();
		return checkDate(gc.getTime());
	}

	public static String
	printDate(Date dt)
	{
		if (dt == null)
			return null;
		checkDate(dt);
		return DATE_FORMAT.format(dt);
	}

	public static Date
	parseDateTime(String s)
	{
		if (s == null)
			return null;
		s = s.trim();
		XMLGregorianCalendar gcal = DATATYPE_FACTORY.newXMLGregorianCalendar(s);
		if (DatatypeConstants.FIELD_UNDEFINED == gcal.getTimezone())
			gcal.setTimezone(0);
		GregorianCalendar gc = gcal.toGregorianCalendar();
		return gc.getTime();
	}

	public static String
	printDateTime(Date dt)
	{
		if (dt == null)
			return null;
		Calendar c = Calendar.getInstance(TIME_ZONE_UTC);
		c.setTime(dt);
		return DatatypeConverter.printDateTime(c);
	}

	public static final Date
	checkDate(Date date)
	{
		if (date.getTime() % (1000L * 60 * 60 * 24) != 0)
			throw new RuntimeException("time is present in date type: " +
			    date.getTime());
		return date;
	}
}
