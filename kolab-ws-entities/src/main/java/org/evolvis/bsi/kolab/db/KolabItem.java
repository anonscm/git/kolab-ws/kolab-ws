package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.util.DateUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

/**
 * A persistent entity which holds a kolab mail items meta data.
 *
 * @author Hendrik Helwich
 */
@Entity
@Table(
    name = KolabItem.TABLE_NAME,
    uniqueConstraints = {
	@UniqueConstraint(columnNames = { KolabItem.CNAME_FK_FOLDER, KolabItem.CNAME_KOLAB_ID }),
	@UniqueConstraint(columnNames = { KolabItem.CNAME_FK_FOLDER, KolabItem.CNAME_MAIL_ID })
    }
)
public class KolabItem implements Serializable {
	private static final long serialVersionUID = -7884060561428546401L;

	public static final String TABLE_NAME = "item";

	static final String CNAME_KOLAB_ID = "kolab_id";
	static final String CNAME_MAIL_ID = "mail_id";
	public static final String CNAME_FK_FOLDER = "fk_folder";
	public static final String CNAME_PK_ITEM = "pk_item";

	private Long pk;
	private Long mailId;
	private String kolabId;
	private Date creationDate; // MySQL DATETIME type does cut milliseconds => use BIGINT type instead
	private Date changeDate;   // MySQL DATETIME type does cut milliseconds => use BIGINT type instead
	private String changeUserId;
	private String changeDeviceId;
	private Long incidenceFirstDate; // MySQL DATETIME type does cut milliseconds => use BIGINT type instead
	private Long incidenceLastDate;  // MySQL DATETIME type does cut milliseconds => use BIGINT type instead
	private Boolean valid;
	private Boolean deleted;
	private ImapFolder folder;

	private String createUserId;

	private String createDeviceId;

	@Id
	@GeneratedValue
	@Column(name = CNAME_PK_ITEM)
	public Long
	getPk()
	{
		return pk;
	}

	public void
	setPk(Long pk)
	{
		this.pk = pk;
	}

	@Column(name = CNAME_MAIL_ID, nullable = false) // must be unique for one folder and not null for sync algorithm
	public Long
	getMailId()
	{
		return mailId;
	}

	public void
	setMailId(Long mailId)
	{
		this.mailId = mailId;
	}

	@Column(name = CNAME_KOLAB_ID, updatable = false) // can be null if item is not valid
	public String
	getKolabId()
	{
		return kolabId;
	}

	public void
	setKolabId(String kolabId)
	{
		this.kolabId = kolabId;
	}

	@Column(name = "creation_date", nullable = false, updatable = false)
	public Long
	getCreationDate_()
	{
		return DateUtil.getLong(creationDate);
	}

	public void
	setCreationDate_(Long creationDate)
	{
		this.creationDate = DateUtil.getDate(creationDate);
	}

	@Column(name = "change_date", nullable = false)
	public Long
	getChangeDate_()
	{
		return DateUtil.getLong(changeDate);
	}

	public void
	setChangeDate_(Long changeDate)
	{
		this.changeDate = DateUtil.getDate(changeDate);
	}

	@Column(name = "change_user_id")
	public String
	getChangeUserId()
	{
		return changeUserId;
	}

	public void
	setChangeUserId(String changeUserId)
	{
		this.changeUserId = changeUserId;
	}

	@Column(name = "create_user_id")
	public String
	getCreateUserId()
	{
		return createUserId;
	}

	public void
	setCreateUserId(String createUserId)
	{
		this.createUserId = createUserId;
	}

	@Column(name = "create_device_id")
	public String
	getCreateDeviceId()
	{
		return createDeviceId;
	}

	public void
	setCreateDeviceId(String createDeviceId)
	{
		this.createDeviceId = createDeviceId;
	}

	@Column(name = "change_device_id")
	public String
	getChangeDeviceId()
	{
		return changeDeviceId;
	}

	public void
	setChangeDeviceId(String changeDeviceId)
	{
		this.changeDeviceId = changeDeviceId;
	}

	@Column(name = "incidence_first_date") // null if folder type is not an incidence type or item not valid
	public Long
	getIncidenceFirstDate_()
	{
		return incidenceFirstDate;
	}

	public void
	setIncidenceFirstDate_(Long incidenceFirstDate)
	{
		this.incidenceFirstDate = incidenceFirstDate;
	}

	@Column(name = "incidence_last_date") // null if folder type is not an incidence type or item not valid
	public Long
	getIncidenceLastDate_()
	{
		return incidenceLastDate;
	}

	public void
	setIncidenceLastDate_(Long incidenceLastDate)
	{
		this.incidenceLastDate = incidenceLastDate;
	}

	@Column(nullable = false)
	public Boolean
	getValid()
	{
		return valid;
	}

	public void
	setValid(Boolean valid)
	{
		this.valid = valid;
	}

	@Column(nullable = false)
	public Boolean
	getDeleted()
	{
		return deleted;
	}

	public void
	setDeleted(Boolean deleted)
	{
		this.deleted = deleted;
	}

	@ManyToOne
	@JoinColumn(name = KolabItem.CNAME_FK_FOLDER)
	public ImapFolder
	getFolder()
	{
		return folder;
	}

	public void
	setFolder(ImapFolder folder)
	{
		this.folder = folder;
	}

	// fields which are not persisted

	@Transient
	public Date
	getCreationDate()
	{
		return creationDate;
	}

	public void
	setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	@Transient
	public Date
	getChangeDate()
	{
		return changeDate;
	}

	public void
	setChangeDate(Date changeDate)
	{
		this.changeDate = changeDate;
	}

	@Transient
	public Date
	getIncidenceFirstDate()
	{
		return DateUtil.getDate(getIncidenceFirstDate_());
	}

	public void
	setIncidenceFirstDate(Date incidenceFirstDate)
	{
		setIncidenceFirstDate_(DateUtil.getLong(incidenceFirstDate));
	}

	@Transient
	public Date
	getIncidenceLastDate()
	{
		return DateUtil.getDate(getIncidenceLastDate_());
	}

	public void
	setIncidenceLastDate(Date incidenceLastDate)
	{
		setIncidenceLastDate_(DateUtil.getLong(incidenceLastDate));
	}
}
