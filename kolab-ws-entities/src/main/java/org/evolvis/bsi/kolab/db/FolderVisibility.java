package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.util.DateUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Hendrik Helwich
 */
@Entity
@Table(
    name = FolderVisibility.TABLE_NAME,
    uniqueConstraints = {
	@UniqueConstraint(columnNames = {
	    FolderVisibility.CNAME_FK_FOLDER,
	    FolderVisibility.CNAME_FK_USER,
	    FolderVisibility.CNAME_CHANGE_DATE
	})
    }
)
public class FolderVisibility implements Serializable {
	private static final long serialVersionUID = 3977488374675121262L;

	public static final String TABLE_NAME = "folder_visibility";

	public static final String CNAME_FK_FOLDER = "fk_folder";
	public static final String CNAME_FK_USER = "fk_user";
	public static final String CNAME_PK_FOLDER_VISBILITY = "pk_folder_visibility";
	public static final String CNAME_CHANGE_DATE = "change_date";

	private Long pk;
	private Long changeDate; // MySQL DATETIME type does cut milliseconds => use BIGINT type instead
	private Boolean visible;
	private ImapFolder folder;
	private User user;

	@Id
	@GeneratedValue
	@Column(name = CNAME_PK_FOLDER_VISBILITY)
	public Long
	getPk()
	{
		return pk;
	}

	public void
	setPk(Long pk)
	{
		this.pk = pk;
	}

	@Column(name = CNAME_CHANGE_DATE, nullable = false)
	public Long
	getChangeDate_()
	{
		return changeDate;
	}

	public void
	setChangeDate_(Long changeDate)
	{
		this.changeDate = changeDate;
	}

	@Column(nullable = false)
	public Boolean
	getVisible()
	{
		return visible;
	}

	public void
	setVisible(Boolean visible)
	{
		this.visible = visible;
	}

	@ManyToOne
	@JoinColumn(name = CNAME_FK_FOLDER, nullable = false)
	public ImapFolder
	getFolder()
	{
		return folder;
	}

	public void
	setFolder(ImapFolder folder)
	{
		this.folder = folder;
	}

	@ManyToOne
	@JoinColumn(name = CNAME_FK_USER, nullable = false)
	public User
	getUser()
	{
		return user;
	}

	public void
	setUser(User user)
	{
		this.user = user;
	}

	// fields which are not persisted

	@Transient
	public Date
	getChangeDate()
	{
		return DateUtil.getDate(getChangeDate_());
	}

	public void
	setChangeDate(Date changeDate)
	{
		setChangeDate_(DateUtil.getLong(changeDate));
	}
}
