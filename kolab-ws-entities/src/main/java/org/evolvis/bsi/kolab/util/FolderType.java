package org.evolvis.bsi.kolab.util;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/**
 * All possible kolab types of an imap folder. If an imap folder is not
 * annotated, it has the type {@link #NONE}.
 */
public enum FolderType {
	CONTACT(1),
	CONTACT_DEFAULT(2),
	EVENT(3),
	EVENT_DEFAULT(4),
	JOURNAL(5),
	JOURNAL_DEFAULT(6),
	NOTE(7),
	NOTE_DEFAULT(8),
	TASK(9),
	TASK_DEFAULT(10),
	MAIL_INBOX(11),
	MAIL_DRAFTS(12),
	MAIL_SENTITEMS(13),
	MAIL_JUNKMAIL(14),
	NONE(15);

	private final int id;

	private FolderType(int id)
	{
		this.id = id;
	}

	public boolean
	isContact()
	{
		return this == CONTACT || this == CONTACT_DEFAULT;
	}

	public boolean
	isEvent()
	{
		return this == EVENT || this == EVENT_DEFAULT;
	}

	public boolean
	isJournal()
	{
		return this == JOURNAL || this == JOURNAL_DEFAULT;
	}

	public boolean
	isNote()
	{
		return this == NOTE || this == NOTE_DEFAULT;
	}

	public boolean
	isTask()
	{
		return this == TASK || this == TASK_DEFAULT;
	}

	public boolean
	isMail()
	{
		return this == MAIL_INBOX || this == MAIL_DRAFTS || this == MAIL_SENTITEMS || this == MAIL_JUNKMAIL;
	}

	public static FolderType
	getType(int id)
	{
		for (FolderType ft : values())
			if (ft.id == id)
				return ft;
		return null;
	}

	public int
	intValue()
	{
		return id;
	}
}
