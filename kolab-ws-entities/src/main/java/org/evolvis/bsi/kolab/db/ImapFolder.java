package org.evolvis.bsi.kolab.db;

/*-
 * kolab-ws — a webservice for accessing Kolab groupware PIM data — is
 * Copyright © 2014, 2015, 2016, 2017, 2018 mirabilos (t.glaser@tarent.de)
 * Copyright © 2010 tarent GmbH
 * Licensor: tarent solutions GmbH, Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import org.evolvis.bsi.kolab.util.DateUtil;
import org.evolvis.bsi.kolab.util.FolderType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A persistent entity which represents a folder in an imap system.
 *
 * @author Hendrik Helwich
 */
@Entity
@Table(name = ImapFolder.TABLE_NAME)
public class ImapFolder implements Serializable {
	private static final long serialVersionUID = -1764579712792297028L;

	//TODO add folder type; check if type is correct in some accessor operations
	//TODO add last folder successfully sync date; ensure in DBSynchronizer that the creation date of a new message is after this date.

	public ImapFolder()
	{ // should not be used in this project
	}

	public ImapFolder(final String name, final FolderType folderType)
	{
		this.name = name;
		this.type = folderType.intValue();
	}

	public static final String TABLE_NAME = "folder";
	public static final String CNAME_PK_FOLDER = "pk_folder";
	public static final String CNAME_NAME = "name";

	private Long pk;
	private String name;
	private List<KolabItem> items = new ArrayList<KolabItem>();
	private Integer type;
	private Boolean shared;
	private Long lastSyncTime; // MySQL DATETIME type does cut milliseconds => use BIGINT type instead

	@Id
	@GeneratedValue
	@Column(name = CNAME_PK_FOLDER)
	public Long
	getPk()
	{
		return pk;
	}

	public void
	setPk(Long pk)
	{
		this.pk = pk;
	}

	@Column(name = CNAME_NAME, unique = true, nullable = false, updatable = false)
	public String
	getName()
	{
		return name;
	}

	public void
	setName(String name)
	{
		this.name = name;
	}

	@OneToMany(mappedBy = "folder")
	@OrderBy(value = "mailId")
	public List<KolabItem>
	getItems()
	{
		return items;
	}

	public void
	setItems(List<KolabItem> items)
	{
		this.items = items;
	}

	@Column(name = "type", nullable = false)
	public Integer
	getType_()
	{
		return type;
	}

	public void
	setType_(Integer type)
	{
		this.type = type;
	}

	//TODO if column is always been written set: @Column(nullable=false)
	public Boolean
	getShared()
	{
		return shared;
	}

	public void
	setShared(Boolean shared)
	{
		this.shared = shared;
	}

	@Column(name = "last_sync_time")
	public Long
	getLastSyncTime_()
	{
		return lastSyncTime;
	}

	public void
	setLastSyncTime_(Long lastSyncTime)
	{
		this.lastSyncTime = lastSyncTime;
	}

	// fields which are not persisted

	@Transient
	public Date
	getLastSyncTime()
	{
		return DateUtil.getDate(getLastSyncTime_());
	}

	public void
	setLastSyncTime(Date lastSyncTime)
	{
		setLastSyncTime_(DateUtil.getLong(lastSyncTime));
	}

	@Transient
	public FolderType
	getType()
	{
		return FolderType.getType(getType_());
	}

	public void
	setType(FolderType type)
	{
		setType_(type.intValue());
	}
}
